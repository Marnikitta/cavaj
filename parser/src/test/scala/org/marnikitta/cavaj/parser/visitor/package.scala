package org.marnikitta.cavaj.parser

import java.io.ByteArrayInputStream

import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}
import org.marnikitta.cavaj.parser.antlr4.CavajParser.PrimaryContext
import org.marnikitta.cavaj.parser.antlr4.{CavajLexer, CavajParser}

package object visitor {
  def parser(expr: String): CavajParser = {
    val charStream = new ANTLRInputStream(new ByteArrayInputStream(expr.getBytes))
    val lexer = new CavajLexer(charStream)
    val tokenStream = new CommonTokenStream(lexer)
    new CavajParser(tokenStream)
  }

  def parsePrimary(expr: String): PrimaryContext = {
    parser(expr).primary()
  }
}
