package org.marnikitta.cavaj.parser.visitor

import org.marnikitta.cavaj.parser.antlr4.{CavajBaseVisitor, CavajVisitor}
import org.marnikitta.cavaj.parser.ast._
import org.scalatest.FunSuite

class PrimaryExpressionVisitorSuite extends FunSuite {
  val visitor: CavajVisitor[AnyRef] = new CavajBaseVisitor[AnyRef] with PrimaryExpressionVisitor

  def parse(expr: String): PrimaryExpression = {
    visitor.visitPrimary(parser(expr).primary()).asInstanceOf[PrimaryExpression]
  }

  test("booleanLiteral") {
    assertResult(BooleanLiteral(true))(parse("true"))
    assertResult(BooleanLiteral(false))(parse("false"))
  }

  test("integerLiteral") {
    assertResult(IntegerLiteral(1))(parse("1"))
    assertResult(IntegerLiteral(123456))(parse("123456"))
  }
  test("stringLiteral") {
    assertResult(StringLiteral("abacaba"))(parse("\"abacaba\""))
    assertResult(StringLiteral("\n\t\r"))(parse("\"\\n\\t\\r\""))
  }

  test("thisUnit") {
    assertResult(ThisExpression)(parse("this"))
    assertResult(UnitExpression)(parse("unit"))
  }

  test("varName") {
    assertResult(VariableReference("abacaba"))(parse("abacaba"))
    assertResult(VariableReference("thisy"))(parse("thisy"))
    assertResult(VariableReference("unity"))(parse("unity"))
  }

  test("block") {
    // TODO:
  }

  test("expression") {
    // TODO:
  }

  test("fieldAccess") {
    assertResult(FieldAccess("abacaba"))(parse("this.abacaba"))
  }

  test("methodInvocation") {
    // TODO:
  }
}
