trait Reader {
  def read: String
}

trait Writer {
  def write(str: String): Unit
}

class Impl {
  self: Reader with Writer =>

  override def read = "Hello there"

  override def write(str: String): Unit = println(str)

}


object Runnalka {
  def main(args: Array[String]): Unit = {
    val aba = new Impl with Reader with Writer

    println(aba.read)
  }
}
