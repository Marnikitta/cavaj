package org.marnikitta.cavaj.parser.ast

sealed trait BinaryExpression extends Expression {
  def e1: Expression

  def e2: Expression
}

case class ConditionalOrExpression(e1: Expression, e2: Expression) extends BinaryExpression

case class ConditionalAndExpression(e1: Expression, e2: Expression) extends BinaryExpression

case class BitwiseOrExpression(e1: Expression, e2: Expression) extends BinaryExpression

case class BitwiseXorExpression(e1: Expression, e2: Expression) extends BinaryExpression

case class BitwiseAndExpression(e1: Expression, e2: Expression) extends BinaryExpression

case class EqualityExpression(e1: Expression, e2: Expression, negative: Boolean) extends BinaryExpression

object BinaryRelations {

  sealed trait BinaryRelation

  case object Less extends BinaryRelation

  case object Greater extends BinaryRelation

  case object LessEqual extends BinaryRelation

  case object GreaterEqual extends BinaryRelation

}

case class RelationExpression(e1: Expression, e2: Expression, relation: BinaryRelations.BinaryRelation) extends BinaryExpression

case class InstanceOfExpression(e: Expression, clazz: ClassName) extends Expression

object ShiftDirections {

  sealed trait ShiftDirection

  case object Left extends ShiftDirection

  case object Right extends ShiftDirection

  case object RightRotate extends ShiftDirection

}

case class ShiftExpression(e1: Expression, e2: Expression, direction: ShiftDirections.ShiftDirection) extends BinaryExpression

object AdditiveOperations {

  sealed trait AdditiveOperation

  case object Add extends AdditiveOperation

  case object Subtract extends AdditiveOperation

}

case class AdditiveExpression(e1: Expression, e2: Expression, operation: AdditiveOperations.AdditiveOperation) extends BinaryExpression

object MultiplicativeOperations {

  sealed trait MultiplicativeOperation

  case object Multiply extends MultiplicativeOperation

  case object Divide extends MultiplicativeOperation

  case object Mod extends MultiplicativeOperation

}

case class MultiplicativeExpression(e1: Expression, e2: Expression, operation: MultiplicativeOperations.MultiplicativeOperation) extends BinaryExpression

sealed trait UnaryExpression extends Expression {
  def e: Expression
}

case class PreIncrement(e: Expression) extends UnaryExpression

case class PreDecrement(e: Expression) extends UnaryExpression

case class MinusExpression(e: Expression) extends UnaryExpression

case class NotExpression(e: Expression) extends UnaryExpression

case class BitwiseNotExpression(e: Expression) extends UnaryExpression

case class CastExpression(e: Expression, clazz: ClassName) extends UnaryExpression

case class PostIncrement(e: Expression) extends UnaryExpression

case class PostDecrement(e: Expression) extends UnaryExpression

