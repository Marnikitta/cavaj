package org.marnikitta.cavaj.parser.ast

trait Expression extends Statement

case class IfThenExpression(condition: Expression, ifBranch: Expression) extends Expression

case class IfThenElseExpression(condition: Expression, ifBranch: Expression, elseBranch: Expression) extends Expression

case class WhileExpression(condition: Expression, loopExpression: Expression) extends Expression

case class AssertExpression(condition: Expression, message: Expression) extends Expression

case class ForExpression(init: Statement, condition: Expression, update: Expression) extends Expression

case class ThrowExpression(e: Expression) extends Expression

case class TryExpression(expression: Expression, catches: Seq[CatchClause], finall: Option[Expression]) extends Expression

case class CatchClause(varName: VarName, types: Seq[ClassName], body: Expression)
