package org.marnikitta.cavaj.parser

import java.io.InputStream

import org.marnikitta.cavaj.parser.ast.CompilationUnit


trait Parser {
  def parse(reader: InputStream): CompilationUnit
}
