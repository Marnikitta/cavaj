package org.marnikitta.cavaj.parser.visitor

import org.marnikitta.cavaj.parser.antlr4.CavajBaseVisitor
import org.marnikitta.cavaj.parser.antlr4.CavajParser._
import org.marnikitta.cavaj.parser.ast
import org.marnikitta.cavaj.parser.ast.VariableReference

trait PrimaryExpressionVisitor {
  self: CavajBaseVisitor[AnyRef] =>

  type ExpressionWrapper = (ast.Expression) => ast.Expression

  override def visitLiteral(ctx: LiteralContext): AnyRef = {
    if (ctx.integerLiteral() != null) {
      visitIntegerLiteral(ctx.integerLiteral())
    } else if (ctx.booleanLiteral() != null) {
      visitBooleanLiteral(ctx.booleanLiteral())
    } else if (ctx.stringLiteral() != null) {
      visitStringLiteral(ctx.stringLiteral())
    } else {
      visitNullLiteral(ctx.nullLiteral())
    }
  }

  override def visitIntegerLiteral(ctx: IntegerLiteralContext): AnyRef =
    ast.IntegerLiteral(ctx.getText.toInt)

  override def visitBooleanLiteral(ctx: BooleanLiteralContext): AnyRef =
    ast.BooleanLiteral(ctx.getText.toBoolean)

  override def visitStringLiteral(ctx: StringLiteralContext): AnyRef =
  // TODO: handle unicode escapes
    ast.StringLiteral(StringContext.treatEscapes(ctx.getText).replaceAll("^\"|\"$", ""))

  override def visitNullLiteral(ctx: NullLiteralContext): AnyRef =
    ast.NullLiteral

  override def visitClassType(ctx: ClassTypeContext): AnyRef =
    ctx.getText

  override def visitVariableName(ctx: VariableNameContext): AnyRef =
    ctx.getText

  override def visitMethodName(ctx: MethodNameContext): AnyRef =
    ctx.getText

  override def visitPrimary(ctx: PrimaryContext): AnyRef = {
    val primary: ast.Expression = visitPrimary_lfno_primary(ctx.primary_lfno_primary).asInstanceOf[ast.Expression]
    import scala.collection.JavaConverters.asScalaBuffer

    asScalaBuffer(ctx.methodInvocation_lf_primary)
      .map(visitMethodInvocation_lf_primary)
      .map(_.asInstanceOf[ExpressionWrapper])
      .foldLeft(primary)((exp, wrapper) => wrapper(exp))
  }

  override def visitPrimary_tmp(ctx: Primary_tmpContext): AnyRef = throw new UnsupportedOperationException

  override def visitPrimary_lfno_primary(ctx: Primary_lfno_primaryContext): AnyRef = {
    if (ctx.expression() != null) {
      visitExpression(ctx.expression())
    } else if (ctx.literal() != null) {
      visitLiteral(ctx.literal())
    } else if (ctx.thisExpression() != null) {
      visitThisExpression(ctx.thisExpression())
    } else if (ctx.unitExpression() != null) {
      visitUnitExpression(ctx.unitExpression())
    } else if (ctx.block() != null) {
      visitBlock(ctx.block())
    } else if (ctx.variableName() != null) {
      VariableReference(visitVariableName(ctx.variableName()).asInstanceOf[ast.VarName])
    } else if (ctx.fieldAccess() != null) {
      visitFieldAccess(ctx.fieldAccess())
    } else {
      visitMethodInvocation_lfno_primary(ctx.methodInvocation_lfno_primary())
    }
  }

  override def visitThisExpression(ctx: ThisExpressionContext): AnyRef = ast.ThisExpression

  override def visitUnitExpression(ctx: UnitExpressionContext): AnyRef = ast.UnitExpression

  override def visitClassInstanceCreationExpression(ctx: ClassInstanceCreationExpressionContext): AnyRef = {
    ast.ClassInstanceCreationExpression(visitClassType(ctx.classType()).asInstanceOf[ast.ClassName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitFieldAccess(ctx: FieldAccessContext): AnyRef = {
    ast.FieldAccess(visitVariableName(ctx.variableName()).asInstanceOf[ast.VarName])
  }

  override def visitMethodInvocation(ctx: MethodInvocationContext): AnyRef = throw new UnsupportedOperationException


  override def visitMethodInvocation_lf_primary(ctx: MethodInvocation_lf_primaryContext): AnyRef = {
    (e: ast.Expression) =>
      ast.ComplexMethodInvocation(e,
        visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
        visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]]
      )
  }

  override def visitMethodInvocation_lfno_primary(ctx: MethodInvocation_lfno_primaryContext): AnyRef = {
    if (ctx.localMethodInvocation() != null) {
      visitLocalMethodInvocation(ctx.localMethodInvocation)
    } else if (ctx.staticMethodInvocation() != null) {
      visitStaticMethodInvocation(ctx.staticMethodInvocation)
    } else if (ctx.variableMethodInvocaton() != null) {
      visitVariableMethodInvocaton(ctx.variableMethodInvocaton)
    } else if (ctx.superMethodInvocation() != null) {
      visitSuperMethodInvocation(ctx.superMethodInvocation())
    } else {
      visitSuperClassMethodInvocation(ctx.superClassMethodInvocation)
    }
  }

  override def visitLocalMethodInvocation(ctx: LocalMethodInvocationContext): AnyRef = {
    ast.LocalMethodInvocation(visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitStaticMethodInvocation(ctx: StaticMethodInvocationContext): AnyRef = {
    ast.StaticMethodInvocation(visitClassType(ctx.classType).asInstanceOf[ast.ClassName],
      visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitVariableMethodInvocaton(ctx: VariableMethodInvocatonContext): AnyRef = {
    ast.VariableMethodInvocation(visitVariableName(ctx.variableName()).asInstanceOf[ast.VarName],
      visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitSuperMethodInvocation(ctx: SuperMethodInvocationContext): AnyRef = {
    ast.SuperMethodInvocation(visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitSuperClassMethodInvocation(ctx: SuperClassMethodInvocationContext): AnyRef = {
    ast.SuperClassMethodInvocation(visitClassType(ctx.classType).asInstanceOf[ast.ClassName],
      visitMethodName(ctx.methodName).asInstanceOf[ast.MethodName],
      visitArgumentList(ctx.argumentList).asInstanceOf[Seq[ast.Expression]])
  }

  override def visitArgumentList(ctx: ArgumentListContext): AnyRef = {
    import scala.collection.JavaConverters.asScalaBuffer
    asScalaBuffer(ctx.expression()).map(visitExpression)
  }
}
