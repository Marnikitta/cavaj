package org.marnikitta.cavaj.parser.ast

case class CompilationUnit(classes: Seq[ClassDeclaration])
