package org.marnikitta.cavaj.parser.ast

object ValVars {

  sealed trait ValVar

  case object Var extends ValVar

  case object Val extends ValVar

}

case class VariableDeclaration(valVar: ValVars.ValVar, varName: VarName, clazz: Option[ClassName], initialization: Option[Expression]) extends ClassBodyDeclaration with Statement
