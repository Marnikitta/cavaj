package org.marnikitta.cavaj.parser.ast

trait PrimaryExpression extends Expression

sealed trait Literal extends PrimaryExpression

case class IntegerLiteral(value: Integer) extends Literal

case class BooleanLiteral(value: Boolean) extends Literal

case class StringLiteral(value: String) extends Literal

case object NullLiteral extends Literal

case class VariableReference(name: VarName) extends PrimaryExpression

object ThisExpression extends PrimaryExpression

object UnitExpression extends PrimaryExpression

case class ClassInstanceCreationExpression(clazz: ClassName, params: Seq[Expression]) extends PrimaryExpression

case class FieldAccess(name: VarName) extends PrimaryExpression

trait MethodInvocation extends PrimaryExpression {
  def name: MethodName

  def argList: Seq[Expression]
}

case class LocalMethodInvocation(name: MethodName, argList: Seq[Expression]) extends MethodInvocation

case class StaticMethodInvocation(clazz: ClassName, name: MethodName, argList: Seq[Expression]) extends MethodInvocation

case class VariableMethodInvocation(variable: VarName, name: MethodName, argList: Seq[Expression]) extends MethodInvocation

case class SuperMethodInvocation(name: MethodName, argList: Seq[Expression]) extends MethodInvocation

case class SuperClassMethodInvocation(clazz: ClassName, name: MethodName, argList: Seq[Expression]) extends MethodInvocation

case class ComplexMethodInvocation(expression: Expression, name: MethodName, argList: Seq[Expression]) extends MethodInvocation


