package org.marnikitta.cavaj.parser.ast

trait Statement

case class Block(statements: Seq[Statement], result: Option[Expression]) extends Expression


