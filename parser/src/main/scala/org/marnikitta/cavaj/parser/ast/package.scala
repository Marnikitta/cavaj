package org.marnikitta.cavaj.parser

package object ast {
  type ClassName = String
  type VarName = String
  type MethodName = String
}
