package org.marnikitta.cavaj.parser.ast

case class ClassDeclaration(isCase: Boolean, params: Seq[ClassParam], superClass: Option[SuperClass], declarations: Seq[ClassBodyDeclaration])

case class ClassParam(valVars: ValVars.ValVar, varName: VarName, typ: ClassName)

case class SuperClass(clazz: ClassName, argList: Seq[Expression])

case class ClassBody(declarations: Seq[ClassBodyDeclaration])

trait ClassBodyDeclaration

case class InstanceInitializer(block: Block)

case class StaticInitializer(block: Block)


