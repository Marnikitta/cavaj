package org.marnikitta.cavaj.parser.ast


object MethodModifiers {

  sealed trait MethodModifier

  case object Public extends MethodModifier

  case object Protected extends MethodModifier

  case object Override extends MethodModifier

  case object Private extends MethodModifier

  case object Static extends MethodModifier

  case object Final extends MethodModifier

  case object Native extends MethodModifier

}

case class MethodDeclaration(modifiers: MethodModifiers.MethodModifier, header: MethodHeader, body: Option[Expression]) extends ClassBodyDeclaration

case class MethodHeader(name: MethodName, params: Seq[FormalParameter], returnType: Option[Expression])

case class FormalParameter(varName: VarName, clazz: ClassName)

case class ConstructorDeclaration(header: ConstructorHeader, body: Expression) extends ClassBodyDeclaration

case class ConstructorHeader(params: Seq[FormalParameter])

