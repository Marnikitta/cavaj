package org.marnikitta.cavaj.parser

import java.io.InputStream

import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}
import org.marnikitta.cavaj.parser.antlr4.{CavajLexer, CavajParser}
import org.marnikitta.cavaj.parser.ast.CompilationUnit

class ANTLRParser extends Parser {
  override def parse(stream: InputStream): CompilationUnit = {
    val charStream = new ANTLRInputStream(stream)
    val lexer = new CavajLexer(charStream)
    val tokenStream = new CommonTokenStream(lexer)
    val parser = new CavajParser(tokenStream)

    parser.compilationUnit()
    null
  }
}
