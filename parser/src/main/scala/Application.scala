import org.antlr.v4.runtime._
import org.marnikitta.cavaj.parser.antlr4.{CavajLexer, CavajParser}

import scala.io.Source

object Application {
  def main(args: Array[String]): Unit = {
    val sourceReader = Source.fromResource("Test.cavaj").bufferedReader()
    val charStream = new ANTLRInputStream(sourceReader)
    val lexer = new CavajLexer(charStream)
    val tokenStream = new CommonTokenStream(lexer)

    val parser = new CavajParser(tokenStream)
    println(parser.compilationUnit().toStringTree(parser))
  }
}
