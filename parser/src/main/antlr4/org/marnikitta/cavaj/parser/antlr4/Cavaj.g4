grammar Cavaj;

/*
 * Productions from §3 (Lexical Structure)
 */

literal
	:	integerLiteral
	| booleanLiteral
	|	stringLiteral
	|	nullLiteral
	;

integerLiteral
  : IntegerLiteral
  ;

booleanLiteral
  : BooleanLiteral
  ;

stringLiteral
  : StringLiteral
  ;

nullLiteral
  : NullLiteral
  ;


/*
 * Productions from §4 (Types, Values, and Variables)
 */

classType
	:	Identifier
	;

/*
 * Productions from §6 (Names)
 */

variableName
	:	Identifier
	;

methodName
	:	Identifier
	;

/*
 * Productions from §7 (Packages)
 */


compilationUnit
	:	classDeclaration* EOF
	;

/*
 * Productions from §8 (Classes)
 */


classDeclaration
	:	'case'? 'class' Identifier classParamClause? superclass? classBody?
	;

classParamClause
  : '(' classParams? ')'
  ;

classParams
  : classParam (',' classParam)*
  ;

valVar
  : VAL
  | VAR
  ;

classParam
  : valVar? Identifier ':' classType
  ;

superclass
	:	'extends' classType ('(' argumentList ')')?
	;

classBody
	:	'{' classBodyDeclaration* '}'
	;

classBodyDeclaration
	:	classMemberDeclaration
	|	instanceInitializer
	|	staticInitializer
	|	constructorDeclaration
	;

classMemberDeclaration
	:	variableDeclaration
	|	methodDeclaration
	|	';'
	;

variableDeclaration
	:	valVar variableDeclaratorId (((':' classType)? ('=' variableInitializer)) | (':' classType))
	;

variableDeclaratorId
	:	Identifier
	;

variableInitializer
	:	expression
	;

methodDeclaration
	:	methodModifier* 'def' methodHeader (('=' expression) | block)?
	;

methodModifier
	: 'public'
	|	'protected'
	| 'override'
	|	'private'
	|	'static'
	|	'final'
	|	'native'
	;

methodHeader
	:	 Identifier '(' formalParameters? ')' (':' classType)?
	;

formalParameters
	:	formalParameter (',' formalParameter)*
	;

formalParameter
	:	variableDeclaratorId ':' classType
	;

instanceInitializer
	:	block
	;

staticInitializer
	:	'static' block
	;

constructorDeclaration
	:	'def' constructorHeader (('=' expression) | block)
	;

constructorHeader
	:	'this' '(' formalParameters? ')'
	;

/*

 * Productions from §14 (Blocks and Statements)
 */

block
	:	'{' blockStatements? '}'
	;

blockStatements
	:	blockStatement (SEMI blockStatement)* resultExpression? ';'?
	;

blockStatement
  : expression
  | localVariableDeclaration
  ;

resultExpression
  : expression
  ;

expression
	:	ifThenExpression
	|	ifThenElseExpression
	|	whileExpression
	|	forExpression
  |	tryExpression
  | conditionalOrExpression
	|	assignment
	| returnExpression
	|	classInstanceCreationExpression
	| throwExpression
	| constructorThisExpression
	;


constructorThisExpression
  : 'this' '(' argumentList ')'
  ;

localVariableDeclaration
	:	variableDeclaration
	;

returnExpression
  : 'return' expression?
  ;

ifThenExpression
	:	'if' '(' expression ')' expression
	;

ifThenElseExpression
	:	'if' '(' expression ')' expression SEMI? 'else' expression
	;

whileExpression
	:	'while' '(' expression ')' expression
	;

forExpression
	:	'for' '(' forInit? ';' forCondition? ';' forUpdate? ')' expression
	;

forInit
  : blockStatement
	;

forCondition
  : expression
  ;

forUpdate
	:	expression
	;

throwExpression
 	:	'throw' expression
 	;

tryExpression
 	:	'try' expression catches
 	|	'try' expression catches? finally_
 	;

catches
 	:	catchClause catchClause*
 	;

catchClause
 	:	'catch' '(' catchFormalParameter ')' expression
 	;

catchFormalParameter
 	:	variableDeclaratorId ':' catchType
 	;

catchType
 	:	classType ('|' classType)*
 	;

finally_
 	:	'finally' expression
 	;


/*
 * Productions from §15 (Expressions)
 */

primary
	:	primary_lfno_primary
		(methodInvocation_lf_primary)*
	;

primary_tmp
	:	literal
	| variableName
	|	'this'
	| 'unit'
	| block
	|	'(' expression ')'
	|	fieldAccess
	|	methodInvocation
	;


primary_lfno_primary
	:	literal
	|	fieldAccess
	| variableName
	|	thisExpression
	| unitExpression
	| block
	|	'(' expression ')'
	|	methodInvocation_lfno_primary
	;

thisExpression
  : 'this'
  ;

unitExpression
  : 'unit'
  ;

classInstanceCreationExpression
	:	'new' classType '(' argumentList? ')'
	;

fieldAccess
	:	'this' '.' variableName
	;

methodInvocation
	:	methodName '(' argumentList? ')'
	|	classType '.' methodName '(' argumentList? ')'
	|	variableName '.' methodName '(' argumentList? ')'
	|	primary '.' methodName '(' argumentList? ')'
	|	'super' '.' methodName '(' argumentList? ')'
	|	classType '.' 'super' '.' methodName '(' argumentList? ')'
	;

methodInvocation_lf_primary
	:	'.' methodName '(' argumentList? ')'
	;

methodInvocation_lfno_primary
  : localMethodInvocation
  | staticMethodInvocation
  | variableMethodInvocaton
  | superMethodInvocation
  | superClassMethodInvocation
	;

localMethodInvocation
	:	methodName '(' argumentList? ')'
	;

staticMethodInvocation
	:	classType '.' methodName '(' argumentList? ')'
  ;

variableMethodInvocaton
  : variableName '.' methodName '(' argumentList? ')'
  ;

superMethodInvocation
	:	'super' '.' methodName '(' argumentList? ')'
	;

superClassMethodInvocation
	:	classType '.' 'super' '.' methodName '(' argumentList? ')'
  ;

argumentList
	:	expression (',' expression)*
	;


assignment
	:	leftHandSide assignmentOperator expression
	;

leftHandSide
	:	variableName
	|	fieldAccess
	;

assignmentOperator
	:	'='
	|	'*='
	|	'/='
	|	'%='
	|	'+='
	|	'-='
	|	'<<='
	|	'>>='
	|	'>>>='
	|	'&='
	|	'^='
	|	'|='
	;

conditionalOrExpression
	:	conditionalAndExpression
	|	conditionalOrExpression '||' conditionalAndExpression
	;

conditionalAndExpression
	:	inclusiveOrExpression
	|	conditionalAndExpression '&&' inclusiveOrExpression
	;

inclusiveOrExpression
	:	exclusiveOrExpression
	|	inclusiveOrExpression '|' exclusiveOrExpression
	;

exclusiveOrExpression
	:	andExpression
	|	exclusiveOrExpression '^' andExpression
	;

andExpression
	:	equalityExpression
	|	andExpression '&' equalityExpression
	;

equalityExpression
	:	relationalExpression
	|	equalityExpression '==' relationalExpression
	|	equalityExpression '!=' relationalExpression
	;

relationalExpression
	:	shiftExpression
	|	relationalExpression '<' shiftExpression
	|	relationalExpression '>' shiftExpression
	|	relationalExpression '<=' shiftExpression
	|	relationalExpression '>=' shiftExpression
	|	relationalExpression 'instanceof' classType
	;

shiftExpression
	:	additiveExpression
	|	shiftExpression '<' '<' additiveExpression
	|	shiftExpression '>' '>' additiveExpression
	|	shiftExpression '>' '>' '>' additiveExpression
	;

additiveExpression
	:	multiplicativeExpression
	|	additiveExpression '+' multiplicativeExpression
	|	additiveExpression '-' multiplicativeExpression
	;

multiplicativeExpression
	:	unaryExpression
	|	multiplicativeExpression '*' unaryExpression
	|	multiplicativeExpression '/' unaryExpression
	|	multiplicativeExpression '%' unaryExpression
	;

unaryExpression
	:	preIncrementExpression
	|	preDecrementExpression
	|	'+' unaryExpression
	|	'-' unaryExpression
	|	unaryExpressionNotPlusMinus
	;

preIncrementExpression
	:	'++' unaryExpression
	;

preDecrementExpression
	:	'--' unaryExpression
	;

unaryExpressionNotPlusMinus
	:	postfixExpression
	|	'~' unaryExpression
	|	'!' unaryExpression
	|	castExpression
	;

postfixExpression
	:	primary postIncrementExpression_lf_postfixExpression*
	|	primary postDecrementExpression_lf_postfixExpression*
	;

postIncrementExpression
	:	postfixExpression '++'
	;

postIncrementExpression_lf_postfixExpression
	:	'++'
	;

postDecrementExpression
	:	postfixExpression '--'
	;

postDecrementExpression_lf_postfixExpression
	:	'--'
	;

castExpression
	: '(' classType ')' unaryExpressionNotPlusMinus
	;

// LEXER

// §3.9 Keywords

ABSTRACT : 'abstract';
ASSERT : 'assert';
BOOLEAN : 'boolean';
BREAK : 'break';
CATCH : 'catch';
CLASS : 'class';
CONTINUE : 'continue';
DEFAULT : 'default';
DO : 'do';
ELSE : 'else';
EXTENDS : 'extends';
FI : 'fi';
FINAL : 'final';
FINALLY : 'finally';
FOR : 'for';
IF : 'if';
INSTANCEOF : 'instanceof';
INT : 'int';
NATIVE : 'native';
NEW : 'new';
PRIVATE : 'private';
PROTECTED : 'protected';
PUBLIC : 'public';
RETURN : 'return';
STATIC : 'static';
SUPER : 'super';
THIS : 'this';
UNIT: 'unit';
VAR : 'var';
VAL : 'val';
WHILE : 'while';

// §3.10.1 Integer Literals

IntegerLiteral
	:	DecimalIntegerLiteral
	;

fragment
DecimalIntegerLiteral
	:	DecimalNumeral IntegerTypeSuffix?
	;

fragment
HexIntegerLiteral
	:	HexNumeral IntegerTypeSuffix?
	;

fragment
OctalIntegerLiteral
	:	OctalNumeral IntegerTypeSuffix?
	;

fragment
BinaryIntegerLiteral
	:	BinaryNumeral IntegerTypeSuffix?
	;

fragment
IntegerTypeSuffix
	:	[lL]
	;

fragment
DecimalNumeral
	:	'0'
	|	NonZeroDigit (Digits? | Underscores Digits)
	;

fragment
Digits
	:	Digit (DigitsAndUnderscores? Digit)?
	;

fragment
Digit
	:	'0'
	|	NonZeroDigit
	;

fragment
NonZeroDigit
	:	[1-9]
	;

fragment
DigitsAndUnderscores
	:	DigitOrUnderscore+
	;

fragment
DigitOrUnderscore
	:	Digit
	|	'_'
	;

fragment
Underscores
	:	'_'+
	;

fragment
HexNumeral
	:	'0' [xX] HexDigits
	;

fragment
HexDigits
	:	HexDigit (HexDigitsAndUnderscores? HexDigit)?
	;

fragment
HexDigit
	:	[0-9a-fA-F]
	;

fragment
HexDigitsAndUnderscores
	:	HexDigitOrUnderscore+
	;

fragment
HexDigitOrUnderscore
	:	HexDigit
	|	'_'
	;

fragment
OctalNumeral
	:	'0' Underscores? OctalDigits
	;

fragment
OctalDigits
	:	OctalDigit (OctalDigitsAndUnderscores? OctalDigit)?
	;

fragment
OctalDigit
	:	[0-7]
	;

fragment
OctalDigitsAndUnderscores
	:	OctalDigitOrUnderscore+
	;

fragment
OctalDigitOrUnderscore
	:	OctalDigit
	|	'_'
	;

fragment
BinaryNumeral
	:	'0' [bB] BinaryDigits
	;

fragment
BinaryDigits
	:	BinaryDigit (BinaryDigitsAndUnderscores? BinaryDigit)?
	;

fragment
BinaryDigit
	:	[01]
	;

fragment
BinaryDigitsAndUnderscores
	:	BinaryDigitOrUnderscore+
	;

fragment
BinaryDigitOrUnderscore
	:	BinaryDigit
	|	'_'
	;

// §3.10.2 Floating-Point Literals

fragment
ExponentIndicator
	:	[eE]
	;

fragment
SignedInteger
	:	Sign? Digits
	;

fragment
Sign
	:	[+-]
	;

fragment
HexSignificand
	:	HexNumeral '.'?
	|	'0' [xX] HexDigits? '.' HexDigits
	;

fragment
BinaryExponent
	:	BinaryExponentIndicator SignedInteger
	;

fragment
BinaryExponentIndicator
	:	[pP]
	;

// §3.10.3 Boolean Literals

BooleanLiteral
	:	'true'
	|	'false'
	;

// §3.10.4 Character Literals

CharacterLiteral
	:	'\'' SingleCharacter '\''
	|	'\'' EscapeSequence '\''
	;

fragment
SingleCharacter
	:	~['\\]
	;

// §3.10.5 String Literals

StringLiteral
	:	'"' StringCharacters? '"'
	;

fragment
StringCharacters
	:	StringCharacter+
	;

fragment
StringCharacter
	:	~["\\]
	|	EscapeSequence
	;

// §3.10.6 Escape Sequences for Character and String Literals

fragment
EscapeSequence
	:	'\\' [btnfr"'\\]
	|	OctalEscape
    |   UnicodeEscape // This is not in the spec but prevents having to preprocess the input
	;

fragment
OctalEscape
	:	'\\' OctalDigit
	|	'\\' OctalDigit OctalDigit
	|	'\\' ZeroToThree OctalDigit OctalDigit
	;

fragment
ZeroToThree
	:	[0-3]
	;

// This is not in the spec but prevents having to preprocess the input
fragment
UnicodeEscape
    :   '\\' 'u' HexDigit HexDigit HexDigit HexDigit
    ;

// §3.10.7 The Null Literal

NullLiteral
	:	'null'
	;

// §3.11 Separators

LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COMMA : ',';
DOT : '.';

// §3.12 Operators

ASSIGN : '=';
GT : '>';
LT : '<';
BANG : '!';
TILDE : '~';
QUESTION : '?';
COLON : ':';
EQUAL : '==';
LE : '<=';
GE : '>=';
NOTEQUAL : '!=';
AND : '&&';
OR : '||';
INC : '++';
DEC : '--';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
BITAND : '&';
BITOR : '|';
CARET : '^';
MOD : '%';
ARROW : '->';
COLONCOLON : '::';

ADD_ASSIGN : '+=';
SUB_ASSIGN : '-=';
MUL_ASSIGN : '*=';
DIV_ASSIGN : '/=';
AND_ASSIGN : '&=';
OR_ASSIGN : '|=';
XOR_ASSIGN : '^=';
MOD_ASSIGN : '%=';
LSHIFT_ASSIGN : '<<=';
RSHIFT_ASSIGN : '>>=';
URSHIFT_ASSIGN : '>>>=';

// §3.8 Identifiers (must appear after all keywords in the grammar)

Identifier
	:	CavajLetter CavajLetterOrDigit*
	;

fragment
CavajLetter
	:	[a-zA-Z$_] // these are the "java letters" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierStart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

fragment
CavajLetterOrDigit
	:	[a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierPart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

//
// Additional symbols not defined in the lexical specification
//

AT : '@';
ELLIPSIS : '...';

//
// Whitespace and comments
//

WS
  : [ \r\n\t] -> skip
  ;

COMMENT
    :   '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;

