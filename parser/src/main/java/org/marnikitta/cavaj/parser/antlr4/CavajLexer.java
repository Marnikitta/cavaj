// Generated from org/marnikitta/cavaj/parser/antlr4/Cavaj.g4 by ANTLR 4.6
package org.marnikitta.cavaj.parser.antlr4;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CavajLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, ABSTRACT=6, ASSERT=7, BOOLEAN=8, 
		BREAK=9, CATCH=10, CLASS=11, CONTINUE=12, DEFAULT=13, DO=14, ELSE=15, 
		EXTENDS=16, FI=17, FINAL=18, FINALLY=19, FOR=20, IF=21, INSTANCEOF=22, 
		INT=23, NATIVE=24, NEW=25, PRIVATE=26, PROTECTED=27, PUBLIC=28, RETURN=29, 
		STATIC=30, SUPER=31, THIS=32, UNIT=33, VAR=34, VAL=35, WHILE=36, IntegerLiteral=37,
          BooleanLiteral = 38, CharacterLiteral = 39, StringLiteral = 40, NullLiteral = 41,
          LPAREN = 42, RPAREN = 43, LBRACE = 44, RBRACE = 45, LBRACK = 46, RBRACK = 47, SEMI = 48,
          COMMA = 49, DOT = 50, ASSIGN = 51, GT = 52, LT = 53, BANG = 54, TILDE = 55, QUESTION = 56,
          COLON = 57, EQUAL = 58, LE = 59, GE = 60, NOTEQUAL = 61, AND = 62, OR = 63, INC = 64,
          DEC = 65, ADD = 66, SUB = 67, MUL = 68, DIV = 69, BITAND = 70, BITOR = 71, CARET = 72,
          MOD = 73, ARROW = 74, COLONCOLON = 75, ADD_ASSIGN = 76, SUB_ASSIGN = 77, MUL_ASSIGN = 78,
          DIV_ASSIGN = 79, AND_ASSIGN = 80, OR_ASSIGN = 81, XOR_ASSIGN = 82, MOD_ASSIGN = 83,
          LSHIFT_ASSIGN = 84, RSHIFT_ASSIGN = 85, URSHIFT_ASSIGN = 86, Identifier = 87,
          AT = 88, ELLIPSIS = 89, WS = 90, COMMENT = 91, LINE_COMMENT = 92;
  public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "ABSTRACT", "ASSERT", "BOOLEAN", 
		"BREAK", "CATCH", "CLASS", "CONTINUE", "DEFAULT", "DO", "ELSE", "EXTENDS", 
		"FI", "FINAL", "FINALLY", "FOR", "IF", "INSTANCEOF", "INT", "NATIVE", 
		"NEW", "PRIVATE", "PROTECTED", "PUBLIC", "RETURN", "STATIC", "SUPER", 
		"THIS", "UNIT", "VAR", "VAL", "WHILE", "IntegerLiteral", "DecimalIntegerLiteral", 
		"HexIntegerLiteral", "OctalIntegerLiteral", "BinaryIntegerLiteral", "IntegerTypeSuffix", 
		"DecimalNumeral", "Digits", "Digit", "NonZeroDigit", "DigitsAndUnderscores", 
		"DigitOrUnderscore", "Underscores", "HexNumeral", "HexDigits", "HexDigit", 
		"HexDigitsAndUnderscores", "HexDigitOrUnderscore", "OctalNumeral", "OctalDigits", 
		"OctalDigit", "OctalDigitsAndUnderscores", "OctalDigitOrUnderscore", "BinaryNumeral", 
		"BinaryDigits", "BinaryDigit", "BinaryDigitsAndUnderscores", "BinaryDigitOrUnderscore",
          "ExponentIndicator", "SignedInteger", "Sign", "HexSignificand", "BinaryExponent",
          "BinaryExponentIndicator", "BooleanLiteral", "CharacterLiteral", "SingleCharacter",
          "StringLiteral", "StringCharacters", "StringCharacter", "EscapeSequence",
          "OctalEscape", "ZeroToThree", "UnicodeEscape", "NullLiteral", "LPAREN",
          "RPAREN", "LBRACE", "RBRACE", "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT",
          "ASSIGN", "GT", "LT", "BANG", "TILDE", "QUESTION", "COLON", "EQUAL", "LE",
          "GE", "NOTEQUAL", "AND", "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV",
          "BITAND", "BITOR", "CARET", "MOD", "ARROW", "COLONCOLON", "ADD_ASSIGN",
          "SUB_ASSIGN", "MUL_ASSIGN", "DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN",
          "MOD_ASSIGN", "LSHIFT_ASSIGN", "RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "Identifier",
          "CavajLetter", "CavajLetterOrDigit", "AT", "ELLIPSIS", "WS", "COMMENT",
          "LINE_COMMENT"
  };

	private static final String[] _LITERAL_NAMES = {
		null, "'case'", "'def'", "'override'", "'throw'", "'try'", "'abstract'", 
		"'assert'", "'boolean'", "'break'", "'catch'", "'class'", "'continue'", 
		"'default'", "'do'", "'else'", "'extends'", "'fi'", "'final'", "'finally'", 
		"'for'", "'if'", "'instanceof'", "'int'", "'native'", "'new'", "'private'", 
		"'protected'", "'public'", "'return'", "'static'", "'super'", "'this'",
          "'unit'", "'var'", "'val'", "'while'", null, null, null, null, "'null'",
          "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "','", "'.'", "'='",
		"'>'", "'<'", "'!'", "'~'", "'?'", "':'", "'=='", "'<='", "'>='", "'!='", 
		"'&&'", "'||'", "'++'", "'--'", "'+'", "'-'", "'*'", "'/'", "'&'", "'|'", 
		"'^'", "'%'", "'->'", "'::'", "'+='", "'-='", "'*='", "'/='", "'&='", 
		"'|='", "'^='", "'%='", "'<<='", "'>>='", "'>>>='", null, "'@'", "'...'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", 
		"CATCH", "CLASS", "CONTINUE", "DEFAULT", "DO", "ELSE", "EXTENDS", "FI", 
		"FINAL", "FINALLY", "FOR", "IF", "INSTANCEOF", "INT", "NATIVE", "NEW", 
		"PRIVATE", "PROTECTED", "PUBLIC", "RETURN", "STATIC", "SUPER", "THIS",
          "UNIT", "VAR", "VAL", "WHILE", "IntegerLiteral", "BooleanLiteral", "CharacterLiteral",
          "StringLiteral", "NullLiteral", "LPAREN", "RPAREN", "LBRACE", "RBRACE",
          "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", "GT", "LT", "BANG",
          "TILDE", "QUESTION", "COLON", "EQUAL", "LE", "GE", "NOTEQUAL", "AND",
          "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "BITAND", "BITOR", "CARET",
          "MOD", "ARROW", "COLONCOLON", "ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN",
          "DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN", "LSHIFT_ASSIGN",
          "RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "Identifier", "AT", "ELLIPSIS", "WS",
          "COMMENT", "LINE_COMMENT"
  };
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public CavajLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Cavaj.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
      case 127:
        return CavajLetter_sempred((RuleContext)_localctx, predIndex);
      case 128:
        return CavajLetterOrDigit_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean CavajLetter_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return Character.isJavaIdentifierStart(_input.LA(-1));
		case 1:
			return Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)));
		}
		return true;
	}
	private boolean CavajLetterOrDigit_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return Character.isJavaIdentifierPart(_input.LA(-1));
		case 3:
			return Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)));
		}
		return true;
	}

	public static final String _serializedATN =
          "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2^\u037b\b\1\4\2\t" +
                  "\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
                  "\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3" +
                  "\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6" +
                  "\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3" +
                  "\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3" +
                  "\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3" +
                  "\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17" +
                  "\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22" +
                  "\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24" +
                  "\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27" +
                  "\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31" +
                  "\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33" +
                  "\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35" +
                  "\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37" +
                  "\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3!\3!\3!\3!\3!\3\"\3\"\3\"\3\"" +
                  "\3\"\3#\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3\'\3\'\5\'\u01f2" +
                  "\n\'\3(\3(\5(\u01f6\n(\3)\3)\5)\u01fa\n)\3*\3*\5*\u01fe\n*\3+\3+\3,\3" +
                  ",\3,\5,\u0205\n,\3,\3,\3,\5,\u020a\n,\5,\u020c\n,\3-\3-\5-\u0210\n-\3" +
                  "-\5-\u0213\n-\3.\3.\5.\u0217\n.\3/\3/\3\60\6\60\u021c\n\60\r\60\16\60" +
                  "\u021d\3\61\3\61\5\61\u0222\n\61\3\62\6\62\u0225\n\62\r\62\16\62\u0226" +
                  "\3\63\3\63\3\63\3\63\3\64\3\64\5\64\u022f\n\64\3\64\5\64\u0232\n\64\3" +
                  "\65\3\65\3\66\6\66\u0237\n\66\r\66\16\66\u0238\3\67\3\67\5\67\u023d\n" +
                  "\67\38\38\58\u0241\n8\38\38\39\39\59\u0247\n9\39\59\u024a\n9\3:\3:\3;" +
                  "\6;\u024f\n;\r;\16;\u0250\3<\3<\5<\u0255\n<\3=\3=\3=\3=\3>\3>\5>\u025d" +
                  "\n>\3>\5>\u0260\n>\3?\3?\3@\6@\u0265\n@\r@\16@\u0266\3A\3A\5A\u026b\n" +
                  "A\3B\3B\3C\5C\u0270\nC\3C\3C\3D\3D\3E\3E\5E\u0278\nE\3E\3E\3E\5E\u027d" +
                  "\nE\3E\3E\5E\u0281\nE\3F\3F\3F\3G\3G\3H\3H\3H\3H\3H\3H\3H\3H\3H\5H\u0291" +
                  "\nH\3I\3I\3I\3I\3I\3I\3I\3I\5I\u029b\nI\3J\3J\3K\3K\5K\u02a1\nK\3K\3K" +
                  "\3L\6L\u02a6\nL\rL\16L\u02a7\3M\3M\5M\u02ac\nM\3N\3N\3N\3N\5N\u02b2\n" +
                  "N\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\5O\u02bf\nO\3P\3P\3Q\3Q\3Q\3Q\3Q\3" +
                  "Q\3Q\3R\3R\3R\3R\3R\3S\3S\3T\3T\3U\3U\3V\3V\3W\3W\3X\3X\3Y\3Y\3Z\3Z\3" +
                  "[\3[\3\\\3\\\3]\3]\3^\3^\3_\3_\3`\3`\3a\3a\3b\3b\3c\3c\3c\3d\3d\3d\3e" +
                  "\3e\3e\3f\3f\3f\3g\3g\3g\3h\3h\3h\3i\3i\3i\3j\3j\3j\3k\3k\3l\3l\3m\3m" +
                  "\3n\3n\3o\3o\3p\3p\3q\3q\3r\3r\3s\3s\3s\3t\3t\3t\3u\3u\3u\3v\3v\3v\3w" +
                  "\3w\3w\3x\3x\3x\3y\3y\3y\3z\3z\3z\3{\3{\3{\3|\3|\3|\3}\3}\3}\3}\3~\3~" +
                  "\3~\3~\3\177\3\177\3\177\3\177\3\177\3\u0080\3\u0080\7\u0080\u0344\n\u0080" +
                  "\f\u0080\16\u0080\u0347\13\u0080\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081" +
                  "\3\u0081\5\u0081\u034f\n\u0081\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082" +
                  "\3\u0082\5\u0082\u0357\n\u0082\3\u0083\3\u0083\3\u0084\3\u0084\3\u0084" +
                  "\3\u0084\3\u0085\3\u0085\3\u0085\3\u0085\3\u0086\3\u0086\3\u0086\3\u0086" +
                  "\7\u0086\u0367\n\u0086\f\u0086\16\u0086\u036a\13\u0086\3\u0086\3\u0086" +
                  "\3\u0086\3\u0086\3\u0086\3\u0087\3\u0087\3\u0087\3\u0087\7\u0087\u0375" +
                  "\n\u0087\f\u0087\16\u0087\u0378\13\u0087\3\u0087\3\u0087\3\u0368\2\u0088" +
                  "\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20" +
                  "\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37" +
                  "= ?!A\"C#E$G%I&K\'M\2O\2Q\2S\2U\2W\2Y\2[\2]\2_\2a\2c\2e\2g\2i\2k\2m\2" +
                  "o\2q\2s\2u\2w\2y\2{\2}\2\177\2\u0081\2\u0083\2\u0085\2\u0087\2\u0089\2" +
                  "\u008b\2\u008d\2\u008f(\u0091)\u0093\2\u0095*\u0097\2\u0099\2\u009b\2" +
                  "\u009d\2\u009f\2\u00a1\2\u00a3+\u00a5,\u00a7-\u00a9.\u00ab/\u00ad\60\u00af" +
                  "\61\u00b1\62\u00b3\63\u00b5\64\u00b7\65\u00b9\66\u00bb\67\u00bd8\u00bf" +
                  "9\u00c1:\u00c3;\u00c5<\u00c7=\u00c9>\u00cb?\u00cd@\u00cfA\u00d1B\u00d3" +
                  "C\u00d5D\u00d7E\u00d9F\u00dbG\u00ddH\u00dfI\u00e1J\u00e3K\u00e5L\u00e7" +
                  "M\u00e9N\u00ebO\u00edP\u00efQ\u00f1R\u00f3S\u00f5T\u00f7U\u00f9V\u00fb" +
                  "W\u00fdX\u00ffY\u0101\2\u0103\2\u0105Z\u0107[\u0109\\\u010b]\u010d^\3" +
                  "\2\27\4\2NNnn\3\2\63;\4\2ZZzz\5\2\62;CHch\3\2\629\4\2DDdd\3\2\62\63\4" +
                  "\2GGgg\4\2--//\4\2RRrr\4\2))^^\4\2$$^^\n\2$$))^^ddhhppttvv\3\2\62\65\6" +
                  "\2&&C\\aac|\4\2\2\u0081\ud802\udc01\3\2\ud802\udc01\3\2\udc02\ue001\7" +
                  "\2&&\62;C\\aac|\5\2\13\f\17\17\"\"\4\2\f\f\17\17\u037e\2\3\3\2\2\2\2\5" +
                  "\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2" +
                  "\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33" +
                  "\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2" +
                  "\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2" +
                  "\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2" +
                  "\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K" +
                  "\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0095\3\2\2\2\2\u00a3\3\2\2" +
                  "\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2\2\2\u00ab\3\2\2\2\2\u00ad" +
                  "\3\2\2\2\2\u00af\3\2\2\2\2\u00b1\3\2\2\2\2\u00b3\3\2\2\2\2\u00b5\3\2\2"+
		"\2\2\u00b7\3\2\2\2\2\u00b9\3\2\2\2\2\u00bb\3\2\2\2\2\u00bd\3\2\2\2\2\u00bf"+
		"\3\2\2\2\2\u00c1\3\2\2\2\2\u00c3\3\2\2\2\2\u00c5\3\2\2\2\2\u00c7\3\2\2"+
		"\2\2\u00c9\3\2\2\2\2\u00cb\3\2\2\2\2\u00cd\3\2\2\2\2\u00cf\3\2\2\2\2\u00d1"+
		"\3\2\2\2\2\u00d3\3\2\2\2\2\u00d5\3\2\2\2\2\u00d7\3\2\2\2\2\u00d9\3\2\2"+
		"\2\2\u00db\3\2\2\2\2\u00dd\3\2\2\2\2\u00df\3\2\2\2\2\u00e1\3\2\2\2\2\u00e3"+
		"\3\2\2\2\2\u00e5\3\2\2\2\2\u00e7\3\2\2\2\2\u00e9\3\2\2\2\2\u00eb\3\2\2"+
		"\2\2\u00ed\3\2\2\2\2\u00ef\3\2\2\2\2\u00f1\3\2\2\2\2\u00f3\3\2\2\2\2\u00f5"+
		"\3\2\2\2\2\u00f7\3\2\2\2\2\u00f9\3\2\2\2\2\u00fb\3\2\2\2\2\u00fd\3\2\2"+
                  "\2\2\u00ff\3\2\2\2\2\u0105\3\2\2\2\2\u0107\3\2\2\2\2\u0109\3\2\2\2\2\u010b" +
                  "\3\2\2\2\2\u010d\3\2\2\2\3\u010f\3\2\2\2\5\u0114\3\2\2\2\7\u0118\3\2\2" +
                  "\2\t\u0121\3\2\2\2\13\u0127\3\2\2\2\r\u012b\3\2\2\2\17\u0134\3\2\2\2\21" +
                  "\u013b\3\2\2\2\23\u0143\3\2\2\2\25\u0149\3\2\2\2\27\u014f\3\2\2\2\31\u0155" +
                  "\3\2\2\2\33\u015e\3\2\2\2\35\u0166\3\2\2\2\37\u0169\3\2\2\2!\u016e\3\2" +
                  "\2\2#\u0176\3\2\2\2%\u0179\3\2\2\2\'\u017f\3\2\2\2)\u0187\3\2\2\2+\u018b" +
                  "\3\2\2\2-\u018e\3\2\2\2/\u0199\3\2\2\2\61\u019d\3\2\2\2\63\u01a4\3\2\2" +
                  "\2\65\u01a8\3\2\2\2\67\u01b0\3\2\2\29\u01ba\3\2\2\2;\u01c1\3\2\2\2=\u01c8" +
                  "\3\2\2\2?\u01cf\3\2\2\2A\u01d5\3\2\2\2C\u01da\3\2\2\2E\u01df\3\2\2\2G" +
                  "\u01e3\3\2\2\2I\u01e7\3\2\2\2K\u01ed\3\2\2\2M\u01ef\3\2\2\2O\u01f3\3\2" +
                  "\2\2Q\u01f7\3\2\2\2S\u01fb\3\2\2\2U\u01ff\3\2\2\2W\u020b\3\2\2\2Y\u020d" +
                  "\3\2\2\2[\u0216\3\2\2\2]\u0218\3\2\2\2_\u021b\3\2\2\2a\u0221\3\2\2\2c" +
                  "\u0224\3\2\2\2e\u0228\3\2\2\2g\u022c\3\2\2\2i\u0233\3\2\2\2k\u0236\3\2" +
                  "\2\2m\u023c\3\2\2\2o\u023e\3\2\2\2q\u0244\3\2\2\2s\u024b\3\2\2\2u\u024e" +
                  "\3\2\2\2w\u0254\3\2\2\2y\u0256\3\2\2\2{\u025a\3\2\2\2}\u0261\3\2\2\2\177" +
                  "\u0264\3\2\2\2\u0081\u026a\3\2\2\2\u0083\u026c\3\2\2\2\u0085\u026f\3\2" +
                  "\2\2\u0087\u0273\3\2\2\2\u0089\u0280\3\2\2\2\u008b\u0282\3\2\2\2\u008d" +
                  "\u0285\3\2\2\2\u008f\u0290\3\2\2\2\u0091\u029a\3\2\2\2\u0093\u029c\3\2" +
                  "\2\2\u0095\u029e\3\2\2\2\u0097\u02a5\3\2\2\2\u0099\u02ab\3\2\2\2\u009b" +
                  "\u02b1\3\2\2\2\u009d\u02be\3\2\2\2\u009f\u02c0\3\2\2\2\u00a1\u02c2\3\2" +
                  "\2\2\u00a3\u02c9\3\2\2\2\u00a5\u02ce\3\2\2\2\u00a7\u02d0\3\2\2\2\u00a9" +
                  "\u02d2\3\2\2\2\u00ab\u02d4\3\2\2\2\u00ad\u02d6\3\2\2\2\u00af\u02d8\3\2" +
                  "\2\2\u00b1\u02da\3\2\2\2\u00b3\u02dc\3\2\2\2\u00b5\u02de\3\2\2\2\u00b7" +
                  "\u02e0\3\2\2\2\u00b9\u02e2\3\2\2\2\u00bb\u02e4\3\2\2\2\u00bd\u02e6\3\2" +
                  "\2\2\u00bf\u02e8\3\2\2\2\u00c1\u02ea\3\2\2\2\u00c3\u02ec\3\2\2\2\u00c5" +
                  "\u02ee\3\2\2\2\u00c7\u02f1\3\2\2\2\u00c9\u02f4\3\2\2\2\u00cb\u02f7\3\2" +
                  "\2\2\u00cd\u02fa\3\2\2\2\u00cf\u02fd\3\2\2\2\u00d1\u0300\3\2\2\2\u00d3" +
                  "\u0303\3\2\2\2\u00d5\u0306\3\2\2\2\u00d7\u0308\3\2\2\2\u00d9\u030a\3\2" +
                  "\2\2\u00db\u030c\3\2\2\2\u00dd\u030e\3\2\2\2\u00df\u0310\3\2\2\2\u00e1" +
                  "\u0312\3\2\2\2\u00e3\u0314\3\2\2\2\u00e5\u0316\3\2\2\2\u00e7\u0319\3\2" +
                  "\2\2\u00e9\u031c\3\2\2\2\u00eb\u031f\3\2\2\2\u00ed\u0322\3\2\2\2\u00ef" +
                  "\u0325\3\2\2\2\u00f1\u0328\3\2\2\2\u00f3\u032b\3\2\2\2\u00f5\u032e\3\2" +
                  "\2\2\u00f7\u0331\3\2\2\2\u00f9\u0334\3\2\2\2\u00fb\u0338\3\2\2\2\u00fd" +
                  "\u033c\3\2\2\2\u00ff\u0341\3\2\2\2\u0101\u034e\3\2\2\2\u0103\u0356\3\2" +
                  "\2\2\u0105\u0358\3\2\2\2\u0107\u035a\3\2\2\2\u0109\u035e\3\2\2\2\u010b" +
                  "\u0362\3\2\2\2\u010d\u0370\3\2\2\2\u010f\u0110\7e\2\2\u0110\u0111\7c\2" +
                  "\2\u0111\u0112\7u\2\2\u0112\u0113\7g\2\2\u0113\4\3\2\2\2\u0114\u0115\7" +
                  "f\2\2\u0115\u0116\7g\2\2\u0116\u0117\7h\2\2\u0117\6\3\2\2\2\u0118\u0119" +
                  "\7q\2\2\u0119\u011a\7x\2\2\u011a\u011b\7g\2\2\u011b\u011c\7t\2\2\u011c" +
                  "\u011d\7t\2\2\u011d\u011e\7k\2\2\u011e\u011f\7f\2\2\u011f\u0120\7g\2\2" +
                  "\u0120\b\3\2\2\2\u0121\u0122\7v\2\2\u0122\u0123\7j\2\2\u0123\u0124\7t" +
                  "\2\2\u0124\u0125\7q\2\2\u0125\u0126\7y\2\2\u0126\n\3\2\2\2\u0127\u0128" +
                  "\7v\2\2\u0128\u0129\7t\2\2\u0129\u012a\7{\2\2\u012a\f\3\2\2\2\u012b\u012c" +
                  "\7c\2\2\u012c\u012d\7d\2\2\u012d\u012e\7u\2\2\u012e\u012f\7v\2\2\u012f" +
                  "\u0130\7t\2\2\u0130\u0131\7c\2\2\u0131\u0132\7e\2\2\u0132\u0133\7v\2\2" +
                  "\u0133\16\3\2\2\2\u0134\u0135\7c\2\2\u0135\u0136\7u\2\2\u0136\u0137\7" +
                  "u\2\2\u0137\u0138\7g\2\2\u0138\u0139\7t\2\2\u0139\u013a\7v\2\2\u013a\20" +
                  "\3\2\2\2\u013b\u013c\7d\2\2\u013c\u013d\7q\2\2\u013d\u013e\7q\2\2\u013e" +
                  "\u013f\7n\2\2\u013f\u0140\7g\2\2\u0140\u0141\7c\2\2\u0141\u0142\7p\2\2" +
                  "\u0142\22\3\2\2\2\u0143\u0144\7d\2\2\u0144\u0145\7t\2\2\u0145\u0146\7" +
                  "g\2\2\u0146\u0147\7c\2\2\u0147\u0148\7m\2\2\u0148\24\3\2\2\2\u0149\u014a" +
                  "\7e\2\2\u014a\u014b\7c\2\2\u014b\u014c\7v\2\2\u014c\u014d\7e\2\2\u014d" +
                  "\u014e\7j\2\2\u014e\26\3\2\2\2\u014f\u0150\7e\2\2\u0150\u0151\7n\2\2\u0151" +
                  "\u0152\7c\2\2\u0152\u0153\7u\2\2\u0153\u0154\7u\2\2\u0154\30\3\2\2\2\u0155" +
                  "\u0156\7e\2\2\u0156\u0157\7q\2\2\u0157\u0158\7p\2\2\u0158\u0159\7v\2\2" +
                  "\u0159\u015a\7k\2\2\u015a\u015b\7p\2\2\u015b\u015c\7w\2\2\u015c\u015d" +
                  "\7g\2\2\u015d\32\3\2\2\2\u015e\u015f\7f\2\2\u015f\u0160\7g\2\2\u0160\u0161" +
                  "\7h\2\2\u0161\u0162\7c\2\2\u0162\u0163\7w\2\2\u0163\u0164\7n\2\2\u0164" +
                  "\u0165\7v\2\2\u0165\34\3\2\2\2\u0166\u0167\7f\2\2\u0167\u0168\7q\2\2\u0168" +
                  "\36\3\2\2\2\u0169\u016a\7g\2\2\u016a\u016b\7n\2\2\u016b\u016c\7u\2\2\u016c" +
                  "\u016d\7g\2\2\u016d \3\2\2\2\u016e\u016f\7g\2\2\u016f\u0170\7z\2\2\u0170" +
                  "\u0171\7v\2\2\u0171\u0172\7g\2\2\u0172\u0173\7p\2\2\u0173\u0174\7f\2\2" +
                  "\u0174\u0175\7u\2\2\u0175\"\3\2\2\2\u0176\u0177\7h\2\2\u0177\u0178\7k" +
                  "\2\2\u0178$\3\2\2\2\u0179\u017a\7h\2\2\u017a\u017b\7k\2\2\u017b\u017c" +
                  "\7p\2\2\u017c\u017d\7c\2\2\u017d\u017e\7n\2\2\u017e&\3\2\2\2\u017f\u0180" +
                  "\7h\2\2\u0180\u0181\7k\2\2\u0181\u0182\7p\2\2\u0182\u0183\7c\2\2\u0183" +
                  "\u0184\7n\2\2\u0184\u0185\7n\2\2\u0185\u0186\7{\2\2\u0186(\3\2\2\2\u0187" +
                  "\u0188\7h\2\2\u0188\u0189\7q\2\2\u0189\u018a\7t\2\2\u018a*\3\2\2\2\u018b" +
                  "\u018c\7k\2\2\u018c\u018d\7h\2\2\u018d,\3\2\2\2\u018e\u018f\7k\2\2\u018f" +
                  "\u0190\7p\2\2\u0190\u0191\7u\2\2\u0191\u0192\7v\2\2\u0192\u0193\7c\2\2" +
                  "\u0193\u0194\7p\2\2\u0194\u0195\7e\2\2\u0195\u0196\7g\2\2\u0196\u0197" +
                  "\7q\2\2\u0197\u0198\7h\2\2\u0198.\3\2\2\2\u0199\u019a\7k\2\2\u019a\u019b" +
                  "\7p\2\2\u019b\u019c\7v\2\2\u019c\60\3\2\2\2\u019d\u019e\7p\2\2\u019e\u019f" +
                  "\7c\2\2\u019f\u01a0\7v\2\2\u01a0\u01a1\7k\2\2\u01a1\u01a2\7x\2\2\u01a2" +
                  "\u01a3\7g\2\2\u01a3\62\3\2\2\2\u01a4\u01a5\7p\2\2\u01a5\u01a6\7g\2\2\u01a6" +
                  "\u01a7\7y\2\2\u01a7\64\3\2\2\2\u01a8\u01a9\7r\2\2\u01a9\u01aa\7t\2\2\u01aa" +
                  "\u01ab\7k\2\2\u01ab\u01ac\7x\2\2\u01ac\u01ad\7c\2\2\u01ad\u01ae\7v\2\2" +
                  "\u01ae\u01af\7g\2\2\u01af\66\3\2\2\2\u01b0\u01b1\7r\2\2\u01b1\u01b2\7" +
                  "t\2\2\u01b2\u01b3\7q\2\2\u01b3\u01b4\7v\2\2\u01b4\u01b5\7g\2\2\u01b5\u01b6" +
                  "\7e\2\2\u01b6\u01b7\7v\2\2\u01b7\u01b8\7g\2\2\u01b8\u01b9\7f\2\2\u01b9" +
                  "8\3\2\2\2\u01ba\u01bb\7r\2\2\u01bb\u01bc\7w\2\2\u01bc\u01bd\7d\2\2\u01bd" +
                  "\u01be\7n\2\2\u01be\u01bf\7k\2\2\u01bf\u01c0\7e\2\2\u01c0:\3\2\2\2\u01c1" +
                  "\u01c2\7t\2\2\u01c2\u01c3\7g\2\2\u01c3\u01c4\7v\2\2\u01c4\u01c5\7w\2\2" +
                  "\u01c5\u01c6\7t\2\2\u01c6\u01c7\7p\2\2\u01c7<\3\2\2\2\u01c8\u01c9\7u\2" +
                  "\2\u01c9\u01ca\7v\2\2\u01ca\u01cb\7c\2\2\u01cb\u01cc\7v\2\2\u01cc\u01cd" +
                  "\7k\2\2\u01cd\u01ce\7e\2\2\u01ce>\3\2\2\2\u01cf\u01d0\7u\2\2\u01d0\u01d1" +
                  "\7w\2\2\u01d1\u01d2\7r\2\2\u01d2\u01d3\7g\2\2\u01d3\u01d4\7t\2\2\u01d4" +
                  "@\3\2\2\2\u01d5\u01d6\7v\2\2\u01d6\u01d7\7j\2\2\u01d7\u01d8\7k\2\2\u01d8" +
                  "\u01d9\7u\2\2\u01d9B\3\2\2\2\u01da\u01db\7w\2\2\u01db\u01dc\7p\2\2\u01dc" +
                  "\u01dd\7k\2\2\u01dd\u01de\7v\2\2\u01deD\3\2\2\2\u01df\u01e0\7x\2\2\u01e0" +
                  "\u01e1\7c\2\2\u01e1\u01e2\7t\2\2\u01e2F\3\2\2\2\u01e3\u01e4\7x\2\2\u01e4" +
                  "\u01e5\7c\2\2\u01e5\u01e6\7n\2\2\u01e6H\3\2\2\2\u01e7\u01e8\7y\2\2\u01e8" +
                  "\u01e9\7j\2\2\u01e9\u01ea\7k\2\2\u01ea\u01eb\7n\2\2\u01eb\u01ec\7g\2\2" +
                  "\u01ecJ\3\2\2\2\u01ed\u01ee\5M\'\2\u01eeL\3\2\2\2\u01ef\u01f1\5W,\2\u01f0" +
                  "\u01f2\5U+\2\u01f1\u01f0\3\2\2\2\u01f1\u01f2\3\2\2\2\u01f2N\3\2\2\2\u01f3" +
                  "\u01f5\5e\63\2\u01f4\u01f6\5U+\2\u01f5\u01f4\3\2\2\2\u01f5\u01f6\3\2\2" +
                  "\2\u01f6P\3\2\2\2\u01f7\u01f9\5o8\2\u01f8\u01fa\5U+\2\u01f9\u01f8\3\2" +
                  "\2\2\u01f9\u01fa\3\2\2\2\u01faR\3\2\2\2\u01fb\u01fd\5y=\2\u01fc\u01fe" +
                  "\5U+\2\u01fd\u01fc\3\2\2\2\u01fd\u01fe\3\2\2\2\u01feT\3\2\2\2\u01ff\u0200" +
                  "\t\2\2\2\u0200V\3\2\2\2\u0201\u020c\7\62\2\2\u0202\u0209\5]/\2\u0203\u0205" +
                  "\5Y-\2\u0204\u0203\3\2\2\2\u0204\u0205\3\2\2\2\u0205\u020a\3\2\2\2\u0206" +
                  "\u0207\5c\62\2\u0207\u0208\5Y-\2\u0208\u020a\3\2\2\2\u0209\u0204\3\2\2" +
                  "\2\u0209\u0206\3\2\2\2\u020a\u020c\3\2\2\2\u020b\u0201\3\2\2\2\u020b\u0202" +
                  "\3\2\2\2\u020cX\3\2\2\2\u020d\u0212\5[.\2\u020e\u0210\5_\60\2\u020f\u020e" +
                  "\3\2\2\2\u020f\u0210\3\2\2\2\u0210\u0211\3\2\2\2\u0211\u0213\5[.\2\u0212" +
                  "\u020f\3\2\2\2\u0212\u0213\3\2\2\2\u0213Z\3\2\2\2\u0214\u0217\7\62\2\2" +
                  "\u0215\u0217\5]/\2\u0216\u0214\3\2\2\2\u0216\u0215\3\2\2\2\u0217\\\3\2" +
                  "\2\2\u0218\u0219\t\3\2\2\u0219^\3\2\2\2\u021a\u021c\5a\61\2\u021b\u021a" +
                  "\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021b\3\2\2\2\u021d\u021e\3\2\2\2\u021e" +
                  "`\3\2\2\2\u021f\u0222\5[.\2\u0220\u0222\7a\2\2\u0221\u021f\3\2\2\2\u0221" +
                  "\u0220\3\2\2\2\u0222b\3\2\2\2\u0223\u0225\7a\2\2\u0224\u0223\3\2\2\2\u0225" +
                  "\u0226\3\2\2\2\u0226\u0224\3\2\2\2\u0226\u0227\3\2\2\2\u0227d\3\2\2\2" +
                  "\u0228\u0229\7\62\2\2\u0229\u022a\t\4\2\2\u022a\u022b\5g\64\2\u022bf\3" +
                  "\2\2\2\u022c\u0231\5i\65\2\u022d\u022f\5k\66\2\u022e\u022d\3\2\2\2\u022e" +
                  "\u022f\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0232\5i\65\2\u0231\u022e\3\2" +
                  "\2\2\u0231\u0232\3\2\2\2\u0232h\3\2\2\2\u0233\u0234\t\5\2\2\u0234j\3\2" +
                  "\2\2\u0235\u0237\5m\67\2\u0236\u0235\3\2\2\2\u0237\u0238\3\2\2\2\u0238" +
                  "\u0236\3\2\2\2\u0238\u0239\3\2\2\2\u0239l\3\2\2\2\u023a\u023d\5i\65\2" +
                  "\u023b\u023d\7a\2\2\u023c\u023a\3\2\2\2\u023c\u023b\3\2\2\2\u023dn\3\2" +
                  "\2\2\u023e\u0240\7\62\2\2\u023f\u0241\5c\62\2\u0240\u023f\3\2\2\2\u0240" +
                  "\u0241\3\2\2\2\u0241\u0242\3\2\2\2\u0242\u0243\5q9\2\u0243p\3\2\2\2\u0244" +
                  "\u0249\5s:\2\u0245\u0247\5u;\2\u0246\u0245\3\2\2\2\u0246\u0247\3\2\2\2" +
                  "\u0247\u0248\3\2\2\2\u0248\u024a\5s:\2\u0249\u0246\3\2\2\2\u0249\u024a" +
                  "\3\2\2\2\u024ar\3\2\2\2\u024b\u024c\t\6\2\2\u024ct\3\2\2\2\u024d\u024f" +
                  "\5w<\2\u024e\u024d\3\2\2\2\u024f\u0250\3\2\2\2\u0250\u024e\3\2\2\2\u0250" +
                  "\u0251\3\2\2\2\u0251v\3\2\2\2\u0252\u0255\5s:\2\u0253\u0255\7a\2\2\u0254" +
                  "\u0252\3\2\2\2\u0254\u0253\3\2\2\2\u0255x\3\2\2\2\u0256\u0257\7\62\2\2" +
                  "\u0257\u0258\t\7\2\2\u0258\u0259\5{>\2\u0259z\3\2\2\2\u025a\u025f\5}?" +
                  "\2\u025b\u025d\5\177@\2\u025c\u025b\3\2\2\2\u025c\u025d\3\2\2\2\u025d" +
                  "\u025e\3\2\2\2\u025e\u0260\5}?\2\u025f\u025c\3\2\2\2\u025f\u0260\3\2\2" +
                  "\2\u0260|\3\2\2\2\u0261\u0262\t\b\2\2\u0262~\3\2\2\2\u0263\u0265\5\u0081" +
                  "A\2\u0264\u0263\3\2\2\2\u0265\u0266\3\2\2\2\u0266\u0264\3\2\2\2\u0266" +
                  "\u0267\3\2\2\2\u0267\u0080\3\2\2\2\u0268\u026b\5}?\2\u0269\u026b\7a\2" +
                  "\2\u026a\u0268\3\2\2\2\u026a\u0269\3\2\2\2\u026b\u0082\3\2\2\2\u026c\u026d" +
                  "\t\t\2\2\u026d\u0084\3\2\2\2\u026e\u0270\5\u0087D\2\u026f\u026e\3\2\2" +
                  "\2\u026f\u0270\3\2\2\2\u0270\u0271\3\2\2\2\u0271\u0272\5Y-\2\u0272\u0086" +
                  "\3\2\2\2\u0273\u0274\t\n\2\2\u0274\u0088\3\2\2\2\u0275\u0277\5e\63\2\u0276" +
                  "\u0278\7\60\2\2\u0277\u0276\3\2\2\2\u0277\u0278\3\2\2\2\u0278\u0281\3" +
                  "\2\2\2\u0279\u027a\7\62\2\2\u027a\u027c\t\4\2\2\u027b\u027d\5g\64\2\u027c" +
                  "\u027b\3\2\2\2\u027c\u027d\3\2\2\2\u027d\u027e\3\2\2\2\u027e\u027f\7\60" +
                  "\2\2\u027f\u0281\5g\64\2\u0280\u0275\3\2\2\2\u0280\u0279\3\2\2\2\u0281" +
                  "\u008a\3\2\2\2\u0282\u0283\5\u008dG\2\u0283\u0284\5\u0085C\2\u0284\u008c" +
                  "\3\2\2\2\u0285\u0286\t\13\2\2\u0286\u008e\3\2\2\2\u0287\u0288\7v\2\2\u0288" +
                  "\u0289\7t\2\2\u0289\u028a\7w\2\2\u028a\u0291\7g\2\2\u028b\u028c\7h\2\2" +
                  "\u028c\u028d\7c\2\2\u028d\u028e\7n\2\2\u028e\u028f\7u\2\2\u028f\u0291" +
                  "\7g\2\2\u0290\u0287\3\2\2\2\u0290\u028b\3\2\2\2\u0291\u0090\3\2\2\2\u0292" +
                  "\u0293\7)\2\2\u0293\u0294\5\u0093J\2\u0294\u0295\7)\2\2\u0295\u029b\3" +
                  "\2\2\2\u0296\u0297\7)\2\2\u0297\u0298\5\u009bN\2\u0298\u0299\7)\2\2\u0299" +
                  "\u029b\3\2\2\2\u029a\u0292\3\2\2\2\u029a\u0296\3\2\2\2\u029b\u0092\3\2" +
                  "\2\2\u029c\u029d\n\f\2\2\u029d\u0094\3\2\2\2\u029e\u02a0\7$\2\2\u029f" +
                  "\u02a1\5\u0097L\2\u02a0\u029f\3\2\2\2\u02a0\u02a1\3\2\2\2\u02a1\u02a2" +
                  "\3\2\2\2\u02a2\u02a3\7$\2\2\u02a3\u0096\3\2\2\2\u02a4\u02a6\5\u0099M\2" +
                  "\u02a5\u02a4\3\2\2\2\u02a6\u02a7\3\2\2\2\u02a7\u02a5\3\2\2\2\u02a7\u02a8" +
                  "\3\2\2\2\u02a8\u0098\3\2\2\2\u02a9\u02ac\n\r\2\2\u02aa\u02ac\5\u009bN" +
                  "\2\u02ab\u02a9\3\2\2\2\u02ab\u02aa\3\2\2\2\u02ac\u009a\3\2\2\2\u02ad\u02ae" +
                  "\7^\2\2\u02ae\u02b2\t\16\2\2\u02af\u02b2\5\u009dO\2\u02b0\u02b2\5\u00a1" +
                  "Q\2\u02b1\u02ad\3\2\2\2\u02b1\u02af\3\2\2\2\u02b1\u02b0\3\2\2\2\u02b2" +
                  "\u009c\3\2\2\2\u02b3\u02b4\7^\2\2\u02b4\u02bf\5s:\2\u02b5\u02b6\7^\2\2" +
                  "\u02b6\u02b7\5s:\2\u02b7\u02b8\5s:\2\u02b8\u02bf\3\2\2\2\u02b9\u02ba\7" +
                  "^\2\2\u02ba\u02bb\5\u009fP\2\u02bb\u02bc\5s:\2\u02bc\u02bd\5s:\2\u02bd" +
                  "\u02bf\3\2\2\2\u02be\u02b3\3\2\2\2\u02be\u02b5\3\2\2\2\u02be\u02b9\3\2" +
                  "\2\2\u02bf\u009e\3\2\2\2\u02c0\u02c1\t\17\2\2\u02c1\u00a0\3\2\2\2\u02c2" +
                  "\u02c3\7^\2\2\u02c3\u02c4\7w\2\2\u02c4\u02c5\5i\65\2\u02c5\u02c6\5i\65" +
                  "\2\u02c6\u02c7\5i\65\2\u02c7\u02c8\5i\65\2\u02c8\u00a2\3\2\2\2\u02c9\u02ca" +
                  "\7p\2\2\u02ca\u02cb\7w\2\2\u02cb\u02cc\7n\2\2\u02cc\u02cd\7n\2\2\u02cd" +
                  "\u00a4\3\2\2\2\u02ce\u02cf\7*\2\2\u02cf\u00a6\3\2\2\2\u02d0\u02d1\7+\2" +
                  "\2\u02d1\u00a8\3\2\2\2\u02d2\u02d3\7}\2\2\u02d3\u00aa\3\2\2\2\u02d4\u02d5" +
                  "\7\177\2\2\u02d5\u00ac\3\2\2\2\u02d6\u02d7\7]\2\2\u02d7\u00ae\3\2\2\2" +
                  "\u02d8\u02d9\7_\2\2\u02d9\u00b0\3\2\2\2\u02da\u02db\7=\2\2\u02db\u00b2" +
                  "\3\2\2\2\u02dc\u02dd\7.\2\2\u02dd\u00b4\3\2\2\2\u02de\u02df\7\60\2\2\u02df" +
                  "\u00b6\3\2\2\2\u02e0\u02e1\7?\2\2\u02e1\u00b8\3\2\2\2\u02e2\u02e3\7@\2" +
                  "\2\u02e3\u00ba\3\2\2\2\u02e4\u02e5\7>\2\2\u02e5\u00bc\3\2\2\2\u02e6\u02e7" +
                  "\7#\2\2\u02e7\u00be\3\2\2\2\u02e8\u02e9\7\u0080\2\2\u02e9\u00c0\3\2\2" +
                  "\2\u02ea\u02eb\7A\2\2\u02eb\u00c2\3\2\2\2\u02ec\u02ed\7<\2\2\u02ed\u00c4" +
                  "\3\2\2\2\u02ee\u02ef\7?\2\2\u02ef\u02f0\7?\2\2\u02f0\u00c6\3\2\2\2\u02f1" +
                  "\u02f2\7>\2\2\u02f2\u02f3\7?\2\2\u02f3\u00c8\3\2\2\2\u02f4\u02f5\7@\2" +
                  "\2\u02f5\u02f6\7?\2\2\u02f6\u00ca\3\2\2\2\u02f7\u02f8\7#\2\2\u02f8\u02f9" +
                  "\7?\2\2\u02f9\u00cc\3\2\2\2\u02fa\u02fb\7(\2\2\u02fb\u02fc\7(\2\2\u02fc" +
                  "\u00ce\3\2\2\2\u02fd\u02fe\7~\2\2\u02fe\u02ff\7~\2\2\u02ff\u00d0\3\2\2" +
                  "\2\u0300\u0301\7-\2\2\u0301\u0302\7-\2\2\u0302\u00d2\3\2\2\2\u0303\u0304" +
                  "\7/\2\2\u0304\u0305\7/\2\2\u0305\u00d4\3\2\2\2\u0306\u0307\7-\2\2\u0307" +
                  "\u00d6\3\2\2\2\u0308\u0309\7/\2\2\u0309\u00d8\3\2\2\2\u030a\u030b\7,\2" +
                  "\2\u030b\u00da\3\2\2\2\u030c\u030d\7\61\2\2\u030d\u00dc\3\2\2\2\u030e" +
                  "\u030f\7(\2\2\u030f\u00de\3\2\2\2\u0310\u0311\7~\2\2\u0311\u00e0\3\2\2" +
                  "\2\u0312\u0313\7`\2\2\u0313\u00e2\3\2\2\2\u0314\u0315\7\'\2\2\u0315\u00e4" +
                  "\3\2\2\2\u0316\u0317\7/\2\2\u0317\u0318\7@\2\2\u0318\u00e6\3\2\2\2\u0319" +
                  "\u031a\7<\2\2\u031a\u031b\7<\2\2\u031b\u00e8\3\2\2\2\u031c\u031d\7-\2" +
                  "\2\u031d\u031e\7?\2\2\u031e\u00ea\3\2\2\2\u031f\u0320\7/\2\2\u0320\u0321" +
                  "\7?\2\2\u0321\u00ec\3\2\2\2\u0322\u0323\7,\2\2\u0323\u0324\7?\2\2\u0324" +
                  "\u00ee\3\2\2\2\u0325\u0326\7\61\2\2\u0326\u0327\7?\2\2\u0327\u00f0\3\2" +
                  "\2\2\u0328\u0329\7(\2\2\u0329\u032a\7?\2\2\u032a\u00f2\3\2\2\2\u032b\u032c" +
                  "\7~\2\2\u032c\u032d\7?\2\2\u032d\u00f4\3\2\2\2\u032e\u032f\7`\2\2\u032f" +
                  "\u0330\7?\2\2\u0330\u00f6\3\2\2\2\u0331\u0332\7\'\2\2\u0332\u0333\7?\2" +
                  "\2\u0333\u00f8\3\2\2\2\u0334\u0335\7>\2\2\u0335\u0336\7>\2\2\u0336\u0337" +
                  "\7?\2\2\u0337\u00fa\3\2\2\2\u0338\u0339\7@\2\2\u0339\u033a\7@\2\2\u033a" +
                  "\u033b\7?\2\2\u033b\u00fc\3\2\2\2\u033c\u033d\7@\2\2\u033d\u033e\7@\2" +
                  "\2\u033e\u033f\7@\2\2\u033f\u0340\7?\2\2\u0340\u00fe\3\2\2\2\u0341\u0345" +
                  "\5\u0101\u0081\2\u0342\u0344\5\u0103\u0082\2\u0343\u0342\3\2\2\2\u0344" +
                  "\u0347\3\2\2\2\u0345\u0343\3\2\2\2\u0345\u0346\3\2\2\2\u0346\u0100\3\2" +
                  "\2\2\u0347\u0345\3\2\2\2\u0348\u034f\t\20\2\2\u0349\u034a\n\21\2\2\u034a" +
                  "\u034f\6\u0081\2\2\u034b\u034c\t\22\2\2\u034c\u034d\t\23\2\2\u034d\u034f" +
                  "\6\u0081\3\2\u034e\u0348\3\2\2\2\u034e\u0349\3\2\2\2\u034e\u034b\3\2\2" +
                  "\2\u034f\u0102\3\2\2\2\u0350\u0357\t\24\2\2\u0351\u0352\n\21\2\2\u0352" +
                  "\u0357\6\u0082\4\2\u0353\u0354\t\22\2\2\u0354\u0355\t\23\2\2\u0355\u0357" +
                  "\6\u0082\5\2\u0356\u0350\3\2\2\2\u0356\u0351\3\2\2\2\u0356\u0353\3\2\2" +
                  "\2\u0357\u0104\3\2\2\2\u0358\u0359\7B\2\2\u0359\u0106\3\2\2\2\u035a\u035b" +
                  "\7\60\2\2\u035b\u035c\7\60\2\2\u035c\u035d\7\60\2\2\u035d\u0108\3\2\2" +
                  "\2\u035e\u035f\t\25\2\2\u035f\u0360\3\2\2\2\u0360\u0361\b\u0085\2\2\u0361" +
                  "\u010a\3\2\2\2\u0362\u0363\7\61\2\2\u0363\u0364\7,\2\2\u0364\u0368\3\2" +
                  "\2\2\u0365\u0367\13\2\2\2\u0366\u0365\3\2\2\2\u0367\u036a\3\2\2\2\u0368" +
                  "\u0369\3\2\2\2\u0368\u0366\3\2\2\2\u0369\u036b\3\2\2\2\u036a\u0368\3\2" +
                  "\2\2\u036b\u036c\7,\2\2\u036c\u036d\7\61\2\2\u036d\u036e\3\2\2\2\u036e" +
                  "\u036f\b\u0086\2\2\u036f\u010c\3\2\2\2\u0370\u0371\7\61\2\2\u0371\u0372" +
                  "\7\61\2\2\u0372\u0376\3\2\2\2\u0373\u0375\n\26\2\2\u0374\u0373\3\2\2\2" +
                  "\u0375\u0378\3\2\2\2\u0376\u0374\3\2\2\2\u0376\u0377\3\2\2\2\u0377\u0379" +
                  "\3\2\2\2\u0378\u0376\3\2\2\2\u0379\u037a\b\u0087\2\2\u037a\u010e\3\2\2" +
                  "\2-\2\u01f1\u01f5\u01f9\u01fd\u0204\u0209\u020b\u020f\u0212\u0216\u021d" +
                  "\u0221\u0226\u022e\u0231\u0238\u023c\u0240\u0246\u0249\u0250\u0254\u025c" +
                  "\u025f\u0266\u026a\u026f\u0277\u027c\u0280\u0290\u029a\u02a0\u02a7\u02ab" +
                  "\u02b1\u02be\u0345\u034e\u0356\u0368\u0376\3\b\2\2";
  public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}