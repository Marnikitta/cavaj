// Generated from org/marnikitta/cavaj/parser/antlr4/Cavaj.g4 by ANTLR 4.6
package org.marnikitta.cavaj.parser.antlr4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CavajParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, ABSTRACT=6, ASSERT=7, BOOLEAN=8, 
		BREAK=9, CATCH=10, CLASS=11, CONTINUE=12, DEFAULT=13, DO=14, ELSE=15, 
		EXTENDS=16, FI=17, FINAL=18, FINALLY=19, FOR=20, IF=21, INSTANCEOF=22, 
		INT=23, NATIVE=24, NEW=25, PRIVATE=26, PROTECTED=27, PUBLIC=28, RETURN=29, 
		STATIC=30, SUPER=31, THIS=32, UNIT=33, VAR=34, VAL=35, WHILE=36, IntegerLiteral=37,
					BooleanLiteral = 38, CharacterLiteral = 39, StringLiteral = 40, NullLiteral = 41,
					LPAREN = 42, RPAREN = 43, LBRACE = 44, RBRACE = 45, LBRACK = 46, RBRACK = 47, SEMI = 48,
					COMMA = 49, DOT = 50, ASSIGN = 51, GT = 52, LT = 53, BANG = 54, TILDE = 55, QUESTION = 56,
					COLON = 57, EQUAL = 58, LE = 59, GE = 60, NOTEQUAL = 61, AND = 62, OR = 63, INC = 64,
					DEC = 65, ADD = 66, SUB = 67, MUL = 68, DIV = 69, BITAND = 70, BITOR = 71, CARET = 72,
					MOD = 73, ARROW = 74, COLONCOLON = 75, ADD_ASSIGN = 76, SUB_ASSIGN = 77, MUL_ASSIGN = 78,
					DIV_ASSIGN = 79, AND_ASSIGN = 80, OR_ASSIGN = 81, XOR_ASSIGN = 82, MOD_ASSIGN = 83,
					LSHIFT_ASSIGN = 84, RSHIFT_ASSIGN = 85, URSHIFT_ASSIGN = 86, Identifier = 87,
					AT = 88, ELLIPSIS = 89, WS = 90, COMMENT = 91, LINE_COMMENT = 92;
	public static final int
					RULE_literal = 0, RULE_integerLiteral = 1, RULE_booleanLiteral = 2, RULE_stringLiteral = 3,
					RULE_nullLiteral = 4, RULE_classType = 5, RULE_variableName = 6, RULE_methodName = 7,
					RULE_compilationUnit = 8, RULE_classDeclaration = 9, RULE_classParamClause = 10,
					RULE_classParams = 11, RULE_valVar = 12, RULE_classParam = 13, RULE_superclass = 14,
					RULE_classBody = 15, RULE_classBodyDeclaration = 16, RULE_classMemberDeclaration = 17,
					RULE_variableDeclaration = 18, RULE_variableDeclaratorId = 19, RULE_variableInitializer = 20,
					RULE_methodDeclaration = 21, RULE_methodModifier = 22, RULE_methodHeader = 23,
					RULE_formalParameters = 24, RULE_formalParameter = 25, RULE_instanceInitializer = 26,
					RULE_staticInitializer = 27, RULE_constructorDeclaration = 28, RULE_constructorHeader = 29,
					RULE_block = 30, RULE_blockStatements = 31, RULE_blockStatement = 32,
					RULE_resultExpression = 33, RULE_expression = 34, RULE_constructorThisExpression = 35,
					RULE_localVariableDeclaration = 36, RULE_returnExpression = 37, RULE_ifThenExpression = 38,
					RULE_ifThenElseExpression = 39, RULE_whileExpression = 40, RULE_forExpression = 41,
					RULE_forInit = 42, RULE_forCondition = 43, RULE_forUpdate = 44, RULE_throwExpression = 45,
					RULE_tryExpression = 46, RULE_catches = 47, RULE_catchClause = 48, RULE_catchFormalParameter = 49,
					RULE_catchType = 50, RULE_finally_ = 51, RULE_primary = 52, RULE_primary_tmp = 53,
					RULE_primary_lfno_primary = 54, RULE_thisExpression = 55, RULE_unitExpression = 56,
					RULE_classInstanceCreationExpression = 57, RULE_fieldAccess = 58, RULE_methodInvocation = 59,
					RULE_methodInvocation_lf_primary = 60, RULE_methodInvocation_lfno_primary = 61,
					RULE_localMethodInvocation = 62, RULE_staticMethodInvocation = 63, RULE_variableMethodInvocaton = 64,
					RULE_superMethodInvocation = 65, RULE_superClassMethodInvocation = 66,
					RULE_argumentList = 67, RULE_assignment = 68, RULE_leftHandSide = 69,
					RULE_assignmentOperator = 70, RULE_conditionalOrExpression = 71, RULE_conditionalAndExpression = 72,
					RULE_inclusiveOrExpression = 73, RULE_exclusiveOrExpression = 74, RULE_andExpression = 75,
					RULE_equalityExpression = 76, RULE_relationalExpression = 77, RULE_shiftExpression = 78,
					RULE_additiveExpression = 79, RULE_multiplicativeExpression = 80, RULE_unaryExpression = 81,
					RULE_preIncrementExpression = 82, RULE_preDecrementExpression = 83, RULE_unaryExpressionNotPlusMinus = 84,
					RULE_postfixExpression = 85, RULE_postIncrementExpression = 86, RULE_postIncrementExpression_lf_postfixExpression = 87,
					RULE_postDecrementExpression = 88, RULE_postDecrementExpression_lf_postfixExpression = 89,
					RULE_castExpression = 90;
	public static final String[] ruleNames = {
					"literal", "integerLiteral", "booleanLiteral", "stringLiteral", "nullLiteral",
					"classType", "variableName", "methodName", "compilationUnit", "classDeclaration",
					"classParamClause", "classParams", "valVar", "classParam", "superclass",
					"classBody", "classBodyDeclaration", "classMemberDeclaration", "variableDeclaration",
					"variableDeclaratorId", "variableInitializer", "methodDeclaration", "methodModifier",
					"methodHeader", "formalParameters", "formalParameter", "instanceInitializer",
					"staticInitializer", "constructorDeclaration", "constructorHeader", "block",
					"blockStatements", "blockStatement", "resultExpression", "expression",
		"constructorThisExpression", "localVariableDeclaration", "returnExpression",
					"ifThenExpression", "ifThenElseExpression", "whileExpression", "forExpression",
					"forInit", "forCondition", "forUpdate", "throwExpression", "tryExpression",
					"catches", "catchClause", "catchFormalParameter", "catchType", "finally_",
					"primary", "primary_tmp", "primary_lfno_primary", "thisExpression", "unitExpression",
					"classInstanceCreationExpression", "fieldAccess", "methodInvocation",
					"methodInvocation_lf_primary", "methodInvocation_lfno_primary", "localMethodInvocation",
					"staticMethodInvocation", "variableMethodInvocaton", "superMethodInvocation",
					"superClassMethodInvocation", "argumentList", "assignment", "leftHandSide",
					"assignmentOperator", "conditionalOrExpression", "conditionalAndExpression",
					"inclusiveOrExpression", "exclusiveOrExpression", "andExpression", "equalityExpression",
					"relationalExpression", "shiftExpression", "additiveExpression", "multiplicativeExpression",
					"unaryExpression", "preIncrementExpression", "preDecrementExpression",
					"unaryExpressionNotPlusMinus", "postfixExpression", "postIncrementExpression",
					"postIncrementExpression_lf_postfixExpression", "postDecrementExpression",
					"postDecrementExpression_lf_postfixExpression", "castExpression"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'case'", "'def'", "'override'", "'throw'", "'try'", "'abstract'", 
		"'assert'", "'boolean'", "'break'", "'catch'", "'class'", "'continue'", 
		"'default'", "'do'", "'else'", "'extends'", "'fi'", "'final'", "'finally'", 
		"'for'", "'if'", "'instanceof'", "'int'", "'native'", "'new'", "'private'", 
		"'protected'", "'public'", "'return'", "'static'", "'super'", "'this'",
					"'unit'", "'var'", "'val'", "'while'", null, null, null, null, "'null'",
		"'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "','", "'.'", "'='", 
		"'>'", "'<'", "'!'", "'~'", "'?'", "':'", "'=='", "'<='", "'>='", "'!='", 
		"'&&'", "'||'", "'++'", "'--'", "'+'", "'-'", "'*'", "'/'", "'&'", "'|'", 
		"'^'", "'%'", "'->'", "'::'", "'+='", "'-='", "'*='", "'/='", "'&='", 
		"'|='", "'^='", "'%='", "'<<='", "'>>='", "'>>>='", null, "'@'", "'...'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", 
		"CATCH", "CLASS", "CONTINUE", "DEFAULT", "DO", "ELSE", "EXTENDS", "FI", 
		"FINAL", "FINALLY", "FOR", "IF", "INSTANCEOF", "INT", "NATIVE", "NEW", 
		"PRIVATE", "PROTECTED", "PUBLIC", "RETURN", "STATIC", "SUPER", "THIS",
					"UNIT", "VAR", "VAL", "WHILE", "IntegerLiteral", "BooleanLiteral", "CharacterLiteral",
					"StringLiteral", "NullLiteral", "LPAREN", "RPAREN", "LBRACE", "RBRACE",
					"LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", "GT", "LT", "BANG",
					"TILDE", "QUESTION", "COLON", "EQUAL", "LE", "GE", "NOTEQUAL", "AND",
					"OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "BITAND", "BITOR", "CARET",
					"MOD", "ARROW", "COLONCOLON", "ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN",
					"DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN", "LSHIFT_ASSIGN",
					"RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "Identifier", "AT", "ELLIPSIS", "WS",
					"COMMENT", "LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Cavaj.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CavajParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LiteralContext extends ParserRuleContext {
		public IntegerLiteralContext integerLiteral() {
			return getRuleContext(IntegerLiteralContext.class, 0);
		}

		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class, 0);
		}

		public StringLiteralContext stringLiteral() {
			return getRuleContext(StringLiteralContext.class, 0);
		}

		public NullLiteralContext nullLiteral() {
			return getRuleContext(NullLiteralContext.class, 0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_literal);
		try {
			setState(186);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case IntegerLiteral:
					enterOuterAlt(_localctx, 1);
				{
					setState(182);
					integerLiteral();
				}
				break;
				case BooleanLiteral:
					enterOuterAlt(_localctx, 2);
				{
					setState(183);
					booleanLiteral();
				}
				break;
				case StringLiteral:
					enterOuterAlt(_localctx, 3);
				{
					setState(184);
					stringLiteral();
				}
				break;
				case NullLiteral:
					enterOuterAlt(_localctx, 4);
				{
					setState(185);
					nullLiteral();
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerLiteralContext extends ParserRuleContext {
		public TerminalNode IntegerLiteral() {
			return getToken(CavajParser.IntegerLiteral, 0);
		}

		public IntegerLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_integerLiteral;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterIntegerLiteral(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitIntegerLiteral(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitIntegerLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerLiteralContext integerLiteral() throws RecognitionException {
		IntegerLiteralContext _localctx = new IntegerLiteralContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_integerLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(188);
				match(IntegerLiteral);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralContext extends ParserRuleContext {
		public TerminalNode BooleanLiteral() {
			return getToken(CavajParser.BooleanLiteral, 0);
		}

		public BooleanLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_booleanLiteral;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterBooleanLiteral(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitBooleanLiteral(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitBooleanLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanLiteralContext booleanLiteral() throws RecognitionException {
		BooleanLiteralContext _localctx = new BooleanLiteralContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_booleanLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(190);
				match(BooleanLiteral);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralContext extends ParserRuleContext {
		public TerminalNode StringLiteral() {
			return getToken(CavajParser.StringLiteral, 0);
		}

		public StringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_stringLiteral;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterStringLiteral(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitStringLiteral(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitStringLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringLiteralContext stringLiteral() throws RecognitionException {
		StringLiteralContext _localctx = new StringLiteralContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_stringLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(192);
				match(StringLiteral);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullLiteralContext extends ParserRuleContext {
		public TerminalNode NullLiteral() {
			return getToken(CavajParser.NullLiteral, 0);
		}

		public NullLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_nullLiteral;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterNullLiteral(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitNullLiteral(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitNullLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullLiteralContext nullLiteral() throws RecognitionException {
		NullLiteralContext _localctx = new NullLiteralContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_nullLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(194);
				match(NullLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassTypeContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }
		public ClassTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassTypeContext classType() throws RecognitionException {
		ClassTypeContext _localctx = new ClassTypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_classType);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(196);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableNameContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }

		public VariableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_variableName;
		}
		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterVariableName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitVariableName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitVariableName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableNameContext variableName() throws RecognitionException {
		VariableNameContext _localctx = new VariableNameContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_variableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(198);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodNameContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }
		public MethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodNameContext methodName() throws RecognitionException {
		MethodNameContext _localctx = new MethodNameContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_methodName);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(200);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompilationUnitContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(CavajParser.EOF, 0); }
		public List<ClassDeclarationContext> classDeclaration() {
			return getRuleContexts(ClassDeclarationContext.class);
		}
		public ClassDeclarationContext classDeclaration(int i) {
			return getRuleContext(ClassDeclarationContext.class,i);
		}
		public CompilationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compilationUnit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCompilationUnit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCompilationUnit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCompilationUnit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompilationUnitContext compilationUnit() throws RecognitionException {
		CompilationUnitContext _localctx = new CompilationUnitContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_compilationUnit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(205);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0 || _la==CLASS) {
				{
				{
					setState(202);
				classDeclaration();
				}
				}
				setState(207);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
				setState(208);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }
		public ClassParamClauseContext classParamClause() {
			return getRuleContext(ClassParamClauseContext.class,0);
		}
		public SuperclassContext superclass() {
			return getRuleContext(SuperclassContext.class,0);
		}
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_classDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(211);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
					setState(210);
				match(T__0);
				}
			}

				setState(213);
			match(CLASS);
				setState(214);
			match(Identifier);
				setState(216);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LPAREN) {
				{
					setState(215);
				classParamClause();
				}
			}

				setState(219);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
					setState(218);
				superclass();
				}
			}

				setState(222);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LBRACE) {
				{
					setState(221);
				classBody();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassParamClauseContext extends ParserRuleContext {
		public ClassParamsContext classParams() {
			return getRuleContext(ClassParamsContext.class,0);
		}
		public ClassParamClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classParamClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassParamClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassParamClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassParamClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassParamClauseContext classParamClause() throws RecognitionException {
		ClassParamClauseContext _localctx = new ClassParamClauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_classParamClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(224);
			match(LPAREN);
				setState(226);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 34)) & ~0x3f) == 0 && ((1L << (_la - 34)) & ((1L << (VAR - 34)) | (1L << (VAL - 34)) | (1L << (Identifier - 34)))) != 0)) {
				{
					setState(225);
				classParams();
				}
			}

				setState(228);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassParamsContext extends ParserRuleContext {
		public List<ClassParamContext> classParam() {
			return getRuleContexts(ClassParamContext.class);
		}
		public ClassParamContext classParam(int i) {
			return getRuleContext(ClassParamContext.class,i);
		}
		public ClassParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classParams; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassParamsContext classParams() throws RecognitionException {
		ClassParamsContext _localctx = new ClassParamsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_classParams);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(230);
			classParam();
				setState(235);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
					setState(231);
				match(COMMA);
					setState(232);
				classParam();
				}
				}
				setState(237);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValVarContext extends ParserRuleContext {
		public TerminalNode VAL() { return getToken(CavajParser.VAL, 0); }
		public TerminalNode VAR() { return getToken(CavajParser.VAR, 0); }
		public ValVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterValVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitValVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitValVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValVarContext valVar() throws RecognitionException {
		ValVarContext _localctx = new ValVarContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_valVar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(238);
			_la = _input.LA(1);
			if ( !(_la==VAR || _la==VAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassParamContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public ValVarContext valVar() {
			return getRuleContext(ValVarContext.class,0);
		}
		public ClassParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classParam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassParam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassParamContext classParam() throws RecognitionException {
		ClassParamContext _localctx = new ClassParamContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_classParam);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(241);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VAR || _la==VAL) {
				{
					setState(240);
				valVar();
				}
			}

				setState(243);
			match(Identifier);
				setState(244);
			match(COLON);
				setState(245);
			classType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperclassContext extends ParserRuleContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public SuperclassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_superclass; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterSuperclass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitSuperclass(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitSuperclass(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SuperclassContext superclass() throws RecognitionException {
		SuperclassContext _localctx = new SuperclassContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_superclass);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(247);
			match(EXTENDS);
				setState(248);
			classType();
				setState(253);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LPAREN) {
				{
					setState(249);
				match(LPAREN);
					setState(250);
				argumentList();
					setState(251);
				match(RPAREN);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyContext extends ParserRuleContext {
		public List<ClassBodyDeclarationContext> classBodyDeclaration() {
			return getRuleContexts(ClassBodyDeclarationContext.class);
		}
		public ClassBodyDeclarationContext classBodyDeclaration(int i) {
			return getRuleContext(ClassBodyDeclarationContext.class,i);
		}
		public ClassBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassBodyContext classBody() throws RecognitionException {
		ClassBodyContext _localctx = new ClassBodyContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_classBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(255);
			match(LBRACE);
				setState(259);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << FINAL) | (1L << NATIVE) | (1L << PRIVATE) | (1L << PROTECTED) | (1L << PUBLIC) | (1L << STATIC) | (1L << VAR) | (1L << VAL) | (1L << LBRACE) | (1L << SEMI))) != 0)) {
				{
				{
					setState(256);
				classBodyDeclaration();
				}
				}
				setState(261);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
				setState(262);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyDeclarationContext extends ParserRuleContext {
		public ClassMemberDeclarationContext classMemberDeclaration() {
			return getRuleContext(ClassMemberDeclarationContext.class,0);
		}
		public InstanceInitializerContext instanceInitializer() {
			return getRuleContext(InstanceInitializerContext.class,0);
		}
		public StaticInitializerContext staticInitializer() {
			return getRuleContext(StaticInitializerContext.class,0);
		}
		public ConstructorDeclarationContext constructorDeclaration() {
			return getRuleContext(ConstructorDeclarationContext.class,0);
		}
		public ClassBodyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBodyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassBodyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassBodyDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassBodyDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassBodyDeclarationContext classBodyDeclaration() throws RecognitionException {
		ClassBodyDeclarationContext _localctx = new ClassBodyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_classBodyDeclaration);
		try {
			setState(268);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 11, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(264);
				classMemberDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(265);
				instanceInitializer();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(266);
				staticInitializer();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(267);
				constructorDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassMemberDeclarationContext extends ParserRuleContext {
		public VariableDeclarationContext variableDeclaration() {
			return getRuleContext(VariableDeclarationContext.class,0);
		}
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public ClassMemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classMemberDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassMemberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassMemberDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassMemberDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassMemberDeclarationContext classMemberDeclaration() throws RecognitionException {
		ClassMemberDeclarationContext _localctx = new ClassMemberDeclarationContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_classMemberDeclaration);
		try {
			setState(273);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VAR:
			case VAL:
				enterOuterAlt(_localctx, 1);
				{
					setState(270);
				variableDeclaration();
				}
				break;
			case T__1:
			case T__2:
			case FINAL:
			case NATIVE:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case STATIC:
				enterOuterAlt(_localctx, 2);
				{
					setState(271);
				methodDeclaration();
				}
				break;
			case SEMI:
				enterOuterAlt(_localctx, 3);
				{
					setState(272);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public ValVarContext valVar() {
			return getRuleContext(ValVarContext.class,0);
		}
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public VariableInitializerContext variableInitializer() {
			return getRuleContext(VariableInitializerContext.class,0);
		}
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitVariableDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitVariableDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_variableDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(275);
			valVar();
				setState(276);
			variableDeclaratorId();
				setState(285);
			_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 14, _ctx)) {
			case 1:
				{
				{
					setState(279);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLON) {
					{
						setState(277);
					match(COLON);
						setState(278);
					classType();
					}
				}

				{
					setState(281);
				match(ASSIGN);
					setState(282);
				variableInitializer();
				}
				}
				}
				break;
			case 2:
				{
				{
					setState(283);
				match(COLON);
					setState(284);
				classType();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorIdContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(CavajParser.Identifier, 0); }
		public VariableDeclaratorIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaratorId; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterVariableDeclaratorId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitVariableDeclaratorId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitVariableDeclaratorId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDeclaratorIdContext variableDeclaratorId() throws RecognitionException {
		VariableDeclaratorIdContext _localctx = new VariableDeclaratorIdContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_variableDeclaratorId);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(287);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableInitializerContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterVariableInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitVariableInitializer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitVariableInitializer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableInitializerContext variableInitializer() throws RecognitionException {
		VariableInitializerContext _localctx = new VariableInitializerContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_variableInitializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(289);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public MethodHeaderContext methodHeader() {
			return getRuleContext(MethodHeaderContext.class,0);
		}
		public List<MethodModifierContext> methodModifier() {
			return getRuleContexts(MethodModifierContext.class);
		}
		public MethodModifierContext methodModifier(int i) {
			return getRuleContext(MethodModifierContext.class,i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration() throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_methodDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(294);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << FINAL) | (1L << NATIVE) | (1L << PRIVATE) | (1L << PROTECTED) | (1L << PUBLIC) | (1L << STATIC))) != 0)) {
				{
				{
					setState(291);
				methodModifier();
				}
				}
				setState(296);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
				setState(297);
			match(T__1);
				setState(298);
			methodHeader();
				setState(302);
			_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 16, _ctx)) {
			case 1:
				{
				{
					setState(299);
				match(ASSIGN);
					setState(300);
				expression();
				}
				}
				break;
			case 2:
				{
					setState(301);
				block();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodModifierContext extends ParserRuleContext {
		public MethodModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodModifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodModifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodModifierContext methodModifier() throws RecognitionException {
		MethodModifierContext _localctx = new MethodModifierContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_methodModifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(304);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << FINAL) | (1L << NATIVE) | (1L << PRIVATE) | (1L << PROTECTED) | (1L << PUBLIC) | (1L << STATIC))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodHeaderContext extends ParserRuleContext {
		public TerminalNode Identifier() {
			return getToken(CavajParser.Identifier, 0);
		}

		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class, 0);
		}
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public MethodHeaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodHeader; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodHeader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodHeader(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodHeader(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodHeaderContext methodHeader() throws RecognitionException {
		MethodHeaderContext _localctx = new MethodHeaderContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_methodHeader);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(306);
			match(Identifier);
				setState(307);
			match(LPAREN);
				setState(309);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
					setState(308);
				formalParameters();
				}
			}

				setState(311);
			match(RPAREN);
				setState(314);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == COLON) {
					{
						setState(312);
						match(COLON);
						setState(313);
						classType();
					}
				}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersContext extends ParserRuleContext {
		public List<FormalParameterContext> formalParameter() {
			return getRuleContexts(FormalParameterContext.class);
		}
		public FormalParameterContext formalParameter(int i) {
			return getRuleContext(FormalParameterContext.class,i);
		}
		public FormalParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterFormalParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitFormalParameters(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitFormalParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FormalParametersContext formalParameters() throws RecognitionException {
		FormalParametersContext _localctx = new FormalParametersContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_formalParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(316);
			formalParameter();
				setState(321);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
					setState(317);
				match(COMMA);
					setState(318);
				formalParameter();
				}
				}
				setState(323);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParameterContext extends ParserRuleContext {
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public FormalParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterFormalParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitFormalParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitFormalParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FormalParameterContext formalParameter() throws RecognitionException {
		FormalParameterContext _localctx = new FormalParameterContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_formalParameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(324);
			variableDeclaratorId();
				setState(325);
			match(COLON);
				setState(326);
			classType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstanceInitializerContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public InstanceInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instanceInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterInstanceInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitInstanceInitializer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitInstanceInitializer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstanceInitializerContext instanceInitializer() throws RecognitionException {
		InstanceInitializerContext _localctx = new InstanceInitializerContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_instanceInitializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(328);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StaticInitializerContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public StaticInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_staticInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterStaticInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitStaticInitializer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitStaticInitializer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StaticInitializerContext staticInitializer() throws RecognitionException {
		StaticInitializerContext _localctx = new StaticInitializerContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_staticInitializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(330);
			match(STATIC);
				setState(331);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorDeclarationContext extends ParserRuleContext {
		public ConstructorHeaderContext constructorHeader() {
			return getRuleContext(ConstructorHeaderContext.class, 0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstructorDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterConstructorDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitConstructorDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitConstructorDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstructorDeclarationContext constructorDeclaration() throws RecognitionException {
		ConstructorDeclarationContext _localctx = new ConstructorDeclarationContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_constructorDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(333);
			match(T__1);
				setState(334);
				constructorHeader();
				setState(338);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ASSIGN:
				{
				{
					setState(335);
				match(ASSIGN);
					setState(336);
				expression();
				}
				}
				break;
			case LBRACE:
				{
					setState(337);
				block();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorHeaderContext extends ParserRuleContext {
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}

		public ConstructorHeaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_constructorHeader;
		}
		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterConstructorHeader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitConstructorHeader(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitConstructorHeader(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstructorHeaderContext constructorHeader() throws RecognitionException {
		ConstructorHeaderContext _localctx = new ConstructorHeaderContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_constructorHeader);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(340);
			match(THIS);
				setState(341);
			match(LPAREN);
				setState(343);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
					setState(342);
				formalParameters();
				}
			}

				setState(345);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockStatementsContext blockStatements() {
			return getRuleContext(BlockStatementsContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(347);
			match(LBRACE);
				setState(349);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << VAR) | (1L << VAL) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(348);
				blockStatements();
				}
			}

				setState(351);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockStatementsContext extends ParserRuleContext {
		public List<BlockStatementContext> blockStatement() {
			return getRuleContexts(BlockStatementContext.class);
		}
		public BlockStatementContext blockStatement(int i) {
			return getRuleContext(BlockStatementContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(CavajParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(CavajParser.SEMI, i);
		}
		public ResultExpressionContext resultExpression() {
			return getRuleContext(ResultExpressionContext.class,0);
		}
		public BlockStatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockStatements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterBlockStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitBlockStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitBlockStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockStatementsContext blockStatements() throws RecognitionException {
		BlockStatementsContext _localctx = new BlockStatementsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_blockStatements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(353);
			blockStatement();
				setState(358);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
						setState(354);
					match(SEMI);
						setState(355);
					blockStatement();
					}
					} 
				}
				setState(360);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
				setState(362);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(361);
				resultExpression();
				}
			}

				setState(365);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
					setState(364);
				match(SEMI);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public LocalVariableDeclarationContext localVariableDeclaration() {
			return getRuleContext(LocalVariableDeclarationContext.class,0);
		}
		public BlockStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterBlockStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitBlockStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitBlockStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockStatementContext blockStatement() throws RecognitionException {
		BlockStatementContext _localctx = new BlockStatementContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_blockStatement);
		try {
			setState(369);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
			case T__4:
			case FOR:
			case IF:
			case NEW:
			case RETURN:
			case SUPER:
			case THIS:
			case UNIT:
			case WHILE:
			case IntegerLiteral:
			case BooleanLiteral:
			case StringLiteral:
			case NullLiteral:
			case LPAREN:
			case LBRACE:
			case BANG:
			case TILDE:
			case INC:
			case DEC:
			case ADD:
			case SUB:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
					setState(367);
				expression();
				}
				break;
			case VAR:
			case VAL:
				enterOuterAlt(_localctx, 2);
				{
					setState(368);
				localVariableDeclaration();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ResultExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ResultExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resultExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterResultExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitResultExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitResultExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ResultExpressionContext resultExpression() throws RecognitionException {
		ResultExpressionContext _localctx = new ResultExpressionContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_resultExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(371);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public IfThenExpressionContext ifThenExpression() {
			return getRuleContext(IfThenExpressionContext.class,0);
		}
		public IfThenElseExpressionContext ifThenElseExpression() {
			return getRuleContext(IfThenElseExpressionContext.class,0);
		}
		public WhileExpressionContext whileExpression() {
			return getRuleContext(WhileExpressionContext.class,0);
		}
		public ForExpressionContext forExpression() {
			return getRuleContext(ForExpressionContext.class,0);
		}
		public TryExpressionContext tryExpression() {
			return getRuleContext(TryExpressionContext.class,0);
		}
		public ConditionalOrExpressionContext conditionalOrExpression() {
			return getRuleContext(ConditionalOrExpressionContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public ReturnExpressionContext returnExpression() {
			return getRuleContext(ReturnExpressionContext.class,0);
		}
		public ClassInstanceCreationExpressionContext classInstanceCreationExpression() {
			return getRuleContext(ClassInstanceCreationExpressionContext.class,0);
		}
		public ThrowExpressionContext throwExpression() {
			return getRuleContext(ThrowExpressionContext.class,0);
		}
		public ConstructorThisExpressionContext constructorThisExpression() {
			return getRuleContext(ConstructorThisExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_expression);
		try {
			setState(384);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(373);
				ifThenExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(374);
				ifThenElseExpression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(375);
				whileExpression();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(376);
				forExpression();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(377);
				tryExpression();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(378);
				conditionalOrExpression(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
					setState(379);
				assignment();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
					setState(380);
				returnExpression();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
					setState(381);
				classInstanceCreationExpression();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
					setState(382);
					throwExpression();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
					setState(383);
				constructorThisExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorThisExpressionContext extends ParserRuleContext {
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public ConstructorThisExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorThisExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterConstructorThisExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitConstructorThisExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitConstructorThisExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstructorThisExpressionContext constructorThisExpression() throws RecognitionException {
		ConstructorThisExpressionContext _localctx = new ConstructorThisExpressionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_constructorThisExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(386);
			match(THIS);
				setState(387);
			match(LPAREN);
				setState(388);
			argumentList();
				setState(389);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalVariableDeclarationContext extends ParserRuleContext {
		public VariableDeclarationContext variableDeclaration() {
			return getRuleContext(VariableDeclarationContext.class,0);
		}
		public LocalVariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localVariableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterLocalVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitLocalVariableDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitLocalVariableDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalVariableDeclarationContext localVariableDeclaration() throws RecognitionException {
		LocalVariableDeclarationContext _localctx = new LocalVariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_localVariableDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(391);
			variableDeclaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterReturnExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitReturnExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitReturnExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnExpressionContext returnExpression() throws RecognitionException {
		ReturnExpressionContext _localctx = new ReturnExpressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_returnExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(393);
			match(RETURN);
				setState(395);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				{
					setState(394);
				expression();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfThenExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public IfThenExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifThenExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterIfThenExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitIfThenExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitIfThenExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfThenExpressionContext ifThenExpression() throws RecognitionException {
		IfThenExpressionContext _localctx = new IfThenExpressionContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_ifThenExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(397);
			match(IF);
				setState(398);
			match(LPAREN);
				setState(399);
			expression();
				setState(400);
			match(RPAREN);
				setState(401);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfThenElseExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(CavajParser.SEMI, 0); }
		public IfThenElseExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifThenElseExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterIfThenElseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitIfThenElseExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitIfThenElseExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfThenElseExpressionContext ifThenElseExpression() throws RecognitionException {
		IfThenElseExpressionContext _localctx = new IfThenElseExpressionContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_ifThenElseExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(403);
			match(IF);
				setState(404);
			match(LPAREN);
				setState(405);
			expression();
				setState(406);
			match(RPAREN);
				setState(407);
			expression();
				setState(409);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
					setState(408);
				match(SEMI);
				}
			}

				setState(411);
			match(ELSE);
				setState(412);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public WhileExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterWhileExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitWhileExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitWhileExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileExpressionContext whileExpression() throws RecognitionException {
		WhileExpressionContext _localctx = new WhileExpressionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_whileExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(414);
			match(WHILE);
				setState(415);
			match(LPAREN);
				setState(416);
			expression();
				setState(417);
			match(RPAREN);
				setState(418);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForInitContext forInit() {
			return getRuleContext(ForInitContext.class,0);
		}
		public ForConditionContext forCondition() {
			return getRuleContext(ForConditionContext.class,0);
		}
		public ForUpdateContext forUpdate() {
			return getRuleContext(ForUpdateContext.class,0);
		}
		public ForExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterForExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitForExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitForExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForExpressionContext forExpression() throws RecognitionException {
		ForExpressionContext _localctx = new ForExpressionContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_forExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(420);
			match(FOR);
				setState(421);
			match(LPAREN);
				setState(423);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << VAR) | (1L << VAL) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(422);
					forInit();
				}
				}

				setState(425);
			match(SEMI);
				setState(427);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(426);
					forCondition();
				}
				}

				setState(429);
			match(SEMI);
				setState(431);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(430);
					forUpdate();
				}
				}

				setState(433);
			match(RPAREN);
				setState(434);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForInitContext extends ParserRuleContext {
		public BlockStatementContext blockStatement() {
			return getRuleContext(BlockStatementContext.class, 0);
		}
		public ForInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forInit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterForInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitForInit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitForInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForInitContext forInit() throws RecognitionException {
		ForInitContext _localctx = new ForInitContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_forInit);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(436);
				blockStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForConditionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterForCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitForCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitForCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForConditionContext forCondition() throws RecognitionException {
		ForConditionContext _localctx = new ForConditionContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_forCondition);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(438);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForUpdateContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForUpdateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forUpdate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterForUpdate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitForUpdate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitForUpdate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForUpdateContext forUpdate() throws RecognitionException {
		ForUpdateContext _localctx = new ForUpdateContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_forUpdate);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(440);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThrowExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ThrowExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_throwExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterThrowExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitThrowExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitThrowExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ThrowExpressionContext throwExpression() throws RecognitionException {
		ThrowExpressionContext _localctx = new ThrowExpressionContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_throwExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(442);
			match(T__3);
				setState(443);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TryExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CatchesContext catches() {
			return getRuleContext(CatchesContext.class,0);
		}
		public Finally_Context finally_() {
			return getRuleContext(Finally_Context.class,0);
		}
		public TryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterTryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitTryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitTryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TryExpressionContext tryExpression() throws RecognitionException {
		TryExpressionContext _localctx = new TryExpressionContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_tryExpression);
		int _la;
		try {
			setState(456);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 34, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(445);
				match(T__4);
					setState(446);
				expression();
					setState(447);
				catches();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(449);
				match(T__4);
					setState(450);
				expression();
					setState(452);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CATCH) {
					{
						setState(451);
					catches();
					}
				}

					setState(454);
				finally_();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchesContext extends ParserRuleContext {
		public List<CatchClauseContext> catchClause() {
			return getRuleContexts(CatchClauseContext.class);
		}
		public CatchClauseContext catchClause(int i) {
			return getRuleContext(CatchClauseContext.class,i);
		}
		public CatchesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catches; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCatches(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCatches(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCatches(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CatchesContext catches() throws RecognitionException {
		CatchesContext _localctx = new CatchesContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_catches);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(458);
			catchClause();
				setState(462);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 35, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
						setState(459);
					catchClause();
					}
					} 
				}
				setState(464);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 35, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchClauseContext extends ParserRuleContext {
		public CatchFormalParameterContext catchFormalParameter() {
			return getRuleContext(CatchFormalParameterContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CatchClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCatchClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCatchClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCatchClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CatchClauseContext catchClause() throws RecognitionException {
		CatchClauseContext _localctx = new CatchClauseContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_catchClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(465);
			match(CATCH);
				setState(466);
			match(LPAREN);
				setState(467);
			catchFormalParameter();
				setState(468);
			match(RPAREN);
				setState(469);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchFormalParameterContext extends ParserRuleContext {
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public CatchTypeContext catchType() {
			return getRuleContext(CatchTypeContext.class,0);
		}
		public CatchFormalParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchFormalParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCatchFormalParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCatchFormalParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCatchFormalParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CatchFormalParameterContext catchFormalParameter() throws RecognitionException {
		CatchFormalParameterContext _localctx = new CatchFormalParameterContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_catchFormalParameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(471);
			variableDeclaratorId();
				setState(472);
			match(COLON);
				setState(473);
			catchType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchTypeContext extends ParserRuleContext {
		public List<ClassTypeContext> classType() {
			return getRuleContexts(ClassTypeContext.class);
		}
		public ClassTypeContext classType(int i) {
			return getRuleContext(ClassTypeContext.class,i);
		}
		public CatchTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCatchType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCatchType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCatchType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CatchTypeContext catchType() throws RecognitionException {
		CatchTypeContext _localctx = new CatchTypeContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_catchType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(475);
			classType();
				setState(480);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BITOR) {
				{
				{
					setState(476);
				match(BITOR);
					setState(477);
				classType();
				}
				}
				setState(482);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Finally_Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Finally_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finally_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterFinally_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitFinally_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitFinally_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Finally_Context finally_() throws RecognitionException {
		Finally_Context _localctx = new Finally_Context(_ctx, getState());
		enterRule(_localctx, 102, RULE_finally_);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(483);
			match(FINALLY);
				setState(484);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public Primary_lfno_primaryContext primary_lfno_primary() {
			return getRuleContext(Primary_lfno_primaryContext.class,0);
		}

		public List<MethodInvocation_lf_primaryContext> methodInvocation_lf_primary() {
			return getRuleContexts(MethodInvocation_lf_primaryContext.class);
		}

		public MethodInvocation_lf_primaryContext methodInvocation_lf_primary(int i) {
			return getRuleContext(MethodInvocation_lf_primaryContext.class, i);
		}
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPrimary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPrimary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_primary);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(486);
			primary_lfno_primary();
				setState(490);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 37, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
						setState(487);
						methodInvocation_lf_primary();
					}
					} 
				}
				setState(492);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 37, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primary_tmpContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}

		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FieldAccessContext fieldAccess() {
			return getRuleContext(FieldAccessContext.class,0);
		}
		public MethodInvocationContext methodInvocation() {
			return getRuleContext(MethodInvocationContext.class,0);
		}
		public Primary_tmpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPrimary_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPrimary_tmp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPrimary_tmp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Primary_tmpContext primary_tmp() throws RecognitionException {
		Primary_tmpContext _localctx = new Primary_tmpContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_primary_tmp);
		try {
			setState(504);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 38, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(493);
				literal();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(494);
					variableName();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(495);
				match(THIS);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(496);
				match(UNIT);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(497);
				block();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(498);
				match(LPAREN);
					setState(499);
				expression();
					setState(500);
				match(RPAREN);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
					setState(502);
				fieldAccess();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
					setState(503);
				methodInvocation();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primary_lfno_primaryContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}

		public FieldAccessContext fieldAccess() {
			return getRuleContext(FieldAccessContext.class, 0);
		}

		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}

		public ThisExpressionContext thisExpression() {
			return getRuleContext(ThisExpressionContext.class, 0);
		}

		public UnitExpressionContext unitExpression() {
			return getRuleContext(UnitExpressionContext.class, 0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MethodInvocation_lfno_primaryContext methodInvocation_lfno_primary() {
			return getRuleContext(MethodInvocation_lfno_primaryContext.class,0);
		}
		public Primary_lfno_primaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_lfno_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPrimary_lfno_primary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPrimary_lfno_primary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPrimary_lfno_primary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Primary_lfno_primaryContext primary_lfno_primary() throws RecognitionException {
		Primary_lfno_primaryContext _localctx = new Primary_lfno_primaryContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_primary_lfno_primary);
		try {
			setState(517);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 39, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(506);
				literal();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(507);
					fieldAccess();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(508);
					variableName();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(509);
					thisExpression();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(510);
					unitExpression();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(511);
					block();
				}
			break;
				case 7:
					enterOuterAlt(_localctx, 7);
				{
					setState(512);
				match(LPAREN);
					setState(513);
				expression();
					setState(514);
				match(RPAREN);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
					setState(516);
				methodInvocation_lfno_primary();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThisExpressionContext extends ParserRuleContext {
		public ThisExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_thisExpression;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterThisExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitThisExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitThisExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ThisExpressionContext thisExpression() throws RecognitionException {
		ThisExpressionContext _localctx = new ThisExpressionContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_thisExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(519);
				match(THIS);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnitExpressionContext extends ParserRuleContext {
		public UnitExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_unitExpression;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterUnitExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitUnitExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor) return ((CavajVisitor<? extends T>) visitor).visitUnitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnitExpressionContext unitExpression() throws RecognitionException {
		UnitExpressionContext _localctx = new UnitExpressionContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_unitExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(521);
				match(UNIT);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassInstanceCreationExpressionContext extends ParserRuleContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class, 0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public ClassInstanceCreationExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classInstanceCreationExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterClassInstanceCreationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitClassInstanceCreationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitClassInstanceCreationExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassInstanceCreationExpressionContext classInstanceCreationExpression() throws RecognitionException {
		ClassInstanceCreationExpressionContext _localctx = new ClassInstanceCreationExpressionContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_classInstanceCreationExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(523);
			match(NEW);
				setState(524);
				classType();
				setState(525);
			match(LPAREN);
				setState(527);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(526);
				argumentList();
				}
			}

				setState(529);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldAccessContext extends ParserRuleContext {
		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}
		public FieldAccessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldAccess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterFieldAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitFieldAccess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitFieldAccess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FieldAccessContext fieldAccess() throws RecognitionException {
		FieldAccessContext _localctx = new FieldAccessContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_fieldAccess);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(531);
				match(THIS);
				setState(532);
				match(DOT);
				setState(533);
				variableName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodInvocationContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class,0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}

		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public MethodInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodInvocation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodInvocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodInvocation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodInvocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodInvocationContext methodInvocation() throws RecognitionException {
		MethodInvocationContext _localctx = new MethodInvocationContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_methodInvocation);
		int _la;
		try {
			setState(589);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 47, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(535);
				methodName();
					setState(536);
				match(LPAREN);
					setState(538);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(537);
					argumentList();
					}
				}

					setState(540);
				match(RPAREN);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(542);
				classType();
					setState(543);
				match(DOT);
					setState(544);
					methodName();
					setState(545);
				match(LPAREN);
					setState(547);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(546);
					argumentList();
					}
				}

					setState(549);
				match(RPAREN);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(551);
					variableName();
					setState(552);
				match(DOT);
					setState(553);
					methodName();
					setState(554);
				match(LPAREN);
					setState(556);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(555);
					argumentList();
					}
				}

					setState(558);
				match(RPAREN);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(560);
				primary();
					setState(561);
				match(DOT);
					setState(562);
					methodName();
					setState(563);
				match(LPAREN);
					setState(565);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(564);
					argumentList();
					}
				}

					setState(567);
				match(RPAREN);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
					setState(569);
				match(SUPER);
					setState(570);
				match(DOT);
					setState(571);
					methodName();
					setState(572);
				match(LPAREN);
					setState(574);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(573);
					argumentList();
					}
				}

					setState(576);
				match(RPAREN);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
					setState(578);
				classType();
					setState(579);
				match(DOT);
					setState(580);
				match(SUPER);
					setState(581);
				match(DOT);
					setState(582);
					methodName();
					setState(583);
				match(LPAREN);
					setState(585);
				_errHandler.sync(this);
				_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(584);
					argumentList();
					}
				}

					setState(587);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodInvocation_lf_primaryContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public MethodInvocation_lf_primaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodInvocation_lf_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodInvocation_lf_primary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodInvocation_lf_primary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodInvocation_lf_primary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodInvocation_lf_primaryContext methodInvocation_lf_primary() throws RecognitionException {
		MethodInvocation_lf_primaryContext _localctx = new MethodInvocation_lf_primaryContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_methodInvocation_lf_primary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(591);
			match(DOT);
				setState(592);
				methodName();
				setState(593);
			match(LPAREN);
				setState(595);
			_errHandler.sync(this);
			_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
				{
					setState(594);
				argumentList();
				}
			}

				setState(597);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodInvocation_lfno_primaryContext extends ParserRuleContext {
		public LocalMethodInvocationContext localMethodInvocation() {
			return getRuleContext(LocalMethodInvocationContext.class, 0);
		}

		public StaticMethodInvocationContext staticMethodInvocation() {
			return getRuleContext(StaticMethodInvocationContext.class, 0);
		}

		public VariableMethodInvocatonContext variableMethodInvocaton() {
			return getRuleContext(VariableMethodInvocatonContext.class, 0);
		}

		public SuperMethodInvocationContext superMethodInvocation() {
			return getRuleContext(SuperMethodInvocationContext.class, 0);
		}

		public SuperClassMethodInvocationContext superClassMethodInvocation() {
			return getRuleContext(SuperClassMethodInvocationContext.class, 0);
		}
		public MethodInvocation_lfno_primaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodInvocation_lfno_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMethodInvocation_lfno_primary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMethodInvocation_lfno_primary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMethodInvocation_lfno_primary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodInvocation_lfno_primaryContext methodInvocation_lfno_primary() throws RecognitionException {
		MethodInvocation_lfno_primaryContext _localctx = new MethodInvocation_lfno_primaryContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_methodInvocation_lfno_primary);
		try {
			setState(604);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 49, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(599);
					localMethodInvocation();
				}
			break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(600);
					staticMethodInvocation();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(601);
					variableMethodInvocaton();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(602);
					superMethodInvocation();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(603);
					superClassMethodInvocation();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalMethodInvocationContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}

		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class, 0);
		}

		public LocalMethodInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_localMethodInvocation;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterLocalMethodInvocation(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitLocalMethodInvocation(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor)
				return ((CavajVisitor<? extends T>) visitor).visitLocalMethodInvocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalMethodInvocationContext localMethodInvocation() throws RecognitionException {
		LocalMethodInvocationContext _localctx = new LocalMethodInvocationContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_localMethodInvocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(606);
				methodName();
				setState(607);
				match(LPAREN);
				setState(609);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(608);
						argumentList();
					}
				}

				setState(611);
				match(RPAREN);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StaticMethodInvocationContext extends ParserRuleContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class, 0);
		}

		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}

		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class, 0);
		}

		public StaticMethodInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_staticMethodInvocation;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterStaticMethodInvocation(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitStaticMethodInvocation(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor)
				return ((CavajVisitor<? extends T>) visitor).visitStaticMethodInvocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StaticMethodInvocationContext staticMethodInvocation() throws RecognitionException {
		StaticMethodInvocationContext _localctx = new StaticMethodInvocationContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_staticMethodInvocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(613);
				classType();
				setState(614);
				match(DOT);
				setState(615);
				methodName();
				setState(616);
				match(LPAREN);
				setState(618);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(617);
						argumentList();
					}
				}

				setState(620);
				match(RPAREN);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableMethodInvocatonContext extends ParserRuleContext {
		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}

		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}

		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class, 0);
		}

		public VariableMethodInvocatonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_variableMethodInvocaton;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterVariableMethodInvocaton(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitVariableMethodInvocaton(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor)
				return ((CavajVisitor<? extends T>) visitor).visitVariableMethodInvocaton(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableMethodInvocatonContext variableMethodInvocaton() throws RecognitionException {
		VariableMethodInvocatonContext _localctx = new VariableMethodInvocatonContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_variableMethodInvocaton);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(622);
				variableName();
				setState(623);
				match(DOT);
				setState(624);
				methodName();
				setState(625);
				match(LPAREN);
				setState(627);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(626);
						argumentList();
					}
				}

				setState(629);
				match(RPAREN);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperMethodInvocationContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}

		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class, 0);
		}

		public SuperMethodInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_superMethodInvocation;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterSuperMethodInvocation(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitSuperMethodInvocation(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor)
				return ((CavajVisitor<? extends T>) visitor).visitSuperMethodInvocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SuperMethodInvocationContext superMethodInvocation() throws RecognitionException {
		SuperMethodInvocationContext _localctx = new SuperMethodInvocationContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_superMethodInvocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(631);
				match(SUPER);
				setState(632);
				match(DOT);
				setState(633);
				methodName();
				setState(634);
				match(LPAREN);
				setState(636);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(635);
						argumentList();
					}
				}

				setState(638);
				match(RPAREN);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperClassMethodInvocationContext extends ParserRuleContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class, 0);
		}

		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class, 0);
		}

		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class, 0);
		}

		public SuperClassMethodInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_superClassMethodInvocation;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).enterSuperClassMethodInvocation(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof CavajListener) ((CavajListener) listener).exitSuperClassMethodInvocation(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof CavajVisitor)
				return ((CavajVisitor<? extends T>) visitor).visitSuperClassMethodInvocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SuperClassMethodInvocationContext superClassMethodInvocation() throws RecognitionException {
		SuperClassMethodInvocationContext _localctx = new SuperClassMethodInvocationContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_superClassMethodInvocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(640);
				classType();
				setState(641);
				match(DOT);
				setState(642);
				match(SUPER);
				setState(643);
				match(DOT);
				setState(644);
				methodName();
				setState(645);
				match(LPAREN);
				setState(647);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << FOR) | (1L << IF) | (1L << NEW) | (1L << RETURN) | (1L << SUPER) | (1L << THIS) | (1L << UNIT) | (1L << WHILE) | (1L << IntegerLiteral) | (1L << BooleanLiteral) | (1L << StringLiteral) | (1L << NullLiteral) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (INC - 64)) | (1L << (DEC - 64)) | (1L << (ADD - 64)) | (1L << (SUB - 64)) | (1L << (Identifier - 64)))) != 0)) {
					{
						setState(646);
						argumentList();
					}
				}

				setState(649);
				match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArgumentListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argumentList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterArgumentList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitArgumentList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitArgumentList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentListContext argumentList() throws RecognitionException {
		ArgumentListContext _localctx = new ArgumentListContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_argumentList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(651);
			expression();
				setState(656);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
					setState(652);
				match(COMMA);
					setState(653);
				expression();
				}
				}
				setState(658);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public LeftHandSideContext leftHandSide() {
			return getRuleContext(LeftHandSideContext.class,0);
		}
		public AssignmentOperatorContext assignmentOperator() {
			return getRuleContext(AssignmentOperatorContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(659);
			leftHandSide();
				setState(660);
			assignmentOperator();
				setState(661);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LeftHandSideContext extends ParserRuleContext {
		public VariableNameContext variableName() {
			return getRuleContext(VariableNameContext.class, 0);
		}
		public FieldAccessContext fieldAccess() {
			return getRuleContext(FieldAccessContext.class,0);
		}
		public LeftHandSideContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leftHandSide; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterLeftHandSide(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitLeftHandSide(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitLeftHandSide(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LeftHandSideContext leftHandSide() throws RecognitionException {
		LeftHandSideContext _localctx = new LeftHandSideContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_leftHandSide);
		try {
			setState(665);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case Identifier:
				enterOuterAlt(_localctx, 1);
				{
					setState(663);
					variableName();
				}
				break;
				case THIS:
				enterOuterAlt(_localctx, 2);
				{
					setState(664);
				fieldAccess();
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentOperatorContext extends ParserRuleContext {
		public AssignmentOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterAssignmentOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitAssignmentOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitAssignmentOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentOperatorContext assignmentOperator() throws RecognitionException {
		AssignmentOperatorContext _localctx = new AssignmentOperatorContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_assignmentOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(667);
			_la = _input.LA(1);
				if (!(((((_la - 51)) & ~0x3f) == 0 && ((1L << (_la - 51)) & ((1L << (ASSIGN - 51)) | (1L << (ADD_ASSIGN - 51)) | (1L << (SUB_ASSIGN - 51)) | (1L << (MUL_ASSIGN - 51)) | (1L << (DIV_ASSIGN - 51)) | (1L << (AND_ASSIGN - 51)) | (1L << (OR_ASSIGN - 51)) | (1L << (XOR_ASSIGN - 51)) | (1L << (MOD_ASSIGN - 51)) | (1L << (LSHIFT_ASSIGN - 51)) | (1L << (RSHIFT_ASSIGN - 51)) | (1L << (URSHIFT_ASSIGN - 51)))) != 0))) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalOrExpressionContext extends ParserRuleContext {
		public ConditionalAndExpressionContext conditionalAndExpression() {
			return getRuleContext(ConditionalAndExpressionContext.class,0);
		}
		public ConditionalOrExpressionContext conditionalOrExpression() {
			return getRuleContext(ConditionalOrExpressionContext.class,0);
		}
		public ConditionalOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterConditionalOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitConditionalOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitConditionalOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionalOrExpressionContext conditionalOrExpression() throws RecognitionException {
		return conditionalOrExpression(0);
	}

	private ConditionalOrExpressionContext conditionalOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ConditionalOrExpressionContext _localctx = new ConditionalOrExpressionContext(_ctx, _parentState);
		ConditionalOrExpressionContext _prevctx = _localctx;
		int _startState = 142;
		enterRecursionRule(_localctx, 142, RULE_conditionalOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(670);
			conditionalAndExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(677);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 57, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ConditionalOrExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_conditionalOrExpression);
						setState(672);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(673);
					match(OR);
						setState(674);
					conditionalAndExpression(0);
					}
					} 
				}
				setState(679);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 57, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConditionalAndExpressionContext extends ParserRuleContext {
		public InclusiveOrExpressionContext inclusiveOrExpression() {
			return getRuleContext(InclusiveOrExpressionContext.class,0);
		}
		public ConditionalAndExpressionContext conditionalAndExpression() {
			return getRuleContext(ConditionalAndExpressionContext.class,0);
		}
		public ConditionalAndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalAndExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterConditionalAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitConditionalAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitConditionalAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionalAndExpressionContext conditionalAndExpression() throws RecognitionException {
		return conditionalAndExpression(0);
	}

	private ConditionalAndExpressionContext conditionalAndExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ConditionalAndExpressionContext _localctx = new ConditionalAndExpressionContext(_ctx, _parentState);
		ConditionalAndExpressionContext _prevctx = _localctx;
		int _startState = 144;
		enterRecursionRule(_localctx, 144, RULE_conditionalAndExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(681);
			inclusiveOrExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(688);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 58, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ConditionalAndExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_conditionalAndExpression);
						setState(683);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(684);
					match(AND);
						setState(685);
					inclusiveOrExpression(0);
					}
					} 
				}
				setState(690);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 58, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InclusiveOrExpressionContext extends ParserRuleContext {
		public ExclusiveOrExpressionContext exclusiveOrExpression() {
			return getRuleContext(ExclusiveOrExpressionContext.class,0);
		}
		public InclusiveOrExpressionContext inclusiveOrExpression() {
			return getRuleContext(InclusiveOrExpressionContext.class,0);
		}
		public InclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusiveOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterInclusiveOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitInclusiveOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitInclusiveOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InclusiveOrExpressionContext inclusiveOrExpression() throws RecognitionException {
		return inclusiveOrExpression(0);
	}

	private InclusiveOrExpressionContext inclusiveOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		InclusiveOrExpressionContext _localctx = new InclusiveOrExpressionContext(_ctx, _parentState);
		InclusiveOrExpressionContext _prevctx = _localctx;
		int _startState = 146;
		enterRecursionRule(_localctx, 146, RULE_inclusiveOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(692);
			exclusiveOrExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(699);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 59, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new InclusiveOrExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_inclusiveOrExpression);
						setState(694);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(695);
					match(BITOR);
						setState(696);
					exclusiveOrExpression(0);
					}
					} 
				}
				setState(701);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 59, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExclusiveOrExpressionContext extends ParserRuleContext {
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public ExclusiveOrExpressionContext exclusiveOrExpression() {
			return getRuleContext(ExclusiveOrExpressionContext.class,0);
		}
		public ExclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusiveOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterExclusiveOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitExclusiveOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitExclusiveOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExclusiveOrExpressionContext exclusiveOrExpression() throws RecognitionException {
		return exclusiveOrExpression(0);
	}

	private ExclusiveOrExpressionContext exclusiveOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExclusiveOrExpressionContext _localctx = new ExclusiveOrExpressionContext(_ctx, _parentState);
		ExclusiveOrExpressionContext _prevctx = _localctx;
		int _startState = 148;
		enterRecursionRule(_localctx, 148, RULE_exclusiveOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(703);
			andExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(710);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 60, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExclusiveOrExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_exclusiveOrExpression);
						setState(705);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(706);
					match(CARET);
						setState(707);
					andExpression(0);
					}
					} 
				}
				setState(712);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 60, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AndExpressionContext extends ParserRuleContext {
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public AndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndExpressionContext andExpression() throws RecognitionException {
		return andExpression(0);
	}

	private AndExpressionContext andExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AndExpressionContext _localctx = new AndExpressionContext(_ctx, _parentState);
		AndExpressionContext _prevctx = _localctx;
		int _startState = 150;
		enterRecursionRule(_localctx, 150, RULE_andExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(714);
			equalityExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(721);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 61, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AndExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_andExpression);
						setState(716);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(717);
					match(BITAND);
						setState(718);
					equalityExpression(0);
					}
					} 
				}
				setState(723);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 61, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualityExpressionContext extends ParserRuleContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public EqualityExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalityExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterEqualityExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitEqualityExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitEqualityExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualityExpressionContext equalityExpression() throws RecognitionException {
		return equalityExpression(0);
	}

	private EqualityExpressionContext equalityExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualityExpressionContext _localctx = new EqualityExpressionContext(_ctx, _parentState);
		EqualityExpressionContext _prevctx = _localctx;
		int _startState = 152;
		enterRecursionRule(_localctx, 152, RULE_equalityExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(725);
			relationalExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(735);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 63, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
						setState(733);
					_errHandler.sync(this);
						switch (getInterpreter().adaptivePredict(_input, 62, _ctx)) {
					case 1:
						{
						_localctx = new EqualityExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_equalityExpression);
							setState(727);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
							setState(728);
						match(EQUAL);
							setState(729);
						relationalExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new EqualityExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_equalityExpression);
							setState(730);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
							setState(731);
						match(NOTEQUAL);
							setState(732);
						relationalExpression(0);
						}
						break;
					}
					} 
				}
				setState(737);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 63, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitRelationalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitRelationalExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		return relationalExpression(0);
	}

	private RelationalExpressionContext relationalExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, _parentState);
		RelationalExpressionContext _prevctx = _localctx;
		int _startState = 154;
		enterRecursionRule(_localctx, 154, RULE_relationalExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(739);
			shiftExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(758);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 65, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
						setState(756);
					_errHandler.sync(this);
						switch (getInterpreter().adaptivePredict(_input, 64, _ctx)) {
					case 1:
						{
						_localctx = new RelationalExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
							setState(741);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
							setState(742);
						match(LT);
							setState(743);
						shiftExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new RelationalExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
							setState(744);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
							setState(745);
						match(GT);
							setState(746);
						shiftExpression(0);
						}
						break;
					case 3:
						{
						_localctx = new RelationalExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
							setState(747);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
							setState(748);
						match(LE);
							setState(749);
						shiftExpression(0);
						}
						break;
					case 4:
						{
						_localctx = new RelationalExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
							setState(750);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
							setState(751);
						match(GE);
							setState(752);
						shiftExpression(0);
						}
						break;
					case 5:
						{
						_localctx = new RelationalExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
							setState(753);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
							setState(754);
						match(INSTANCEOF);
							setState(755);
						classType();
						}
						break;
					}
					} 
				}
				setState(760);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 65, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ShiftExpressionContext extends ParserRuleContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public ShiftExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterShiftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitShiftExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitShiftExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShiftExpressionContext shiftExpression() throws RecognitionException {
		return shiftExpression(0);
	}

	private ShiftExpressionContext shiftExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ShiftExpressionContext _localctx = new ShiftExpressionContext(_ctx, _parentState);
		ShiftExpressionContext _prevctx = _localctx;
		int _startState = 156;
		enterRecursionRule(_localctx, 156, RULE_shiftExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(762);
			additiveExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(779);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 67, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
						setState(777);
					_errHandler.sync(this);
						switch (getInterpreter().adaptivePredict(_input, 66, _ctx)) {
					case 1:
						{
						_localctx = new ShiftExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
							setState(764);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
							setState(765);
						match(LT);
							setState(766);
						match(LT);
							setState(767);
						additiveExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new ShiftExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
							setState(768);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
							setState(769);
						match(GT);
							setState(770);
						match(GT);
							setState(771);
						additiveExpression(0);
						}
						break;
					case 3:
						{
						_localctx = new ShiftExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
							setState(772);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
							setState(773);
						match(GT);
							setState(774);
						match(GT);
							setState(775);
						match(GT);
							setState(776);
						additiveExpression(0);
						}
						break;
					}
					} 
				}
				setState(781);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 67, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitAdditiveExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitAdditiveExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		return additiveExpression(0);
	}

	private AdditiveExpressionContext additiveExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, _parentState);
		AdditiveExpressionContext _prevctx = _localctx;
		int _startState = 158;
		enterRecursionRule(_localctx, 158, RULE_additiveExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(783);
			multiplicativeExpression(0);
			}
			_ctx.stop = _input.LT(-1);
				setState(793);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 69, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
						setState(791);
					_errHandler.sync(this);
						switch (getInterpreter().adaptivePredict(_input, 68, _ctx)) {
					case 1:
						{
						_localctx = new AdditiveExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
							setState(785);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
							setState(786);
						match(ADD);
							setState(787);
						multiplicativeExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new AdditiveExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
							setState(788);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
							setState(789);
						match(SUB);
							setState(790);
						multiplicativeExpression(0);
						}
						break;
					}
					} 
				}
				setState(795);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 69, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitMultiplicativeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitMultiplicativeExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		return multiplicativeExpression(0);
	}

	private MultiplicativeExpressionContext multiplicativeExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, _parentState);
		MultiplicativeExpressionContext _prevctx = _localctx;
		int _startState = 160;
		enterRecursionRule(_localctx, 160, RULE_multiplicativeExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
				setState(797);
			unaryExpression();
			}
			_ctx.stop = _input.LT(-1);
				setState(810);
			_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 71, _ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
						setState(808);
					_errHandler.sync(this);
						switch (getInterpreter().adaptivePredict(_input, 70, _ctx)) {
					case 1:
						{
						_localctx = new MultiplicativeExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
							setState(799);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
							setState(800);
						match(MUL);
							setState(801);
						unaryExpression();
						}
						break;
					case 2:
						{
						_localctx = new MultiplicativeExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
							setState(802);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
							setState(803);
						match(DIV);
							setState(804);
						unaryExpression();
						}
						break;
					case 3:
						{
						_localctx = new MultiplicativeExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
							setState(805);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
							setState(806);
						match(MOD);
							setState(807);
						unaryExpression();
						}
						break;
					}
					} 
				}
				setState(812);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 71, _ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public PreIncrementExpressionContext preIncrementExpression() {
			return getRuleContext(PreIncrementExpressionContext.class,0);
		}
		public PreDecrementExpressionContext preDecrementExpression() {
			return getRuleContext(PreDecrementExpressionContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public UnaryExpressionNotPlusMinusContext unaryExpressionNotPlusMinus() {
			return getRuleContext(UnaryExpressionNotPlusMinusContext.class,0);
		}
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitUnaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitUnaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_unaryExpression);
		try {
			setState(820);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INC:
				enterOuterAlt(_localctx, 1);
				{
					setState(813);
				preIncrementExpression();
				}
				break;
			case DEC:
				enterOuterAlt(_localctx, 2);
				{
					setState(814);
				preDecrementExpression();
				}
				break;
			case ADD:
				enterOuterAlt(_localctx, 3);
				{
					setState(815);
				match(ADD);
					setState(816);
				unaryExpression();
				}
				break;
			case SUB:
				enterOuterAlt(_localctx, 4);
				{
					setState(817);
				match(SUB);
					setState(818);
				unaryExpression();
				}
				break;
			case SUPER:
			case THIS:
			case UNIT:
			case IntegerLiteral:
			case BooleanLiteral:
			case StringLiteral:
			case NullLiteral:
			case LPAREN:
			case LBRACE:
			case BANG:
			case TILDE:
			case Identifier:
				enterOuterAlt(_localctx, 5);
				{
					setState(819);
				unaryExpressionNotPlusMinus();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreIncrementExpressionContext extends ParserRuleContext {
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public PreIncrementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preIncrementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPreIncrementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPreIncrementExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPreIncrementExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PreIncrementExpressionContext preIncrementExpression() throws RecognitionException {
		PreIncrementExpressionContext _localctx = new PreIncrementExpressionContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_preIncrementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(822);
			match(INC);
				setState(823);
			unaryExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreDecrementExpressionContext extends ParserRuleContext {
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public PreDecrementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preDecrementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPreDecrementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPreDecrementExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPreDecrementExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PreDecrementExpressionContext preDecrementExpression() throws RecognitionException {
		PreDecrementExpressionContext _localctx = new PreDecrementExpressionContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_preDecrementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(825);
			match(DEC);
				setState(826);
			unaryExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionNotPlusMinusContext extends ParserRuleContext {
		public PostfixExpressionContext postfixExpression() {
			return getRuleContext(PostfixExpressionContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public CastExpressionContext castExpression() {
			return getRuleContext(CastExpressionContext.class,0);
		}
		public UnaryExpressionNotPlusMinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpressionNotPlusMinus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterUnaryExpressionNotPlusMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitUnaryExpressionNotPlusMinus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitUnaryExpressionNotPlusMinus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryExpressionNotPlusMinusContext unaryExpressionNotPlusMinus() throws RecognitionException {
		UnaryExpressionNotPlusMinusContext _localctx = new UnaryExpressionNotPlusMinusContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_unaryExpressionNotPlusMinus);
		try {
			setState(834);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 73, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(828);
				postfixExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(829);
				match(TILDE);
					setState(830);
				unaryExpression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					setState(831);
				match(BANG);
					setState(832);
				unaryExpression();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					setState(833);
				castExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixExpressionContext extends ParserRuleContext {
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public List<PostIncrementExpression_lf_postfixExpressionContext> postIncrementExpression_lf_postfixExpression() {
			return getRuleContexts(PostIncrementExpression_lf_postfixExpressionContext.class);
		}
		public PostIncrementExpression_lf_postfixExpressionContext postIncrementExpression_lf_postfixExpression(int i) {
			return getRuleContext(PostIncrementExpression_lf_postfixExpressionContext.class,i);
		}
		public List<PostDecrementExpression_lf_postfixExpressionContext> postDecrementExpression_lf_postfixExpression() {
			return getRuleContexts(PostDecrementExpression_lf_postfixExpressionContext.class);
		}
		public PostDecrementExpression_lf_postfixExpressionContext postDecrementExpression_lf_postfixExpression(int i) {
			return getRuleContext(PostDecrementExpression_lf_postfixExpressionContext.class,i);
		}
		public PostfixExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfixExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPostfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPostfixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPostfixExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixExpressionContext postfixExpression() throws RecognitionException {
		PostfixExpressionContext _localctx = new PostfixExpressionContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_postfixExpression);
		try {
			int _alt;
			setState(850);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 76, _ctx)) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
					setState(836);
				primary();
					setState(840);
				_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 74, _ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
							setState(837);
						postIncrementExpression_lf_postfixExpression();
						}
						} 
					}
					setState(842);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 74, _ctx);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
					setState(843);
				primary();
					setState(847);
				_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 75, _ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
							setState(844);
						postDecrementExpression_lf_postfixExpression();
						}
						} 
					}
					setState(849);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 75, _ctx);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostIncrementExpressionContext extends ParserRuleContext {
		public PostfixExpressionContext postfixExpression() {
			return getRuleContext(PostfixExpressionContext.class,0);
		}
		public PostIncrementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postIncrementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPostIncrementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPostIncrementExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPostIncrementExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostIncrementExpressionContext postIncrementExpression() throws RecognitionException {
		PostIncrementExpressionContext _localctx = new PostIncrementExpressionContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_postIncrementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(852);
			postfixExpression();
				setState(853);
			match(INC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostIncrementExpression_lf_postfixExpressionContext extends ParserRuleContext {
		public PostIncrementExpression_lf_postfixExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postIncrementExpression_lf_postfixExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPostIncrementExpression_lf_postfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPostIncrementExpression_lf_postfixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPostIncrementExpression_lf_postfixExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostIncrementExpression_lf_postfixExpressionContext postIncrementExpression_lf_postfixExpression() throws RecognitionException {
		PostIncrementExpression_lf_postfixExpressionContext _localctx = new PostIncrementExpression_lf_postfixExpressionContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_postIncrementExpression_lf_postfixExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(855);
			match(INC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostDecrementExpressionContext extends ParserRuleContext {
		public PostfixExpressionContext postfixExpression() {
			return getRuleContext(PostfixExpressionContext.class,0);
		}
		public PostDecrementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postDecrementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPostDecrementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPostDecrementExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPostDecrementExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostDecrementExpressionContext postDecrementExpression() throws RecognitionException {
		PostDecrementExpressionContext _localctx = new PostDecrementExpressionContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_postDecrementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(857);
			postfixExpression();
				setState(858);
			match(DEC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostDecrementExpression_lf_postfixExpressionContext extends ParserRuleContext {
		public PostDecrementExpression_lf_postfixExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postDecrementExpression_lf_postfixExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterPostDecrementExpression_lf_postfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitPostDecrementExpression_lf_postfixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitPostDecrementExpression_lf_postfixExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostDecrementExpression_lf_postfixExpressionContext postDecrementExpression_lf_postfixExpression() throws RecognitionException {
		PostDecrementExpression_lf_postfixExpressionContext _localctx = new PostDecrementExpression_lf_postfixExpressionContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_postDecrementExpression_lf_postfixExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(860);
			match(DEC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastExpressionContext extends ParserRuleContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public UnaryExpressionNotPlusMinusContext unaryExpressionNotPlusMinus() {
			return getRuleContext(UnaryExpressionNotPlusMinusContext.class,0);
		}
		public CastExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).enterCastExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CavajListener ) ((CavajListener)listener).exitCastExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CavajVisitor ) return ((CavajVisitor<? extends T>)visitor).visitCastExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CastExpressionContext castExpression() throws RecognitionException {
		CastExpressionContext _localctx = new CastExpressionContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_castExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(862);
			match(LPAREN);
				setState(863);
			classType();
				setState(864);
			match(RPAREN);
				setState(865);
			unaryExpressionNotPlusMinus();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
			case 71:
			return conditionalOrExpression_sempred((ConditionalOrExpressionContext)_localctx, predIndex);
			case 72:
			return conditionalAndExpression_sempred((ConditionalAndExpressionContext)_localctx, predIndex);
			case 73:
			return inclusiveOrExpression_sempred((InclusiveOrExpressionContext)_localctx, predIndex);
			case 74:
			return exclusiveOrExpression_sempred((ExclusiveOrExpressionContext)_localctx, predIndex);
			case 75:
			return andExpression_sempred((AndExpressionContext)_localctx, predIndex);
			case 76:
			return equalityExpression_sempred((EqualityExpressionContext)_localctx, predIndex);
			case 77:
			return relationalExpression_sempred((RelationalExpressionContext)_localctx, predIndex);
			case 78:
			return shiftExpression_sempred((ShiftExpressionContext)_localctx, predIndex);
			case 79:
			return additiveExpression_sempred((AdditiveExpressionContext)_localctx, predIndex);
			case 80:
			return multiplicativeExpression_sempred((MultiplicativeExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean conditionalOrExpression_sempred(ConditionalOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean conditionalAndExpression_sempred(ConditionalAndExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean inclusiveOrExpression_sempred(InclusiveOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean exclusiveOrExpression_sempred(ExclusiveOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean andExpression_sempred(AndExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean equalityExpression_sempred(EqualityExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 2);
		case 6:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean relationalExpression_sempred(RelationalExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 5);
		case 8:
			return precpred(_ctx, 4);
		case 9:
			return precpred(_ctx, 3);
		case 10:
			return precpred(_ctx, 2);
		case 11:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shiftExpression_sempred(ShiftExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 12:
			return precpred(_ctx, 3);
		case 13:
			return precpred(_ctx, 2);
		case 14:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean additiveExpression_sempred(AdditiveExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 15:
			return precpred(_ctx, 2);
		case 16:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean multiplicativeExpression_sempred(MultiplicativeExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 17:
			return precpred(_ctx, 3);
		case 18:
			return precpred(_ctx, 2);
		case 19:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
					"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3^\u0366\4\2\t\2\4" +
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
									"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\3\2\3\2\3\2\3\2\5\2" +
									"\u00bd\n\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n" +
									"\7\n\u00ce\n\n\f\n\16\n\u00d1\13\n\3\n\3\n\3\13\5\13\u00d6\n\13\3\13\3" +
									"\13\3\13\5\13\u00db\n\13\3\13\5\13\u00de\n\13\3\13\5\13\u00e1\n\13\3\f" +
									"\3\f\5\f\u00e5\n\f\3\f\3\f\3\r\3\r\3\r\7\r\u00ec\n\r\f\r\16\r\u00ef\13" +
									"\r\3\16\3\16\3\17\5\17\u00f4\n\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3" +
									"\20\3\20\3\20\5\20\u0100\n\20\3\21\3\21\7\21\u0104\n\21\f\21\16\21\u0107" +
									"\13\21\3\21\3\21\3\22\3\22\3\22\3\22\5\22\u010f\n\22\3\23\3\23\3\23\5" +
									"\23\u0114\n\23\3\24\3\24\3\24\3\24\5\24\u011a\n\24\3\24\3\24\3\24\3\24" +
									"\5\24\u0120\n\24\3\25\3\25\3\26\3\26\3\27\7\27\u0127\n\27\f\27\16\27\u012a" +
									"\13\27\3\27\3\27\3\27\3\27\3\27\5\27\u0131\n\27\3\30\3\30\3\31\3\31\3" +
									"\31\5\31\u0138\n\31\3\31\3\31\3\31\5\31\u013d\n\31\3\32\3\32\3\32\7\32" +
									"\u0142\n\32\f\32\16\32\u0145\13\32\3\33\3\33\3\33\3\33\3\34\3\34\3\35" +
									"\3\35\3\35\3\36\3\36\3\36\3\36\3\36\5\36\u0155\n\36\3\37\3\37\3\37\5\37" +
									"\u015a\n\37\3\37\3\37\3 \3 \5 \u0160\n \3 \3 \3!\3!\3!\7!\u0167\n!\f!" +
									"\16!\u016a\13!\3!\5!\u016d\n!\3!\5!\u0170\n!\3\"\3\"\5\"\u0174\n\"\3#" +
									"\3#\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u0183\n$\3%\3%\3%\3%\3%\3&\3&" +
									"\3\'\3\'\5\'\u018e\n\'\3(\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\5)\u019c\n" +
									")\3)\3)\3)\3*\3*\3*\3*\3*\3*\3+\3+\3+\5+\u01aa\n+\3+\3+\5+\u01ae\n+\3" +
									"+\3+\5+\u01b2\n+\3+\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3/\3\60\3\60\3\60\3" +
									"\60\3\60\3\60\3\60\5\60\u01c7\n\60\3\60\3\60\5\60\u01cb\n\60\3\61\3\61" +
									"\7\61\u01cf\n\61\f\61\16\61\u01d2\13\61\3\62\3\62\3\62\3\62\3\62\3\62" +
									"\3\63\3\63\3\63\3\63\3\64\3\64\3\64\7\64\u01e1\n\64\f\64\16\64\u01e4\13" +
									"\64\3\65\3\65\3\65\3\66\3\66\7\66\u01eb\n\66\f\66\16\66\u01ee\13\66\3" +
									"\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u01fb\n\67" +
									"\38\38\38\38\38\38\38\38\38\38\38\58\u0208\n8\39\39\3:\3:\3;\3;\3;\3;" +
									"\5;\u0212\n;\3;\3;\3<\3<\3<\3<\3=\3=\3=\5=\u021d\n=\3=\3=\3=\3=\3=\3=" +
									"\3=\5=\u0226\n=\3=\3=\3=\3=\3=\3=\3=\5=\u022f\n=\3=\3=\3=\3=\3=\3=\3=" +
									"\5=\u0238\n=\3=\3=\3=\3=\3=\3=\3=\5=\u0241\n=\3=\3=\3=\3=\3=\3=\3=\3=" +
									"\3=\5=\u024c\n=\3=\3=\5=\u0250\n=\3>\3>\3>\3>\5>\u0256\n>\3>\3>\3?\3?" +
									"\3?\3?\3?\5?\u025f\n?\3@\3@\3@\5@\u0264\n@\3@\3@\3A\3A\3A\3A\3A\5A\u026d" +
									"\nA\3A\3A\3B\3B\3B\3B\3B\5B\u0276\nB\3B\3B\3C\3C\3C\3C\3C\5C\u027f\nC" +
									"\3C\3C\3D\3D\3D\3D\3D\3D\3D\5D\u028a\nD\3D\3D\3E\3E\3E\7E\u0291\nE\fE" +
									"\16E\u0294\13E\3F\3F\3F\3F\3G\3G\5G\u029c\nG\3H\3H\3I\3I\3I\3I\3I\3I\7" +
									"I\u02a6\nI\fI\16I\u02a9\13I\3J\3J\3J\3J\3J\3J\7J\u02b1\nJ\fJ\16J\u02b4" +
									"\13J\3K\3K\3K\3K\3K\3K\7K\u02bc\nK\fK\16K\u02bf\13K\3L\3L\3L\3L\3L\3L" +
									"\7L\u02c7\nL\fL\16L\u02ca\13L\3M\3M\3M\3M\3M\3M\7M\u02d2\nM\fM\16M\u02d5" +
									"\13M\3N\3N\3N\3N\3N\3N\3N\3N\3N\7N\u02e0\nN\fN\16N\u02e3\13N\3O\3O\3O" +
									"\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\3O\7O\u02f7\nO\fO\16O\u02fa" +
									"\13O\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\7P\u030c\nP\fP\16" +
									"P\u030f\13P\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\7Q\u031a\nQ\fQ\16Q\u031d\13Q\3" +
									"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\7R\u032b\nR\fR\16R\u032e\13R\3S\3S" +
									"\3S\3S\3S\3S\3S\5S\u0337\nS\3T\3T\3T\3U\3U\3U\3V\3V\3V\3V\3V\3V\5V\u0345" +
									"\nV\3W\3W\7W\u0349\nW\fW\16W\u034c\13W\3W\3W\7W\u0350\nW\fW\16W\u0353" +
									"\13W\5W\u0355\nW\3X\3X\3X\3Y\3Y\3Z\3Z\3Z\3[\3[\3\\\3\\\3\\\3\\\3\\\3\\" +
									"\2\f\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2]\2\4" +
									"\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNP" +
									"RTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a\u008c\u008e" +
									"\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6" +
									"\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\2\5\3\2$%\7\2\5\5\24" +
									"\24\32\32\34\36  \4\2\65\65NX\u0383\2\u00bc\3\2\2\2\4\u00be\3\2\2\2\6" +
									"\u00c0\3\2\2\2\b\u00c2\3\2\2\2\n\u00c4\3\2\2\2\f\u00c6\3\2\2\2\16\u00c8" +
									"\3\2\2\2\20\u00ca\3\2\2\2\22\u00cf\3\2\2\2\24\u00d5\3\2\2\2\26\u00e2\3" +
									"\2\2\2\30\u00e8\3\2\2\2\32\u00f0\3\2\2\2\34\u00f3\3\2\2\2\36\u00f9\3\2" +
									"\2\2 \u0101\3\2\2\2\"\u010e\3\2\2\2$\u0113\3\2\2\2&\u0115\3\2\2\2(\u0121" +
									"\3\2\2\2*\u0123\3\2\2\2,\u0128\3\2\2\2.\u0132\3\2\2\2\60\u0134\3\2\2\2" +
									"\62\u013e\3\2\2\2\64\u0146\3\2\2\2\66\u014a\3\2\2\28\u014c\3\2\2\2:\u014f" +
									"\3\2\2\2<\u0156\3\2\2\2>\u015d\3\2\2\2@\u0163\3\2\2\2B\u0173\3\2\2\2D" +
									"\u0175\3\2\2\2F\u0182\3\2\2\2H\u0184\3\2\2\2J\u0189\3\2\2\2L\u018b\3\2" +
									"\2\2N\u018f\3\2\2\2P\u0195\3\2\2\2R\u01a0\3\2\2\2T\u01a6\3\2\2\2V\u01b6" +
									"\3\2\2\2X\u01b8\3\2\2\2Z\u01ba\3\2\2\2\\\u01bc\3\2\2\2^\u01ca\3\2\2\2" +
									"`\u01cc\3\2\2\2b\u01d3\3\2\2\2d\u01d9\3\2\2\2f\u01dd\3\2\2\2h\u01e5\3" +
									"\2\2\2j\u01e8\3\2\2\2l\u01fa\3\2\2\2n\u0207\3\2\2\2p\u0209\3\2\2\2r\u020b" +
									"\3\2\2\2t\u020d\3\2\2\2v\u0215\3\2\2\2x\u024f\3\2\2\2z\u0251\3\2\2\2|" +
									"\u025e\3\2\2\2~\u0260\3\2\2\2\u0080\u0267\3\2\2\2\u0082\u0270\3\2\2\2" +
									"\u0084\u0279\3\2\2\2\u0086\u0282\3\2\2\2\u0088\u028d\3\2\2\2\u008a\u0295" +
									"\3\2\2\2\u008c\u029b\3\2\2\2\u008e\u029d\3\2\2\2\u0090\u029f\3\2\2\2\u0092" +
									"\u02aa\3\2\2\2\u0094\u02b5\3\2\2\2\u0096\u02c0\3\2\2\2\u0098\u02cb\3\2" +
									"\2\2\u009a\u02d6\3\2\2\2\u009c\u02e4\3\2\2\2\u009e\u02fb\3\2\2\2\u00a0" +
									"\u0310\3\2\2\2\u00a2\u031e\3\2\2\2\u00a4\u0336\3\2\2\2\u00a6\u0338\3\2" +
									"\2\2\u00a8\u033b\3\2\2\2\u00aa\u0344\3\2\2\2\u00ac\u0354\3\2\2\2\u00ae" +
									"\u0356\3\2\2\2\u00b0\u0359\3\2\2\2\u00b2\u035b\3\2\2\2\u00b4\u035e\3\2" +
									"\2\2\u00b6\u0360\3\2\2\2\u00b8\u00bd\5\4\3\2\u00b9\u00bd\5\6\4\2\u00ba" +
									"\u00bd\5\b\5\2\u00bb\u00bd\5\n\6\2\u00bc\u00b8\3\2\2\2\u00bc\u00b9\3\2" +
									"\2\2\u00bc\u00ba\3\2\2\2\u00bc\u00bb\3\2\2\2\u00bd\3\3\2\2\2\u00be\u00bf" +
									"\7\'\2\2\u00bf\5\3\2\2\2\u00c0\u00c1\7(\2\2\u00c1\7\3\2\2\2\u00c2\u00c3" +
									"\7*\2\2\u00c3\t\3\2\2\2\u00c4\u00c5\7+\2\2\u00c5\13\3\2\2\2\u00c6\u00c7" +
									"\7Y\2\2\u00c7\r\3\2\2\2\u00c8\u00c9\7Y\2\2\u00c9\17\3\2\2\2\u00ca\u00cb" +
									"\7Y\2\2\u00cb\21\3\2\2\2\u00cc\u00ce\5\24\13\2\u00cd\u00cc\3\2\2\2\u00ce" +
									"\u00d1\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d2\3\2" +
									"\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d3\7\2\2\3\u00d3\23\3\2\2\2\u00d4\u00d6" +
									"\7\3\2\2\u00d5\u00d4\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7" +
									"\u00d8\7\r\2\2\u00d8\u00da\7Y\2\2\u00d9\u00db\5\26\f\2\u00da\u00d9\3\2" +
									"\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc\u00de\5\36\20\2\u00dd" +
									"\u00dc\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00e0\3\2\2\2\u00df\u00e1\5 " +
									"\21\2\u00e0\u00df\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\25\3\2\2\2\u00e2\u00e4" +
									"\7,\2\2\u00e3\u00e5\5\30\r\2\u00e4\u00e3\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5" +
									"\u00e6\3\2\2\2\u00e6\u00e7\7-\2\2\u00e7\27\3\2\2\2\u00e8\u00ed\5\34\17" +
									"\2\u00e9\u00ea\7\63\2\2\u00ea\u00ec\5\34\17\2\u00eb\u00e9\3\2\2\2\u00ec" +
									"\u00ef\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\31\3\2\2" +
									"\2\u00ef\u00ed\3\2\2\2\u00f0\u00f1\t\2\2\2\u00f1\33\3\2\2\2\u00f2\u00f4" +
									"\5\32\16\2\u00f3\u00f2\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2" +
									"\u00f5\u00f6\7Y\2\2\u00f6\u00f7\7;\2\2\u00f7\u00f8\5\f\7\2\u00f8\35\3" +
									"\2\2\2\u00f9\u00fa\7\22\2\2\u00fa\u00ff\5\f\7\2\u00fb\u00fc\7,\2\2\u00fc" +
									"\u00fd\5\u0088E\2\u00fd\u00fe\7-\2\2\u00fe\u0100\3\2\2\2\u00ff\u00fb\3" +
									"\2\2\2\u00ff\u0100\3\2\2\2\u0100\37\3\2\2\2\u0101\u0105\7.\2\2\u0102\u0104" +
									"\5\"\22\2\u0103\u0102\3\2\2\2\u0104\u0107\3\2\2\2\u0105\u0103\3\2\2\2" +
									"\u0105\u0106\3\2\2\2\u0106\u0108\3\2\2\2\u0107\u0105\3\2\2\2\u0108\u0109" +
									"\7/\2\2\u0109!\3\2\2\2\u010a\u010f\5$\23\2\u010b\u010f\5\66\34\2\u010c" +
									"\u010f\58\35\2\u010d\u010f\5:\36\2\u010e\u010a\3\2\2\2\u010e\u010b\3\2" +
									"\2\2\u010e\u010c\3\2\2\2\u010e\u010d\3\2\2\2\u010f#\3\2\2\2\u0110\u0114" +
									"\5&\24\2\u0111\u0114\5,\27\2\u0112\u0114\7\62\2\2\u0113\u0110\3\2\2\2" +
									"\u0113\u0111\3\2\2\2\u0113\u0112\3\2\2\2\u0114%\3\2\2\2\u0115\u0116\5" +
									"\32\16\2\u0116\u011f\5(\25\2\u0117\u0118\7;\2\2\u0118\u011a\5\f\7\2\u0119" +
									"\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a\u011b\3\2\2\2\u011b\u011c\7\65" +
									"\2\2\u011c\u0120\5*\26\2\u011d\u011e\7;\2\2\u011e\u0120\5\f\7\2\u011f" +
									"\u0119\3\2\2\2\u011f\u011d\3\2\2\2\u0120\'\3\2\2\2\u0121\u0122\7Y\2\2" +
									"\u0122)\3\2\2\2\u0123\u0124\5F$\2\u0124+\3\2\2\2\u0125\u0127\5.\30\2\u0126" +
									"\u0125\3\2\2\2\u0127\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2" +
									"\2\2\u0129\u012b\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012c\7\4\2\2\u012c" +
									"\u0130\5\60\31\2\u012d\u012e\7\65\2\2\u012e\u0131\5F$\2\u012f\u0131\5" +
									"> \2\u0130\u012d\3\2\2\2\u0130\u012f\3\2\2\2\u0130\u0131\3\2\2\2\u0131" +
									"-\3\2\2\2\u0132\u0133\t\3\2\2\u0133/\3\2\2\2\u0134\u0135\7Y\2\2\u0135" +
									"\u0137\7,\2\2\u0136\u0138\5\62\32\2\u0137\u0136\3\2\2\2\u0137\u0138\3" +
									"\2\2\2\u0138\u0139\3\2\2\2\u0139\u013c\7-\2\2\u013a\u013b\7;\2\2\u013b" +
									"\u013d\5\f\7\2\u013c\u013a\3\2\2\2\u013c\u013d\3\2\2\2\u013d\61\3\2\2" +
									"\2\u013e\u0143\5\64\33\2\u013f\u0140\7\63\2\2\u0140\u0142\5\64\33\2\u0141" +
									"\u013f\3\2\2\2\u0142\u0145\3\2\2\2\u0143\u0141\3\2\2\2\u0143\u0144\3\2" +
									"\2\2\u0144\63\3\2\2\2\u0145\u0143\3\2\2\2\u0146\u0147\5(\25\2\u0147\u0148" +
									"\7;\2\2\u0148\u0149\5\f\7\2\u0149\65\3\2\2\2\u014a\u014b\5> \2\u014b\67" +
									"\3\2\2\2\u014c\u014d\7 \2\2\u014d\u014e\5> \2\u014e9\3\2\2\2\u014f\u0150" +
									"\7\4\2\2\u0150\u0154\5<\37\2\u0151\u0152\7\65\2\2\u0152\u0155\5F$\2\u0153" +
									"\u0155\5> \2\u0154\u0151\3\2\2\2\u0154\u0153\3\2\2\2\u0155;\3\2\2\2\u0156" +
									"\u0157\7\"\2\2\u0157\u0159\7,\2\2\u0158\u015a\5\62\32\2\u0159\u0158\3" +
									"\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u015c\7-\2\2\u015c" +
									"=\3\2\2\2\u015d\u015f\7.\2\2\u015e\u0160\5@!\2\u015f\u015e\3\2\2\2\u015f" +
									"\u0160\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0162\7/\2\2\u0162?\3\2\2\2\u0163" +
									"\u0168\5B\"\2\u0164\u0165\7\62\2\2\u0165\u0167\5B\"\2\u0166\u0164\3\2" +
									"\2\2\u0167\u016a\3\2\2\2\u0168\u0166\3\2\2\2\u0168\u0169\3\2\2\2\u0169" +
									"\u016c\3\2\2\2\u016a\u0168\3\2\2\2\u016b\u016d\5D#\2\u016c\u016b\3\2\2" +
									"\2\u016c\u016d\3\2\2\2\u016d\u016f\3\2\2\2\u016e\u0170\7\62\2\2\u016f" +
									"\u016e\3\2\2\2\u016f\u0170\3\2\2\2\u0170A\3\2\2\2\u0171\u0174\5F$\2\u0172" +
									"\u0174\5J&\2\u0173\u0171\3\2\2\2\u0173\u0172\3\2\2\2\u0174C\3\2\2\2\u0175" +
									"\u0176\5F$\2\u0176E\3\2\2\2\u0177\u0183\5N(\2\u0178\u0183\5P)\2\u0179" +
									"\u0183\5R*\2\u017a\u0183\5T+\2\u017b\u0183\5^\60\2\u017c\u0183\5\u0090" +
									"I\2\u017d\u0183\5\u008aF\2\u017e\u0183\5L\'\2\u017f\u0183\5t;\2\u0180" +
									"\u0183\5\\/\2\u0181\u0183\5H%\2\u0182\u0177\3\2\2\2\u0182\u0178\3\2\2" +
									"\2\u0182\u0179\3\2\2\2\u0182\u017a\3\2\2\2\u0182\u017b\3\2\2\2\u0182\u017c" +
									"\3\2\2\2\u0182\u017d\3\2\2\2\u0182\u017e\3\2\2\2\u0182\u017f\3\2\2\2\u0182" +
									"\u0180\3\2\2\2\u0182\u0181\3\2\2\2\u0183G\3\2\2\2\u0184\u0185\7\"\2\2" +
									"\u0185\u0186\7,\2\2\u0186\u0187\5\u0088E\2\u0187\u0188\7-\2\2\u0188I\3" +
									"\2\2\2\u0189\u018a\5&\24\2\u018aK\3\2\2\2\u018b\u018d\7\37\2\2\u018c\u018e" +
									"\5F$\2\u018d\u018c\3\2\2\2\u018d\u018e\3\2\2\2\u018eM\3\2\2\2\u018f\u0190" +
									"\7\27\2\2\u0190\u0191\7,\2\2\u0191\u0192\5F$\2\u0192\u0193\7-\2\2\u0193" +
									"\u0194\5F$\2\u0194O\3\2\2\2\u0195\u0196\7\27\2\2\u0196\u0197\7,\2\2\u0197" +
									"\u0198\5F$\2\u0198\u0199\7-\2\2\u0199\u019b\5F$\2\u019a\u019c\7\62\2\2" +
									"\u019b\u019a\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019d\3\2\2\2\u019d\u019e" +
									"\7\21\2\2\u019e\u019f\5F$\2\u019fQ\3\2\2\2\u01a0\u01a1\7&\2\2\u01a1\u01a2" +
									"\7,\2\2\u01a2\u01a3\5F$\2\u01a3\u01a4\7-\2\2\u01a4\u01a5\5F$\2\u01a5S" +
									"\3\2\2\2\u01a6\u01a7\7\26\2\2\u01a7\u01a9\7,\2\2\u01a8\u01aa\5V,\2\u01a9" +
									"\u01a8\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab\u01ad\7\62" +
									"\2\2\u01ac\u01ae\5X-\2\u01ad\u01ac\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae\u01af" +
									"\3\2\2\2\u01af\u01b1\7\62\2\2\u01b0\u01b2\5Z.\2\u01b1\u01b0\3\2\2\2\u01b1" +
									"\u01b2\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b4\7-\2\2\u01b4\u01b5\5F$" +
									"\2\u01b5U\3\2\2\2\u01b6\u01b7\5B\"\2\u01b7W\3\2\2\2\u01b8\u01b9\5F$\2" +
									"\u01b9Y\3\2\2\2\u01ba\u01bb\5F$\2\u01bb[\3\2\2\2\u01bc\u01bd\7\6\2\2\u01bd" +
									"\u01be\5F$\2\u01be]\3\2\2\2\u01bf\u01c0\7\7\2\2\u01c0\u01c1\5F$\2\u01c1" +
									"\u01c2\5`\61\2\u01c2\u01cb\3\2\2\2\u01c3\u01c4\7\7\2\2\u01c4\u01c6\5F" +
									"$\2\u01c5\u01c7\5`\61\2\u01c6\u01c5\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7" +
									"\u01c8\3\2\2\2\u01c8\u01c9\5h\65\2\u01c9\u01cb\3\2\2\2\u01ca\u01bf\3\2" +
									"\2\2\u01ca\u01c3\3\2\2\2\u01cb_\3\2\2\2\u01cc\u01d0\5b\62\2\u01cd\u01cf" +
									"\5b\62\2\u01ce\u01cd\3\2\2\2\u01cf\u01d2\3\2\2\2\u01d0\u01ce\3\2\2\2\u01d0" +
									"\u01d1\3\2\2\2\u01d1a\3\2\2\2\u01d2\u01d0\3\2\2\2\u01d3\u01d4\7\f\2\2" +
									"\u01d4\u01d5\7,\2\2\u01d5\u01d6\5d\63\2\u01d6\u01d7\7-\2\2\u01d7\u01d8" +
									"\5F$\2\u01d8c\3\2\2\2\u01d9\u01da\5(\25\2\u01da\u01db\7;\2\2\u01db\u01dc" +
									"\5f\64\2\u01dce\3\2\2\2\u01dd\u01e2\5\f\7\2\u01de\u01df\7I\2\2\u01df\u01e1" +
									"\5\f\7\2\u01e0\u01de\3\2\2\2\u01e1\u01e4\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e2" +
									"\u01e3\3\2\2\2\u01e3g\3\2\2\2\u01e4\u01e2\3\2\2\2\u01e5\u01e6\7\25\2\2" +
									"\u01e6\u01e7\5F$\2\u01e7i\3\2\2\2\u01e8\u01ec\5n8\2\u01e9\u01eb\5z>\2" +
									"\u01ea\u01e9\3\2\2\2\u01eb\u01ee\3\2\2\2\u01ec\u01ea\3\2\2\2\u01ec\u01ed" +
									"\3\2\2\2\u01edk\3\2\2\2\u01ee\u01ec\3\2\2\2\u01ef\u01fb\5\2\2\2\u01f0" +
									"\u01fb\5\16\b\2\u01f1\u01fb\7\"\2\2\u01f2\u01fb\7#\2\2\u01f3\u01fb\5>" +
									" \2\u01f4\u01f5\7,\2\2\u01f5\u01f6\5F$\2\u01f6\u01f7\7-\2\2\u01f7\u01fb" +
									"\3\2\2\2\u01f8\u01fb\5v<\2\u01f9\u01fb\5x=\2\u01fa\u01ef\3\2\2\2\u01fa" +
									"\u01f0\3\2\2\2\u01fa\u01f1\3\2\2\2\u01fa\u01f2\3\2\2\2\u01fa\u01f3\3\2" +
									"\2\2\u01fa\u01f4\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fa\u01f9\3\2\2\2\u01fb" +
									"m\3\2\2\2\u01fc\u0208\5\2\2\2\u01fd\u0208\5v<\2\u01fe\u0208\5\16\b\2\u01ff" +
									"\u0208\5p9\2\u0200\u0208\5r:\2\u0201\u0208\5> \2\u0202\u0203\7,\2\2\u0203" +
									"\u0204\5F$\2\u0204\u0205\7-\2\2\u0205\u0208\3\2\2\2\u0206\u0208\5|?\2" +
									"\u0207\u01fc\3\2\2\2\u0207\u01fd\3\2\2\2\u0207\u01fe\3\2\2\2\u0207\u01ff" +
									"\3\2\2\2\u0207\u0200\3\2\2\2\u0207\u0201\3\2\2\2\u0207\u0202\3\2\2\2\u0207" +
									"\u0206\3\2\2\2\u0208o\3\2\2\2\u0209\u020a\7\"\2\2\u020aq\3\2\2\2\u020b" +
									"\u020c\7#\2\2\u020cs\3\2\2\2\u020d\u020e\7\33\2\2\u020e\u020f\5\f\7\2" +
									"\u020f\u0211\7,\2\2\u0210\u0212\5\u0088E\2\u0211\u0210\3\2\2\2\u0211\u0212" +
									"\3\2\2\2\u0212\u0213\3\2\2\2\u0213\u0214\7-\2\2\u0214u\3\2\2\2\u0215\u0216" +
									"\7\"\2\2\u0216\u0217\7\64\2\2\u0217\u0218\5\16\b\2\u0218w\3\2\2\2\u0219" +
									"\u021a\5\20\t\2\u021a\u021c\7,\2\2\u021b\u021d\5\u0088E\2\u021c\u021b" +
									"\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021e\3\2\2\2\u021e\u021f\7-\2\2\u021f" +
									"\u0250\3\2\2\2\u0220\u0221\5\f\7\2\u0221\u0222\7\64\2\2\u0222\u0223\5" +
									"\20\t\2\u0223\u0225\7,\2\2\u0224\u0226\5\u0088E\2\u0225\u0224\3\2\2\2" +
									"\u0225\u0226\3\2\2\2\u0226\u0227\3\2\2\2\u0227\u0228\7-\2\2\u0228\u0250" +
									"\3\2\2\2\u0229\u022a\5\16\b\2\u022a\u022b\7\64\2\2\u022b\u022c\5\20\t" +
									"\2\u022c\u022e\7,\2\2\u022d\u022f\5\u0088E\2\u022e\u022d\3\2\2\2\u022e" +
									"\u022f\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0231\7-\2\2\u0231\u0250\3\2" +
									"\2\2\u0232\u0233\5j\66\2\u0233\u0234\7\64\2\2\u0234\u0235\5\20\t\2\u0235" +
									"\u0237\7,\2\2\u0236\u0238\5\u0088E\2\u0237\u0236\3\2\2\2\u0237\u0238\3" +
									"\2\2\2\u0238\u0239\3\2\2\2\u0239\u023a\7-\2\2\u023a\u0250\3\2\2\2\u023b" +
									"\u023c\7!\2\2\u023c\u023d\7\64\2\2\u023d\u023e\5\20\t\2\u023e\u0240\7" +
									",\2\2\u023f\u0241\5\u0088E\2\u0240\u023f\3\2\2\2\u0240\u0241\3\2\2\2\u0241" +
									"\u0242\3\2\2\2\u0242\u0243\7-\2\2\u0243\u0250\3\2\2\2\u0244\u0245\5\f" +
									"\7\2\u0245\u0246\7\64\2\2\u0246\u0247\7!\2\2\u0247\u0248\7\64\2\2\u0248" +
									"\u0249\5\20\t\2\u0249\u024b\7,\2\2\u024a\u024c\5\u0088E\2\u024b\u024a" +
									"\3\2\2\2\u024b\u024c\3\2\2\2\u024c\u024d\3\2\2\2\u024d\u024e\7-\2\2\u024e" +
									"\u0250\3\2\2\2\u024f\u0219\3\2\2\2\u024f\u0220\3\2\2\2\u024f\u0229\3\2" +
									"\2\2\u024f\u0232\3\2\2\2\u024f\u023b\3\2\2\2\u024f\u0244\3\2\2\2\u0250" +
									"y\3\2\2\2\u0251\u0252\7\64\2\2\u0252\u0253\5\20\t\2\u0253\u0255\7,\2\2" +
									"\u0254\u0256\5\u0088E\2\u0255\u0254\3\2\2\2\u0255\u0256\3\2\2\2\u0256" +
									"\u0257\3\2\2\2\u0257\u0258\7-\2\2\u0258{\3\2\2\2\u0259\u025f\5~@\2\u025a" +
									"\u025f\5\u0080A\2\u025b\u025f\5\u0082B\2\u025c\u025f\5\u0084C\2\u025d" +
									"\u025f\5\u0086D\2\u025e\u0259\3\2\2\2\u025e\u025a\3\2\2\2\u025e\u025b" +
									"\3\2\2\2\u025e\u025c\3\2\2\2\u025e\u025d\3\2\2\2\u025f}\3\2\2\2\u0260" +
									"\u0261\5\20\t\2\u0261\u0263\7,\2\2\u0262\u0264\5\u0088E\2\u0263\u0262" +
									"\3\2\2\2\u0263\u0264\3\2\2\2\u0264\u0265\3\2\2\2\u0265\u0266\7-\2\2\u0266" +
									"\177\3\2\2\2\u0267\u0268\5\f\7\2\u0268\u0269\7\64\2\2\u0269\u026a\5\20" +
									"\t\2\u026a\u026c\7,\2\2\u026b\u026d\5\u0088E\2\u026c\u026b\3\2\2\2\u026c" +
									"\u026d\3\2\2\2\u026d\u026e\3\2\2\2\u026e\u026f\7-\2\2\u026f\u0081\3\2" +
									"\2\2\u0270\u0271\5\16\b\2\u0271\u0272\7\64\2\2\u0272\u0273\5\20\t\2\u0273" +
									"\u0275\7,\2\2\u0274\u0276\5\u0088E\2\u0275\u0274\3\2\2\2\u0275\u0276\3" +
									"\2\2\2\u0276\u0277\3\2\2\2\u0277\u0278\7-\2\2\u0278\u0083\3\2\2\2\u0279" +
									"\u027a\7!\2\2\u027a\u027b\7\64\2\2\u027b\u027c\5\20\t\2\u027c\u027e\7" +
									",\2\2\u027d\u027f\5\u0088E\2\u027e\u027d\3\2\2\2\u027e\u027f\3\2\2\2\u027f" +
									"\u0280\3\2\2\2\u0280\u0281\7-\2\2\u0281\u0085\3\2\2\2\u0282\u0283\5\f" +
									"\7\2\u0283\u0284\7\64\2\2\u0284\u0285\7!\2\2\u0285\u0286\7\64\2\2\u0286" +
									"\u0287\5\20\t\2\u0287\u0289\7,\2\2\u0288\u028a\5\u0088E\2\u0289\u0288" +
									"\3\2\2\2\u0289\u028a\3\2\2\2\u028a\u028b\3\2\2\2\u028b\u028c\7-\2\2\u028c" +
									"\u0087\3\2\2\2\u028d\u0292\5F$\2\u028e\u028f\7\63\2\2\u028f\u0291\5F$" +
									"\2\u0290\u028e\3\2\2\2\u0291\u0294\3\2\2\2\u0292\u0290\3\2\2\2\u0292\u0293" +
									"\3\2\2\2\u0293\u0089\3\2\2\2\u0294\u0292\3\2\2\2\u0295\u0296\5\u008cG" +
									"\2\u0296\u0297\5\u008eH\2\u0297\u0298\5F$\2\u0298\u008b\3\2\2\2\u0299" +
									"\u029c\5\16\b\2\u029a\u029c\5v<\2\u029b\u0299\3\2\2\2\u029b\u029a\3\2" +
									"\2\2\u029c\u008d\3\2\2\2\u029d\u029e\t\4\2\2\u029e\u008f\3\2\2\2\u029f" +
									"\u02a0\bI\1\2\u02a0\u02a1\5\u0092J\2\u02a1\u02a7\3\2\2\2\u02a2\u02a3\f" +
									"\3\2\2\u02a3\u02a4\7A\2\2\u02a4\u02a6\5\u0092J\2\u02a5\u02a2\3\2\2\2\u02a6" +
									"\u02a9\3\2\2\2\u02a7\u02a5\3\2\2\2\u02a7\u02a8\3\2\2\2\u02a8\u0091\3\2" +
									"\2\2\u02a9\u02a7\3\2\2\2\u02aa\u02ab\bJ\1\2\u02ab\u02ac\5\u0094K\2\u02ac" +
									"\u02b2\3\2\2\2\u02ad\u02ae\f\3\2\2\u02ae\u02af\7@\2\2\u02af\u02b1\5\u0094" +
									"K\2\u02b0\u02ad\3\2\2\2\u02b1\u02b4\3\2\2\2\u02b2\u02b0\3\2\2\2\u02b2" +
									"\u02b3\3\2\2\2\u02b3\u0093\3\2\2\2\u02b4\u02b2\3\2\2\2\u02b5\u02b6\bK" +
									"\1\2\u02b6\u02b7\5\u0096L\2\u02b7\u02bd\3\2\2\2\u02b8\u02b9\f\3\2\2\u02b9" +
									"\u02ba\7I\2\2\u02ba\u02bc\5\u0096L\2\u02bb\u02b8\3\2\2\2\u02bc\u02bf\3" +
									"\2\2\2\u02bd\u02bb\3\2\2\2\u02bd\u02be\3\2\2\2\u02be\u0095\3\2\2\2\u02bf" +
									"\u02bd\3\2\2\2\u02c0\u02c1\bL\1\2\u02c1\u02c2\5\u0098M\2\u02c2\u02c8\3" +
									"\2\2\2\u02c3\u02c4\f\3\2\2\u02c4\u02c5\7J\2\2\u02c5\u02c7\5\u0098M\2\u02c6" +
									"\u02c3\3\2\2\2\u02c7\u02ca\3\2\2\2\u02c8\u02c6\3\2\2\2\u02c8\u02c9\3\2" +
									"\2\2\u02c9\u0097\3\2\2\2\u02ca\u02c8\3\2\2\2\u02cb\u02cc\bM\1\2\u02cc" +
									"\u02cd\5\u009aN\2\u02cd\u02d3\3\2\2\2\u02ce\u02cf\f\3\2\2\u02cf\u02d0" +
									"\7H\2\2\u02d0\u02d2\5\u009aN\2\u02d1\u02ce\3\2\2\2\u02d2\u02d5\3\2\2\2" +
									"\u02d3\u02d1\3\2\2\2\u02d3\u02d4\3\2\2\2\u02d4\u0099\3\2\2\2\u02d5\u02d3" +
									"\3\2\2\2\u02d6\u02d7\bN\1\2\u02d7\u02d8\5\u009cO\2\u02d8\u02e1\3\2\2\2" +
									"\u02d9\u02da\f\4\2\2\u02da\u02db\7<\2\2\u02db\u02e0\5\u009cO\2\u02dc\u02dd" +
									"\f\3\2\2\u02dd\u02de\7?\2\2\u02de\u02e0\5\u009cO\2\u02df\u02d9\3\2\2\2" +
									"\u02df\u02dc\3\2\2\2\u02e0\u02e3\3\2\2\2\u02e1\u02df\3\2\2\2\u02e1\u02e2" +
									"\3\2\2\2\u02e2\u009b\3\2\2\2\u02e3\u02e1\3\2\2\2\u02e4\u02e5\bO\1\2\u02e5" +
									"\u02e6\5\u009eP\2\u02e6\u02f8\3\2\2\2\u02e7\u02e8\f\7\2\2\u02e8\u02e9" +
									"\7\67\2\2\u02e9\u02f7\5\u009eP\2\u02ea\u02eb\f\6\2\2\u02eb\u02ec\7\66" +
									"\2\2\u02ec\u02f7\5\u009eP\2\u02ed\u02ee\f\5\2\2\u02ee\u02ef\7=\2\2\u02ef" +
									"\u02f7\5\u009eP\2\u02f0\u02f1\f\4\2\2\u02f1\u02f2\7>\2\2\u02f2\u02f7\5" +
									"\u009eP\2\u02f3\u02f4\f\3\2\2\u02f4\u02f5\7\30\2\2\u02f5\u02f7\5\f\7\2" +
									"\u02f6\u02e7\3\2\2\2\u02f6\u02ea\3\2\2\2\u02f6\u02ed\3\2\2\2\u02f6\u02f0" +
									"\3\2\2\2\u02f6\u02f3\3\2\2\2\u02f7\u02fa\3\2\2\2\u02f8\u02f6\3\2\2\2\u02f8" +
									"\u02f9\3\2\2\2\u02f9\u009d\3\2\2\2\u02fa\u02f8\3\2\2\2\u02fb\u02fc\bP" +
									"\1\2\u02fc\u02fd\5\u00a0Q\2\u02fd\u030d\3\2\2\2\u02fe\u02ff\f\5\2\2\u02ff" +
									"\u0300\7\67\2\2\u0300\u0301\7\67\2\2\u0301\u030c\5\u00a0Q\2\u0302\u0303" +
									"\f\4\2\2\u0303\u0304\7\66\2\2\u0304\u0305\7\66\2\2\u0305\u030c\5\u00a0" +
									"Q\2\u0306\u0307\f\3\2\2\u0307\u0308\7\66\2\2\u0308\u0309\7\66\2\2\u0309" +
									"\u030a\7\66\2\2\u030a\u030c\5\u00a0Q\2\u030b\u02fe\3\2\2\2\u030b\u0302" +
									"\3\2\2\2\u030b\u0306\3\2\2\2\u030c\u030f\3\2\2\2\u030d\u030b\3\2\2\2\u030d" +
									"\u030e\3\2\2\2\u030e\u009f\3\2\2\2\u030f\u030d\3\2\2\2\u0310\u0311\bQ" +
									"\1\2\u0311\u0312\5\u00a2R\2\u0312\u031b\3\2\2\2\u0313\u0314\f\4\2\2\u0314" +
									"\u0315\7D\2\2\u0315\u031a\5\u00a2R\2\u0316\u0317\f\3\2\2\u0317\u0318\7" +
									"E\2\2\u0318\u031a\5\u00a2R\2\u0319\u0313\3\2\2\2\u0319\u0316\3\2\2\2\u031a" +
									"\u031d\3\2\2\2\u031b\u0319\3\2\2\2\u031b\u031c\3\2\2\2\u031c\u00a1\3\2" +
									"\2\2\u031d\u031b\3\2\2\2\u031e\u031f\bR\1\2\u031f\u0320\5\u00a4S\2\u0320" +
									"\u032c\3\2\2\2\u0321\u0322\f\5\2\2\u0322\u0323\7F\2\2\u0323\u032b\5\u00a4" +
									"S\2\u0324\u0325\f\4\2\2\u0325\u0326\7G\2\2\u0326\u032b\5\u00a4S\2\u0327" +
									"\u0328\f\3\2\2\u0328\u0329\7K\2\2\u0329\u032b\5\u00a4S\2\u032a\u0321\3" +
									"\2\2\2\u032a\u0324\3\2\2\2\u032a\u0327\3\2\2\2\u032b\u032e\3\2\2\2\u032c" +
									"\u032a\3\2\2\2\u032c\u032d\3\2\2\2\u032d\u00a3\3\2\2\2\u032e\u032c\3\2" +
									"\2\2\u032f\u0337\5\u00a6T\2\u0330\u0337\5\u00a8U\2\u0331\u0332\7D\2\2" +
									"\u0332\u0337\5\u00a4S\2\u0333\u0334\7E\2\2\u0334\u0337\5\u00a4S\2\u0335" +
									"\u0337\5\u00aaV\2\u0336\u032f\3\2\2\2\u0336\u0330\3\2\2\2\u0336\u0331" +
									"\3\2\2\2\u0336\u0333\3\2\2\2\u0336\u0335\3\2\2\2\u0337\u00a5\3\2\2\2\u0338" +
									"\u0339\7B\2\2\u0339\u033a\5\u00a4S\2\u033a\u00a7\3\2\2\2\u033b\u033c\7" +
									"C\2\2\u033c\u033d\5\u00a4S\2\u033d\u00a9\3\2\2\2\u033e\u0345\5\u00acW" +
									"\2\u033f\u0340\79\2\2\u0340\u0345\5\u00a4S\2\u0341\u0342\78\2\2\u0342" +
									"\u0345\5\u00a4S\2\u0343\u0345\5\u00b6\\\2\u0344\u033e\3\2\2\2\u0344\u033f" +
									"\3\2\2\2\u0344\u0341\3\2\2\2\u0344\u0343\3\2\2\2\u0345\u00ab\3\2\2\2\u0346" +
									"\u034a\5j\66\2\u0347\u0349\5\u00b0Y\2\u0348\u0347\3\2\2\2\u0349\u034c" +
									"\3\2\2\2\u034a\u0348\3\2\2\2\u034a\u034b\3\2\2\2\u034b\u0355\3\2\2\2\u034c" +
									"\u034a\3\2\2\2\u034d\u0351\5j\66\2\u034e\u0350\5\u00b4[\2\u034f\u034e" +
									"\3\2\2\2\u0350\u0353\3\2\2\2\u0351\u034f\3\2\2\2\u0351\u0352\3\2\2\2\u0352" +
									"\u0355\3\2\2\2\u0353\u0351\3\2\2\2\u0354\u0346\3\2\2\2\u0354\u034d\3\2" +
									"\2\2\u0355\u00ad\3\2\2\2\u0356\u0357\5\u00acW\2\u0357\u0358\7B\2\2\u0358" +
									"\u00af\3\2\2\2\u0359\u035a\7B\2\2\u035a\u00b1\3\2\2\2\u035b\u035c\5\u00ac" +
									"W\2\u035c\u035d\7C\2\2\u035d\u00b3\3\2\2\2\u035e\u035f\7C\2\2\u035f\u00b5" +
									"\3\2\2\2\u0360\u0361\7,\2\2\u0361\u0362\5\f\7\2\u0362\u0363\7-\2\2\u0363" +
									"\u0364\5\u00aaV\2\u0364\u00b7\3\2\2\2O\u00bc\u00cf\u00d5\u00da\u00dd\u00e0" +
									"\u00e4\u00ed\u00f3\u00ff\u0105\u010e\u0113\u0119\u011f\u0128\u0130\u0137" +
									"\u013c\u0143\u0154\u0159\u015f\u0168\u016c\u016f\u0173\u0182\u018d\u019b" +
									"\u01a9\u01ad\u01b1\u01c6\u01ca\u01d0\u01e2\u01ec\u01fa\u0207\u0211\u021c" +
									"\u0225\u022e\u0237\u0240\u024b\u024f\u0255\u025e\u0263\u026c\u0275\u027e" +
									"\u0289\u0292\u029b\u02a7\u02b2\u02bd\u02c8\u02d3\u02df\u02e1\u02f6\u02f8" +
									"\u030b\u030d\u0319\u031b\u032a\u032c\u0336\u0344\u034a\u0351\u0354";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}