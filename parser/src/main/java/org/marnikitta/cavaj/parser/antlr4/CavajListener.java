// Generated from org/marnikitta/cavaj/parser/antlr4/Cavaj.g4 by ANTLR 4.6
package org.marnikitta.cavaj.parser.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CavajParser}.
 */
public interface CavajListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CavajParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(CavajParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(CavajParser.LiteralContext ctx);
	/**
   * Enter a parse tree produced by {@link CavajParser#integerLiteral}.
   * @param ctx the parse tree
   */
  void enterIntegerLiteral(CavajParser.IntegerLiteralContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#integerLiteral}.
   *
   * @param ctx the parse tree
   */
  void exitIntegerLiteral(CavajParser.IntegerLiteralContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#booleanLiteral}.
   *
   * @param ctx the parse tree
   */
  void enterBooleanLiteral(CavajParser.BooleanLiteralContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#booleanLiteral}.
   *
   * @param ctx the parse tree
   */
  void exitBooleanLiteral(CavajParser.BooleanLiteralContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#stringLiteral}.
   *
   * @param ctx the parse tree
   */
  void enterStringLiteral(CavajParser.StringLiteralContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#stringLiteral}.
   *
   * @param ctx the parse tree
   */
  void exitStringLiteral(CavajParser.StringLiteralContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#nullLiteral}.
   *
   * @param ctx the parse tree
   */
  void enterNullLiteral(CavajParser.NullLiteralContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#nullLiteral}.
   *
   * @param ctx the parse tree
   */
  void exitNullLiteral(CavajParser.NullLiteralContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#classType}.
	 * @param ctx the parse tree
	 */
	void enterClassType(CavajParser.ClassTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classType}.
	 * @param ctx the parse tree
	 */
	void exitClassType(CavajParser.ClassTypeContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#variableName}.
   * @param ctx the parse tree
   */
  void enterVariableName(CavajParser.VariableNameContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#variableName}.
   * @param ctx the parse tree
   */
  void exitVariableName(CavajParser.VariableNameContext ctx);

  /**
	 * Enter a parse tree produced by {@link CavajParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(CavajParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(CavajParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(CavajParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(CavajParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(CavajParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(CavajParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classParamClause}.
	 * @param ctx the parse tree
	 */
	void enterClassParamClause(CavajParser.ClassParamClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classParamClause}.
	 * @param ctx the parse tree
	 */
	void exitClassParamClause(CavajParser.ClassParamClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classParams}.
	 * @param ctx the parse tree
	 */
	void enterClassParams(CavajParser.ClassParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classParams}.
	 * @param ctx the parse tree
	 */
	void exitClassParams(CavajParser.ClassParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#valVar}.
	 * @param ctx the parse tree
	 */
	void enterValVar(CavajParser.ValVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#valVar}.
	 * @param ctx the parse tree
	 */
	void exitValVar(CavajParser.ValVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classParam}.
	 * @param ctx the parse tree
	 */
	void enterClassParam(CavajParser.ClassParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classParam}.
	 * @param ctx the parse tree
	 */
	void exitClassParam(CavajParser.ClassParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#superclass}.
	 * @param ctx the parse tree
	 */
	void enterSuperclass(CavajParser.SuperclassContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#superclass}.
	 * @param ctx the parse tree
	 */
	void exitSuperclass(CavajParser.SuperclassContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(CavajParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(CavajParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassBodyDeclaration(CavajParser.ClassBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassBodyDeclaration(CavajParser.ClassBodyDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassMemberDeclaration(CavajParser.ClassMemberDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassMemberDeclaration(CavajParser.ClassMemberDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(CavajParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(CavajParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaratorId(CavajParser.VariableDeclaratorIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaratorId(CavajParser.VariableDeclaratorIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void enterVariableInitializer(CavajParser.VariableInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void exitVariableInitializer(CavajParser.VariableInitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclaration(CavajParser.MethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclaration(CavajParser.MethodDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodModifier}.
	 * @param ctx the parse tree
	 */
	void enterMethodModifier(CavajParser.MethodModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodModifier}.
	 * @param ctx the parse tree
	 */
	void exitMethodModifier(CavajParser.MethodModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodHeader}.
	 * @param ctx the parse tree
	 */
	void enterMethodHeader(CavajParser.MethodHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodHeader}.
	 * @param ctx the parse tree
	 */
	void exitMethodHeader(CavajParser.MethodHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameters(CavajParser.FormalParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameters(CavajParser.FormalParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameter(CavajParser.FormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameter(CavajParser.FormalParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#instanceInitializer}.
	 * @param ctx the parse tree
	 */
	void enterInstanceInitializer(CavajParser.InstanceInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#instanceInitializer}.
	 * @param ctx the parse tree
	 */
	void exitInstanceInitializer(CavajParser.InstanceInitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#staticInitializer}.
	 * @param ctx the parse tree
	 */
	void enterStaticInitializer(CavajParser.StaticInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#staticInitializer}.
	 * @param ctx the parse tree
	 */
	void exitStaticInitializer(CavajParser.StaticInitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterConstructorDeclaration(CavajParser.ConstructorDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitConstructorDeclaration(CavajParser.ConstructorDeclarationContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#constructorHeader}.
   * @param ctx the parse tree
   */
  void enterConstructorHeader(CavajParser.ConstructorHeaderContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#constructorHeader}.
   * @param ctx the parse tree
   */
  void exitConstructorHeader(CavajParser.ConstructorHeaderContext ctx);

  /**
	 * Enter a parse tree produced by {@link CavajParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(CavajParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(CavajParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#blockStatements}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatements(CavajParser.BlockStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#blockStatements}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatements(CavajParser.BlockStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(CavajParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(CavajParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#resultExpression}.
	 * @param ctx the parse tree
	 */
	void enterResultExpression(CavajParser.ResultExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#resultExpression}.
	 * @param ctx the parse tree
	 */
	void exitResultExpression(CavajParser.ResultExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(CavajParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(CavajParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#constructorThisExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstructorThisExpression(CavajParser.ConstructorThisExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#constructorThisExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstructorThisExpression(CavajParser.ConstructorThisExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterLocalVariableDeclaration(CavajParser.LocalVariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitLocalVariableDeclaration(CavajParser.LocalVariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#returnExpression}.
	 * @param ctx the parse tree
	 */
	void enterReturnExpression(CavajParser.ReturnExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#returnExpression}.
	 * @param ctx the parse tree
	 */
	void exitReturnExpression(CavajParser.ReturnExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#ifThenExpression}.
	 * @param ctx the parse tree
	 */
	void enterIfThenExpression(CavajParser.IfThenExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#ifThenExpression}.
	 * @param ctx the parse tree
	 */
	void exitIfThenExpression(CavajParser.IfThenExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#ifThenElseExpression}.
	 * @param ctx the parse tree
	 */
	void enterIfThenElseExpression(CavajParser.IfThenElseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#ifThenElseExpression}.
	 * @param ctx the parse tree
	 */
	void exitIfThenElseExpression(CavajParser.IfThenElseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void enterWhileExpression(CavajParser.WhileExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void exitWhileExpression(CavajParser.WhileExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#forExpression}.
	 * @param ctx the parse tree
	 */
	void enterForExpression(CavajParser.ForExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#forExpression}.
	 * @param ctx the parse tree
	 */
	void exitForExpression(CavajParser.ForExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(CavajParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(CavajParser.ForInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#forCondition}.
	 * @param ctx the parse tree
	 */
	void enterForCondition(CavajParser.ForConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#forCondition}.
	 * @param ctx the parse tree
	 */
	void exitForCondition(CavajParser.ForConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void enterForUpdate(CavajParser.ForUpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void exitForUpdate(CavajParser.ForUpdateContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#throwExpression}.
	 * @param ctx the parse tree
	 */
	void enterThrowExpression(CavajParser.ThrowExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#throwExpression}.
	 * @param ctx the parse tree
	 */
	void exitThrowExpression(CavajParser.ThrowExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void enterTryExpression(CavajParser.TryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void exitTryExpression(CavajParser.TryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#catches}.
	 * @param ctx the parse tree
	 */
	void enterCatches(CavajParser.CatchesContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#catches}.
	 * @param ctx the parse tree
	 */
	void exitCatches(CavajParser.CatchesContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void enterCatchClause(CavajParser.CatchClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void exitCatchClause(CavajParser.CatchClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#catchFormalParameter}.
	 * @param ctx the parse tree
	 */
	void enterCatchFormalParameter(CavajParser.CatchFormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#catchFormalParameter}.
	 * @param ctx the parse tree
	 */
	void exitCatchFormalParameter(CavajParser.CatchFormalParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#catchType}.
	 * @param ctx the parse tree
	 */
	void enterCatchType(CavajParser.CatchTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#catchType}.
	 * @param ctx the parse tree
	 */
	void exitCatchType(CavajParser.CatchTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#finally_}.
	 * @param ctx the parse tree
	 */
	void enterFinally_(CavajParser.Finally_Context ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#finally_}.
	 * @param ctx the parse tree
	 */
	void exitFinally_(CavajParser.Finally_Context ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(CavajParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(CavajParser.PrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#primary_tmp}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_tmp(CavajParser.Primary_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#primary_tmp}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_tmp(CavajParser.Primary_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#primary_lfno_primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_lfno_primary(CavajParser.Primary_lfno_primaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#primary_lfno_primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_lfno_primary(CavajParser.Primary_lfno_primaryContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#thisExpression}.
   *
   * @param ctx the parse tree
   */
  void enterThisExpression(CavajParser.ThisExpressionContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#thisExpression}.
   *
   * @param ctx the parse tree
   */
  void exitThisExpression(CavajParser.ThisExpressionContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#unitExpression}.
   *
   * @param ctx the parse tree
   */
  void enterUnitExpression(CavajParser.UnitExpressionContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#unitExpression}.
   *
   * @param ctx the parse tree
   */
  void exitUnitExpression(CavajParser.UnitExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#classInstanceCreationExpression}.
	 * @param ctx the parse tree
	 */
	void enterClassInstanceCreationExpression(CavajParser.ClassInstanceCreationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#classInstanceCreationExpression}.
	 * @param ctx the parse tree
	 */
	void exitClassInstanceCreationExpression(CavajParser.ClassInstanceCreationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void enterFieldAccess(CavajParser.FieldAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void exitFieldAccess(CavajParser.FieldAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodInvocation}.
	 * @param ctx the parse tree
	 */
	void enterMethodInvocation(CavajParser.MethodInvocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodInvocation}.
	 * @param ctx the parse tree
	 */
	void exitMethodInvocation(CavajParser.MethodInvocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodInvocation_lf_primary}.
	 * @param ctx the parse tree
	 */
	void enterMethodInvocation_lf_primary(CavajParser.MethodInvocation_lf_primaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodInvocation_lf_primary}.
	 * @param ctx the parse tree
	 */
	void exitMethodInvocation_lf_primary(CavajParser.MethodInvocation_lf_primaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#methodInvocation_lfno_primary}.
	 * @param ctx the parse tree
	 */
	void enterMethodInvocation_lfno_primary(CavajParser.MethodInvocation_lfno_primaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#methodInvocation_lfno_primary}.
	 * @param ctx the parse tree
	 */
	void exitMethodInvocation_lfno_primary(CavajParser.MethodInvocation_lfno_primaryContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#localMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void enterLocalMethodInvocation(CavajParser.LocalMethodInvocationContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#localMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void exitLocalMethodInvocation(CavajParser.LocalMethodInvocationContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#staticMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void enterStaticMethodInvocation(CavajParser.StaticMethodInvocationContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#staticMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void exitStaticMethodInvocation(CavajParser.StaticMethodInvocationContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#variableMethodInvocaton}.
   *
   * @param ctx the parse tree
   */
  void enterVariableMethodInvocaton(CavajParser.VariableMethodInvocatonContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#variableMethodInvocaton}.
   *
   * @param ctx the parse tree
   */
  void exitVariableMethodInvocaton(CavajParser.VariableMethodInvocatonContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#superMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void enterSuperMethodInvocation(CavajParser.SuperMethodInvocationContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#superMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void exitSuperMethodInvocation(CavajParser.SuperMethodInvocationContext ctx);

  /**
   * Enter a parse tree produced by {@link CavajParser#superClassMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void enterSuperClassMethodInvocation(CavajParser.SuperClassMethodInvocationContext ctx);

  /**
   * Exit a parse tree produced by {@link CavajParser#superClassMethodInvocation}.
   *
   * @param ctx the parse tree
   */
  void exitSuperClassMethodInvocation(CavajParser.SuperClassMethodInvocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentList(CavajParser.ArgumentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentList(CavajParser.ArgumentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(CavajParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(CavajParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#leftHandSide}.
	 * @param ctx the parse tree
	 */
	void enterLeftHandSide(CavajParser.LeftHandSideContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#leftHandSide}.
	 * @param ctx the parse tree
	 */
	void exitLeftHandSide(CavajParser.LeftHandSideContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(CavajParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(CavajParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#conditionalOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalOrExpression(CavajParser.ConditionalOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#conditionalOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalOrExpression(CavajParser.ConditionalOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#conditionalAndExpression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalAndExpression(CavajParser.ConditionalAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#conditionalAndExpression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalAndExpression(CavajParser.ConditionalAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterInclusiveOrExpression(CavajParser.InclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitInclusiveOrExpression(CavajParser.InclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterExclusiveOrExpression(CavajParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitExclusiveOrExpression(CavajParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(CavajParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(CavajParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpression(CavajParser.EqualityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpression(CavajParser.EqualityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(CavajParser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(CavajParser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpression(CavajParser.ShiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpression(CavajParser.ShiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(CavajParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(CavajParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(CavajParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(CavajParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpression(CavajParser.UnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpression(CavajParser.UnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#preIncrementExpression}.
	 * @param ctx the parse tree
	 */
	void enterPreIncrementExpression(CavajParser.PreIncrementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#preIncrementExpression}.
	 * @param ctx the parse tree
	 */
	void exitPreIncrementExpression(CavajParser.PreIncrementExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#preDecrementExpression}.
	 * @param ctx the parse tree
	 */
	void enterPreDecrementExpression(CavajParser.PreDecrementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#preDecrementExpression}.
	 * @param ctx the parse tree
	 */
	void exitPreDecrementExpression(CavajParser.PreDecrementExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#unaryExpressionNotPlusMinus}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpressionNotPlusMinus(CavajParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#unaryExpressionNotPlusMinus}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpressionNotPlusMinus(CavajParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpression(CavajParser.PostfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpression(CavajParser.PostfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#postIncrementExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostIncrementExpression(CavajParser.PostIncrementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#postIncrementExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostIncrementExpression(CavajParser.PostIncrementExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#postIncrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostIncrementExpression_lf_postfixExpression(CavajParser.PostIncrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#postIncrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostIncrementExpression_lf_postfixExpression(CavajParser.PostIncrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#postDecrementExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostDecrementExpression(CavajParser.PostDecrementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#postDecrementExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostDecrementExpression(CavajParser.PostDecrementExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#postDecrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostDecrementExpression_lf_postfixExpression(CavajParser.PostDecrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#postDecrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostDecrementExpression_lf_postfixExpression(CavajParser.PostDecrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CavajParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void enterCastExpression(CavajParser.CastExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CavajParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void exitCastExpression(CavajParser.CastExpressionContext ctx);
}