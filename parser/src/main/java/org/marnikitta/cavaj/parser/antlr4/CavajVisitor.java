// Generated from org/marnikitta/cavaj/parser/antlr4/Cavaj.g4 by ANTLR 4.6
package org.marnikitta.cavaj.parser.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CavajParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CavajVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CavajParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(CavajParser.LiteralContext ctx);
	/**
   * Visit a parse tree produced by {@link CavajParser#integerLiteral}.
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitIntegerLiteral(CavajParser.IntegerLiteralContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#booleanLiteral}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitBooleanLiteral(CavajParser.BooleanLiteralContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#stringLiteral}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitStringLiteral(CavajParser.StringLiteralContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#nullLiteral}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitNullLiteral(CavajParser.NullLiteralContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#classType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassType(CavajParser.ClassTypeContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#variableName}.
   * @param ctx the parse tree
	 * @return the visitor result
   */
  T visitVariableName(CavajParser.VariableNameContext ctx);

  /**
	 * Visit a parse tree produced by {@link CavajParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(CavajParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#compilationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompilationUnit(CavajParser.CompilationUnitContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(CavajParser.ClassDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classParamClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassParamClause(CavajParser.ClassParamClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassParams(CavajParser.ClassParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#valVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValVar(CavajParser.ValVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassParam(CavajParser.ClassParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#superclass}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuperclass(CavajParser.SuperclassContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBody(CavajParser.ClassBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBodyDeclaration(CavajParser.ClassBodyDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassMemberDeclaration(CavajParser.ClassMemberDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#variableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(CavajParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaratorId(CavajParser.VariableDeclaratorIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#variableInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableInitializer(CavajParser.VariableInitializerContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodDeclaration(CavajParser.MethodDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodModifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodModifier(CavajParser.MethodModifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodHeader}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodHeader(CavajParser.MethodHeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#formalParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameters(CavajParser.FormalParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#formalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameter(CavajParser.FormalParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#instanceInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstanceInitializer(CavajParser.InstanceInitializerContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#staticInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStaticInitializer(CavajParser.StaticInitializerContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#constructorDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructorDeclaration(CavajParser.ConstructorDeclarationContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#constructorHeader}.
   * @param ctx the parse tree
	 * @return the visitor result
   */
  T visitConstructorHeader(CavajParser.ConstructorHeaderContext ctx);

  /**
	 * Visit a parse tree produced by {@link CavajParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(CavajParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#blockStatements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatements(CavajParser.BlockStatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#blockStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(CavajParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#resultExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResultExpression(CavajParser.ResultExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(CavajParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#constructorThisExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructorThisExpression(CavajParser.ConstructorThisExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#localVariableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalVariableDeclaration(CavajParser.LocalVariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#returnExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnExpression(CavajParser.ReturnExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#ifThenExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfThenExpression(CavajParser.IfThenExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#ifThenElseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfThenElseExpression(CavajParser.IfThenElseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#whileExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileExpression(CavajParser.WhileExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#forExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForExpression(CavajParser.ForExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#forInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForInit(CavajParser.ForInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#forCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForCondition(CavajParser.ForConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#forUpdate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForUpdate(CavajParser.ForUpdateContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#throwExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThrowExpression(CavajParser.ThrowExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#tryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTryExpression(CavajParser.TryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#catches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatches(CavajParser.CatchesContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#catchClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatchClause(CavajParser.CatchClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#catchFormalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatchFormalParameter(CavajParser.CatchFormalParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#catchType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatchType(CavajParser.CatchTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#finally_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFinally_(CavajParser.Finally_Context ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(CavajParser.PrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#primary_tmp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_tmp(CavajParser.Primary_tmpContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#primary_lfno_primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_lfno_primary(CavajParser.Primary_lfno_primaryContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#thisExpression}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitThisExpression(CavajParser.ThisExpressionContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#unitExpression}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitUnitExpression(CavajParser.UnitExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#classInstanceCreationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassInstanceCreationExpression(CavajParser.ClassInstanceCreationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#fieldAccess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldAccess(CavajParser.FieldAccessContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodInvocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodInvocation(CavajParser.MethodInvocationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodInvocation_lf_primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodInvocation_lf_primary(CavajParser.MethodInvocation_lf_primaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#methodInvocation_lfno_primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodInvocation_lfno_primary(CavajParser.MethodInvocation_lfno_primaryContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#localMethodInvocation}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitLocalMethodInvocation(CavajParser.LocalMethodInvocationContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#staticMethodInvocation}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitStaticMethodInvocation(CavajParser.StaticMethodInvocationContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#variableMethodInvocaton}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitVariableMethodInvocaton(CavajParser.VariableMethodInvocatonContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#superMethodInvocation}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitSuperMethodInvocation(CavajParser.SuperMethodInvocationContext ctx);

  /**
   * Visit a parse tree produced by {@link CavajParser#superClassMethodInvocation}.
   *
   * @param ctx the parse tree
   * @return the visitor result
   */
  T visitSuperClassMethodInvocation(CavajParser.SuperClassMethodInvocationContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#argumentList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentList(CavajParser.ArgumentListContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(CavajParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#leftHandSide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeftHandSide(CavajParser.LeftHandSideContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#assignmentOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentOperator(CavajParser.AssignmentOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#conditionalOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalOrExpression(CavajParser.ConditionalOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#conditionalAndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalAndExpression(CavajParser.ConditionalAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusiveOrExpression(CavajParser.InclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclusiveOrExpression(CavajParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(CavajParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#equalityExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpression(CavajParser.EqualityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpression(CavajParser.RelationalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpression(CavajParser.ShiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpression(CavajParser.AdditiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpression(CavajParser.MultiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpression(CavajParser.UnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#preIncrementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPreIncrementExpression(CavajParser.PreIncrementExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#preDecrementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPreDecrementExpression(CavajParser.PreDecrementExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#unaryExpressionNotPlusMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpressionNotPlusMinus(CavajParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#postfixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixExpression(CavajParser.PostfixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#postIncrementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostIncrementExpression(CavajParser.PostIncrementExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#postIncrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostIncrementExpression_lf_postfixExpression(CavajParser.PostIncrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#postDecrementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostDecrementExpression(CavajParser.PostDecrementExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#postDecrementExpression_lf_postfixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostDecrementExpression_lf_postfixExpression(CavajParser.PostDecrementExpression_lf_postfixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CavajParser#castExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCastExpression(CavajParser.CastExpressionContext ctx);
}