package org.marnikitta.cavaj.semantic.ast;

import java.util.IllegalFormatCodePointException;
import java.util.IntSummaryStatistics;
import java.util.Set;

/**
 * Created by marnikitta on 2/20/17.
 */
public class Method {

  enum MethodModifier {
    PUBLIC,
    PROTECTED,
    PRIVATE,
    ABSTRACT,
    STATIC,
    FINAL,
    NATIVE
  }

  private final String name;

  private final Set<MethodModifier> modifiers;

  private final MethodSignature signature;

  public Method(final String name, final Set<MethodModifier> modifiers, final MethodSignature signature) {
    this.name = name;
    this.modifiers = modifiers;
    this.signature = signature;
  }
}
