package org.marnikitta.cavaj.semantic.ast.type;

/**
 * Created by marnikitta on 2/20/17.
 */
public class Clazz implements Type {
  private final String name;
  private final Clazz superClazz;

  public Clazz(final String name, final Clazz superClazz) {
    this.name = name;
    this.superClazz = superClazz;
  }

  @Override
  public String name() {
    return this.name;
  }

  public Clazz superClazz() {
    return superClazz;
  }
}
