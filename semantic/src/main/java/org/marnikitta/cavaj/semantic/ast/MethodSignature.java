package org.marnikitta.cavaj.semantic.ast;

import org.marnikitta.cavaj.semantic.ast.type.Clazz;

import java.util.List;
import java.util.Objects;

/**
 * Created by marnikitta on 2/20/17.
 */
public class MethodSignature {
  private final Clazz returnType;

  private final List<Clazz> argumentList;

  public MethodSignature(final Clazz returnType, final List<Clazz> argumentList) {
    this.returnType = returnType;
    this.argumentList = argumentList;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final MethodSignature that = (MethodSignature) o;
    return Objects.equals(returnType, that.returnType) &&
            Objects.equals(argumentList, that.argumentList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(returnType, argumentList);
  }

  @Override
  public String toString() {
    return "MethodSignature{" + "returnType=" + returnType +
            ", argumentList=" + argumentList +
            '}';
  }
}
