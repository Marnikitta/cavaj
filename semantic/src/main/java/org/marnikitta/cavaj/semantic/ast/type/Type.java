package org.marnikitta.cavaj.semantic.ast.type;

/**
 * Created by marnikitta on 2/20/17.
 */
public interface Type {
  String name();
}
